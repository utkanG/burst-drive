﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.PostProcessing;
using Steamworks;
using System.Text;

public class MenuUIControl : MonoBehaviour {

    [SerializeField]
    private GameObject scoresMenu;
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private GameObject returnButton;
    [SerializeField]
    private GameObject playButton;
    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private Animator anim;
    [SerializeField]
    private GameObject main1stMenu;
    [SerializeField]
    private GameObject playMenu;
    [SerializeField]
    private GameObject singlePlayerButton;
    [SerializeField]
    private GameObject settingsMenu;
    [SerializeField]
    private GameObject pauseButtons;
    [SerializeField]
    private GameObject gameOverMenu;
    [SerializeField]
    private GameObject firstSettingsMenu;
    [SerializeField]
    private GameObject controlsMenu;
    [SerializeField]
    private GameObject[] inputMenus;
    [SerializeField]
    private Text[] inputDeviceTexts;
    [HideInInspector]
    public PostProcessingProfile ppprofile;
    [SerializeField]
    private Text bloomOnText;
    [SerializeField]
    private Text screenModeText;
    [SerializeField]
    private Text resText;
    [SerializeField]
    private Slider musicSlider;
    [SerializeField]
    private Slider soundSlider;
    [SerializeField]
    private GameObject top10Button;
    [SerializeField]
    private Toggle luckyToggle;

    [Header("Input Stuff")]
    private bool changingKey;
    [SerializeField]
    private Text[] keyTexts1;
    [SerializeField]
    private Text[] keyTexts2;
    [SerializeField]
    private GameObject keyChangeWarning;
    private KeyCode currentKeyCode;
    private int currentKeyIndex;
    private bool gotKey;
    [SerializeField]
    private Button[] accelerationButtons; // 0, 1 p1 | 2, 3 p2
    [SerializeField]
    private Button[] navButtons; // 0, 1 p1 | 2, 3 p2

    [SerializeField]
    private GameObject musicSliderObject;
    [SerializeField]
    private GameObject p1controllerObject;

    [HideInInspector]
    public int currentScreenModeIndex;
    private int screenIndexToSet;
    [HideInInspector]
    public FullScreenMode currentScreenMode;
    private int joystickCount = 0;
    [HideInInspector]
    public float musicVol;
    [HideInInspector]
    public float soundVol;
    private float bindTimer;
    private bool timerSet;
    [SerializeField]
    private GameObject titleObject;
    [SerializeField]
    private GameObject menuParticles;

    [SerializeField]
    StandaloneInputModule inputModule;
    
    public CameraShaker camShaker;
    [SerializeField]
    Text camShakeText;

    private AudioSource buttonSound;
    [HideInInspector]
    public Resolution selectedResolution;

    [SerializeField] private Text backText;

    public void Awake()
    {
        GameControl.OnPaused += ActivatePauseMenu;
        GameControl.OnUnpaused += DeactivatePauseMenu;

        buttonSound = GetComponent<AudioSource>();

        //for(int i = 0; i < Screen.resolutions.Length; i++)
        //{
        //    Debug.Log(Screen.resolutions[i].ToString());
        //}
    }

    private void Start()
    {
        mainMenu.SetActive(true);
        anim.Play("None");
        Cursor.visible = true;
        main1stMenu.SetActive(true);
        playMenu.SetActive(false);
        settingsMenu.SetActive(false);
        settingsChanged = false;

        joystickCount = Input.GetJoystickNames().Length;
        ApplySettings();
        EventSystem.current.SetSelectedGameObject(playButton);

        controls[0].SetActive(false);
        controls[1].SetActive(false);

        if(SteamManager.Initialized)
            SteamFriends.SetRichPresence("steam_display", "#Status_AtMainMenu");
    }

    private void ApplySettings()
    {
        SwitchFullScreen(0);
        SwitchResolution(0);
        ChangeInputDevice1(0);
        ChangeInputDevice2(0);
        UpdateKeyTexts();
        UpdateBloomText();
        UpdateCamShakeText();
        musicSlider.value = musicVol;
        soundSlider.value = soundVol;
        Invoke("ActivateSounds", 1);
        MusicManager.instance.SetVolume(musicVol);
        SoundManager.instance.SetVolume(0);
        EventSystem.current.SetSelectedGameObject(musicSliderObject);
        //ApplySettingsButton();
    }

    private void ActivateSounds()
    {
        SoundManager.instance.SetVolume(soundVol);
    }

    public void ApplySettingsButton()
    {
        if(!settingsChanged) return;

        currentScreenModeIndex = screenIndexToSet;
        switch (currentScreenModeIndex)
        {
            case 0:
                screenModeText.text = "FULLSCREEN";
                currentScreenMode = FullScreenMode.ExclusiveFullScreen;
                break;
            case 1:
                screenModeText.text = "WINDOWED";
                currentScreenMode = FullScreenMode.Windowed;
                break;
            case 2:
                screenModeText.text = "BORDERLESS";
                currentScreenMode = FullScreenMode.FullScreenWindow;
                break;
            default:
                break;
        }

        Screen.SetResolution(selectedResolution.width, selectedResolution.height, currentScreenMode);
        resText.text = selectedResolution.width + "x" + selectedResolution.height;
        settingsChanged = false;
        GameControl.instance.Save();
    }

    private IEnumerator ResolutionApplyRoutine()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        ApplySettingsButton();
    }

    public void SetAudio(int index) // 0 for music, 1 for sound
    {
        if (index == 0)
            musicVol = musicSlider.value;
        if (index == 1)
            soundVol = soundSlider.value;

        SoundManager.instance.SetVolume(soundVol);
        MusicManager.instance.SetVolume(musicVol);
        GameControl.instance.Save();
    }

    public void ActivateScoresMenu()
    {
        titleObject.SetActive(false);
        scoresMenu.SetActive(true);
        main1stMenu.SetActive(false);
        EventSystem.current.SetSelectedGameObject(top10Button);
    }

    private void ActivatePauseMenu()
    {
        controlsAnim[0].speed = 10;
        controlsAnim[1].speed = 10;
        ShowControls();
        pauseMenu.SetActive(true);
        pauseButtons.SetActive(true);
        EventSystem.current.SetSelectedGameObject(returnButton);
        Cursor.visible = true;
        //Cursor.lockState = CursorLockMode.None;
    }

    public void StartChangeKey(int index)
    {
        keyChangeWarning.SetActive(true);
        inputModule.submitButton = "";
        currentKeyIndex = index;
        gotKey = false;
        changingKey = true;
    }

    private void UpdateKeyTexts()
    {
        if (GameControl.instance.player1Script.controllerInput)
        {
            inputDeviceTexts[0].text = "CONTROLLER " + GameControl.instance.player1Script.controllerIndex;
            inputMenus[0].SetActive(false);
            inputMenus[2].SetActive(true);
            ChangeNavButton(0, 1);
        }
        else
        {
            inputDeviceTexts[0].text = "KEYBOARD";
            inputMenus[0].SetActive(true);
            inputMenus[2].SetActive(false);
            ChangeNavButton(0, 0);
        }

        if (GameControl.instance.player2Script.controllerInput)
        {
            inputDeviceTexts[1].text = "CONTROLLER " + GameControl.instance.player2Script.controllerIndex;
            inputMenus[1].SetActive(false);
            inputMenus[3].SetActive(true);
            ChangeNavButton(2, 3);
        }
        else
        {
            inputDeviceTexts[1].text = "KEYBOARD";
            inputMenus[1].SetActive(true);
            inputMenus[3].SetActive(false);
            ChangeNavButton(2, 2);
        }

        for (int i = 0; i < 8; i++)
        {
            keyTexts1[i].text = AddSpacesToSentence(KeyCodeToJoystickButton(GameControl.instance.player1Script.keyCodes[i]));
            keyTexts2[i].text = AddSpacesToSentence(KeyCodeToJoystickButton(GameControl.instance.player2Script.keyCodes[i]));
        }

        musicSlider.value = musicVol;
        soundSlider.value = soundVol;
        SoundManager.instance.SetVolume(soundVol);
        MusicManager.instance.SetVolume(musicVol);
    }

    private void UpdateBloomText()
    {
        if (ppprofile.bloom.enabled)
            bloomOnText.text = "BLOOM ON";
        else
            bloomOnText.text = "BLOOM OFF";
    }

    private void OnGUI()
    {
        if (changingKey)
        {
            if (!timerSet)
            {
                timerSet = true;
                bindTimer = .2f;
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                keyChangeWarning.SetActive(false);
                changingKey = false;
                gotKey = true;
                timerSet = false;
                inputModule.submitButton = "Submit";
                return;
            }

            Event e = Event.current;

            int playerIndex = 0;
            if (currentKeyIndex <= 7)
                playerIndex = 0;
            else
                playerIndex = 1;

            if (bindTimer <= 0)
            {
                if (playerIndex == 0)
                {
                    if (GameControl.instance.player1Script.controllerInput)
                    {
                        if (Input.anyKey && !gotKey)
                        {
                            gotKey = true;
                            for (int i = 0; i < 20; i++)
                            {
                                if (Input.GetKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "JoystickButton" + i)))
                                {
                                    currentKeyCode = (KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + GameControl.instance.player1Script.controllerIndex + "Button" + i);
                                    break;
                                }
                            }
                            StopKeyChanging();
                        }
                    }
                    else
                    {
                        if (Input.anyKeyDown && !gotKey)
                        {
                            gotKey = true;
                            currentKeyCode = e.keyCode;
                            if (Input.GetKey(KeyCode.LeftShift))
                            {
                                currentKeyCode = KeyCode.LeftShift;
                            }
                            else if (Input.GetKey(KeyCode.RightShift))
                            {
                                currentKeyCode = KeyCode.RightShift;
                            }
                            StopKeyChanging();
                        }
                    }
                }
                else
                {
                    if (GameControl.instance.player2Script.controllerInput)
                    {
                        if (Input.anyKey && !gotKey)
                        {
                            gotKey = true;
                            for (int i = 0; i < 20; i++)
                            {
                                if (Input.GetKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "JoystickButton" + i)))
                                {
                                    currentKeyCode = (KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + GameControl.instance.player2Script.controllerIndex + "Button" + i);
                                    break;
                                }
                            }
                            StopKeyChanging();
                        }
                    }
                    else
                    {
                        if (Input.anyKeyDown && !gotKey)
                        {
                            gotKey = true;
                            currentKeyCode = e.keyCode;
                            if (Input.GetKey(KeyCode.LeftShift))
                            {
                                currentKeyCode = KeyCode.LeftShift;
                            }
                            else if (Input.GetKey(KeyCode.RightShift))
                            {
                                currentKeyCode = KeyCode.RightShift;
                            }
                            StopKeyChanging();
                        }
                    }
                }
            }   

            if (currentKeyIndex < 8)
            {
                GameControl.instance.player1Script.keyCodes[currentKeyIndex] = currentKeyCode;
                keyTexts1[currentKeyIndex].text = AddSpacesToSentence(KeyCodeToJoystickButton(currentKeyCode));
                if(GameControl.instance.gameOn)
                    ShowControls(1);
            }
            else
            {
                GameControl.instance.player2Script.keyCodes[currentKeyIndex - 8] = currentKeyCode;
                keyTexts2[currentKeyIndex - 8].text = AddSpacesToSentence(KeyCodeToJoystickButton(currentKeyCode));
                if(GameControl.instance.gameOn)
                    ShowControls(2);
            }

            if (gotKey)
            {
                currentKeyCode = KeyCode.None;
                GameControl.instance.Save();
                timerSet = false;
            }
        }
    }

    private float timer;
    private void StopKeyChanging()
    {
        keyChangeWarning.SetActive(false);
        changingKey = false;
        timer = .2f;
    }

    private void DeactivatePauseMenu()
    {
        pauseMenu.SetActive(false);
        HideControls();
        pauseButtons.SetActive(false);
        firstSettingsMenu.SetActive(true);
        controlsMenu.SetActive(false);

        settingsMenu.SetActive(false);
        /*Cursor.lockState = CursorLockMode.Locked*/;
        Cursor.visible = false;
    }

    public void BackToMenu()
    {
        //SwitchFullScreen(0);
        //SwitchResolution(0);
        StartCoroutine(ResolutionApplyRoutine());

        titleObject.SetActive(true);
        playMenu.SetActive(false);
        main1stMenu.SetActive(true);
        scoresMenu.SetActive(false);
        settingsMenu.SetActive(false);

        EventSystem.current.SetSelectedGameObject(playButton);
        if (GameControl.instance.paused)
        {
            pauseButtons.SetActive(true);
            EventSystem.current.SetSelectedGameObject(returnButton);
        }
        SetTexts();

        if(SteamManager.Initialized)
            SteamFriends.SetRichPresence("steam_display", "#Status_AtMainMenu");
    }

    public void ControlsMenu()
    {
        firstSettingsMenu.SetActive(false);
        controlsMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(p1controllerObject);
    }

    public void BackToSettings()
    {
        firstSettingsMenu.SetActive(true);
        controlsMenu.SetActive(false);
        EventSystem.current.SetSelectedGameObject(musicSliderObject);
    }

    public void StartGame(int playerCount)
    {
        controlsAnim[0].speed = 1;
        controlsAnim[1].speed = 1;
        ShowControls();
        Invoke("HideControls", 20);
        MusicManager.instance.ChangeMusic(MusicManager.Music.Combat);
        GameControl.instance.EndGame();
        mainMenu.SetActive(false);
        main1stMenu.SetActive(true);
        menuParticles.SetActive(true);
        playMenu.SetActive(false);
        GameControl.instance.StartGame(playerCount);
        anim.Play("Game Start");
        Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
    }

    public void HideControls()
    {
        if(pauseMenu.activeInHierarchy) return;
        controls[0].SetActive(false);
        controls[1].SetActive(false);
    }

    private void ChangeNavButton(int player, int accelerationButton) // 0 for p 1, 2 for p 2
    {
        Navigation nav;
        nav = navButtons[player].navigation;
        nav.selectOnDown = accelerationButtons[accelerationButton];
        navButtons[player].navigation = nav;
        nav = navButtons[player+1].navigation;
        nav.selectOnDown = accelerationButtons[accelerationButton];
        navButtons[player+1].navigation = nav;
    }

    public void ChangeInputDevice1(int direction)
    {
        joystickCount = Input.GetJoystickNames().Length;

        if (direction == 1)
        {
            switch (inputDeviceTexts[0].text)
            {
                case "KEYBOARD":
                    inputDeviceTexts[0].text = "CONTROLLER 1";
                    inputMenus[0].SetActive(false);
                    inputMenus[2].SetActive(true);
                    GameControl.instance.player1Script.controllerInput = true;
                    GameControl.instance.player1Script.controllerIndex = 1;
                    break;
                case "CONTROLLER 1":
                    if (joystickCount > 1)
                    {
                        inputDeviceTexts[0].text = "CONTROLLER 2";
                        GameControl.instance.player1Script.controllerInput = true;
                        GameControl.instance.player1Script.controllerIndex = 2;
                    }
                    else
                    {
                        inputDeviceTexts[0].text = "KEYBOARD";
                        inputMenus[0].SetActive(true);
                        inputMenus[2].SetActive(false);
                        GameControl.instance.player1Script.controllerInput = false;
                    }
                    break;
                case "CONTROLLER 2":
                    inputDeviceTexts[0].text = "CONTROLLER 1";
                    inputMenus[0].SetActive(false);
                    inputMenus[2].SetActive(true);
                    GameControl.instance.player1Script.controllerInput = true;
                    GameControl.instance.player1Script.controllerIndex = 1;
                    break;
                default:
                    break;
            }
        }
        else if(direction == -1)
        {
            switch (inputDeviceTexts[0].text)
            {
                case "KEYBOARD":
                    if (joystickCount > 1)
                    {
                        inputDeviceTexts[0].text = "CONTROLLER 2";
                        inputMenus[0].SetActive(false);
                        inputMenus[2].SetActive(true);
                        GameControl.instance.player1Script.controllerInput = true;
                        GameControl.instance.player1Script.controllerIndex = 2;
                    }
                    else
                    {
                        inputDeviceTexts[0].text = "CONTROLLER 1";
                        inputMenus[0].SetActive(false);
                        inputMenus[2].SetActive(true);
                        GameControl.instance.player1Script.controllerIndex = 1;
                        GameControl.instance.player1Script.controllerInput = true;
                    }
                    break;
                case "CONTROLLER 1":
                    inputDeviceTexts[0].text = "KEYBOARD";
                    inputMenus[0].SetActive(true);
                    inputMenus[2].SetActive(false);
                    GameControl.instance.player1Script.controllerInput = false;
                    break;
                case "CONTROLLER 2":
                    GameControl.instance.player1Script.controllerIndex = 1;
                    GameControl.instance.player1Script.controllerInput = true;
                    inputDeviceTexts[0].text = "CONTROLLER 1";
                    inputMenus[0].SetActive(false);
                    inputMenus[2].SetActive(true);
                    break;
                default:
                    break;
            }
        }
        if (GameControl.instance.player1Script.controllerInput)
            ChangeNavButton(0, 1);
        else
            ChangeNavButton(0, 0);
        if(GameControl.instance.gameOn)
            ShowControls(1);
        GameControl.instance.Save();
    }

    public void ChangeInputDevice2(int direction)
    {
        joystickCount = Input.GetJoystickNames().Length;

        if (direction == 1)
        {
            switch (inputDeviceTexts[1].text)
            {
                case "KEYBOARD":
                    inputDeviceTexts[1].text = "CONTROLLER 1";
                    inputMenus[1].SetActive(false);
                    inputMenus[3].SetActive(true);
                    GameControl.instance.player2Script.controllerInput = true;
                    GameControl.instance.player2Script.controllerIndex = 1;
                    break;
                case "CONTROLLER 1":
                    if (joystickCount > 1)
                    {
                        inputDeviceTexts[1].text = "CONTROLLER 2";
                        GameControl.instance.player2Script.controllerInput = true;
                        GameControl.instance.player2Script.controllerIndex = 2;
                    }
                    else
                    {
                        inputDeviceTexts[1].text = "KEYBOARD";
                        inputMenus[1].SetActive(true);
                        inputMenus[3].SetActive(false);
                        GameControl.instance.player2Script.controllerInput = false;
                    }
                    break;
                case "CONTROLLER 2":
                    inputDeviceTexts[1].text = "CONTROLLER 1";
                    inputMenus[1].SetActive(false);
                    inputMenus[3].SetActive(true);
                    GameControl.instance.player2Script.controllerInput = true;
                    GameControl.instance.player2Script.controllerIndex = 1;
                    break;
                default:
                    break;
            }
        }
        else if(direction == -1)
        {
            switch (inputDeviceTexts[1].text)
            {
                case "KEYBOARD":
                    if (joystickCount > 1)
                    {
                        inputDeviceTexts[1].text = "CONTROLLER 2";
                        inputMenus[1].SetActive(false);
                        inputMenus[3].SetActive(true);
                        GameControl.instance.player2Script.controllerIndex = 2;
                        GameControl.instance.player2Script.controllerInput = true;
                    }
                    else
                    {
                        inputDeviceTexts[1].text = "CONTROLLER 1";
                        inputMenus[1].SetActive(false);
                        inputMenus[3].SetActive(true);
                        GameControl.instance.player2Script.controllerIndex = 1;
                        GameControl.instance.player2Script.controllerInput = true;
                    }
                    break;
                case "CONTROLLER 1":
                    inputDeviceTexts[1].text = "KEYBOARD";
                    inputMenus[1].SetActive(true);
                    inputMenus[3].SetActive(false);
                    GameControl.instance.player2Script.controllerInput = false;
                    break;
                case "CONTROLLER 2":
                    inputDeviceTexts[1].text = "CONTROLLER 1";
                    inputMenus[1].SetActive(false);
                    inputMenus[3].SetActive(true);
                    GameControl.instance.player2Script.controllerInput = true;
                    GameControl.instance.player2Script.controllerIndex = 1;
                    break;
                default:
                    break;
            }
        }

        if (GameControl.instance.player2Script.controllerInput)
            ChangeNavButton(2, 3);
        else
            ChangeNavButton(2, 2);

        if(GameControl.instance.gameOn)
            ShowControls(2);
        GameControl.instance.Save();
    }
   
    public void QuitGame()
    {
        Application.Quit();
    }

    public void Settings()
    {
        titleObject.SetActive(false);
        screenIndexToSet = currentScreenModeIndex;
        main1stMenu.SetActive(false);
        settingsMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(musicSliderObject);
        resText.text = selectedResolution.width + "x" + selectedResolution.height ;
        pauseButtons.SetActive(false);
        settingsChanged = false;
        backText.text = "BACK";
    }

    public void ReturnGame()
    {
        GameControl.instance.Pause();
    }

    public void PlayButton()
    {
        StartGame(1);
        gameOverMenu.SetActive(false);
        mainMenu.SetActive(false);
        main1stMenu.SetActive(false);
        menuParticles.SetActive(false);
        playMenu.SetActive(false);
        GameControl.instance.pressToFireTexts[1].gameObject.SetActive(true);
        GameControl.instance.pressToFireTexts[0].gameObject.SetActive(false);
        EventSystem.current.SetSelectedGameObject(singlePlayerButton);

        if (luckyToggle.isOn)
            for (int i = 0; i < 15; i++)
            {
                //GameObject e = ExplosionPooler.instance.GetExplosion(3);
                //e.transform.position = Vector3.zero;
                //e.SetActive(true);
                PickupManager.instance.SpawnPickUp(Random.Range(0, 29), Random.insideUnitCircle * 6);
            }
    }

    public void QuitToMain()
    {
        pauseMenu.SetActive(false);
        MusicManager.instance.ChangeMusic(MusicManager.Music.Menu);
        GameControl.instance.paused = false;
        Time.timeScale = 1;
        GameControl.instance.EndGame();
        EventSystem.current.SetSelectedGameObject(returnButton);
        EventSystem.current.SetSelectedGameObject(playButton);
        //levelNoSlider.value = LevelManager.instance.currentLevelNo;
        //levelNoText.text = "Level " + LevelManager.instance.currentLevelNo.ToString();
        mainMenu.SetActive(true);
        menuParticles.SetActive(true);
        main1stMenu.SetActive(true);
        gameOverMenu.SetActive(false);
        anim.Play("None");

        if(SteamManager.Initialized)
            SteamFriends.SetRichPresence("steam_display", "#Status_AtMainMenu");

        controls[0].SetActive(false);
        controls[1].SetActive(false);
    }

    public void Retire()
    {
        controls[0].SetActive(false);
        controls[1].SetActive(false);

        LevelManager.instance.CleanUpLevel();
        GameControl.instance.ActivateGameOverMenu();
        GameControl.instance.gameOverMenuTimer = -1;
        GameControl.instance.canSpawnOver[0] = false;
        GameControl.instance.canSpawnOver[1] = false;
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        //Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        GameControl.instance.playerPanels[0].SetActive(false);
        GameControl.instance.playerPanels[1].SetActive(false);
        GameControl.instance.playerScores[0].SetActive(false);
        GameControl.instance.playerScores[1].SetActive(false);
        GameControl.instance.EndGame();
        GameControl.instance.pressToFireTexts[1].gameObject.SetActive(false);
        GameControl.instance.pressToFireTexts[0].gameObject.SetActive(false);
        EnemyManager.instance.hpSlider.transform.parent.gameObject.SetActive(false);

        if(SteamManager.Initialized)
            SteamFriends.SetRichPresence("steam_display", "#Status_AtMainMenu");
    }

    private void Update()
    {
        if(bindTimer > 0)
        {
            bindTimer -= Time.unscaledDeltaTime;
        }

        if(timer > 0)
        {
            timer -= Time.unscaledDeltaTime;

            if(timer <= 0)
                inputModule.submitButton = "Submit";
        }
    }

    //public void UpdateStartLevelNo()
    //{
    //    LevelManager.instance.currentLevelNo = (int)levelNoSlider.value;
    //    levelNoText.text = "Level " + (int)levelNoSlider.value;
    //}

    //public void UpdateStartFireRate()
    //{
    //    PickupManager.instance.player1.bonusFireRatePoints = (int)fireSlider.value - 1;
    //    fireText.text = "Fire Rate " + (int)fireSlider.value;
    //    if (dimensionalReload.isOn)
    //    {
    //        fireSlider.value = 21;
    //        fireText.text = "Dimensional Reload";
    //    }
    //}

    //public void UpdateStartCooling()
    //{
    //    PickupManager.instance.player1.bonusCoolingPoints = (int)coolSlider.value - 1;
    //    coolText.text = "Cooling " + (int)coolSlider.value;
    //    if (absoluteCooling.isOn)
    //    {
    //        coolSlider.value = 21;
    //        coolText.text = "Absolute Cooling";
    //    }
    //}

    public void ToggleBloom()
    {
        ppprofile.bloom.enabled = !ppprofile.bloom.enabled;
        UpdateBloomText();

        GameControl.instance.Save();
    }

    public void ToggleCamShake()
    {
        camShaker.enabled = !camShaker.enabled;
        UpdateCamShakeText();

        GameControl.instance.Save();
    }

    private void UpdateCamShakeText()
    {
        if (camShaker.enabled)
            camShakeText.text = "CAMERA SHAKE ON";
        else
            camShakeText.text = "CAMERA SHAKE OFF";
    }

    private bool settingsChanged = false;
    public void SwitchFullScreen(int direction)
    {
        screenIndexToSet += direction;
        if (screenIndexToSet < 0)
            screenIndexToSet = 2;
        if (screenIndexToSet > 2)
            screenIndexToSet = 0;
        settingsChanged = true;
        backText.text = "APPLY & BACK";
        if (direction == 0)
            screenIndexToSet = currentScreenModeIndex;
        switch (screenIndexToSet)
        {
            case 0:
                screenModeText.text = "FULLSCREEN";
                break;
            case 1:
                screenModeText.text = "WINDOWED";
                break;
            case 2:
                screenModeText.text = "BORDERLESS";
                break;
            default:
                break;
        }
    }

    private void SetTexts()
    {
        resText.text = selectedResolution.width.ToString() + "x" + selectedResolution.height.ToString();
        switch (currentScreenModeIndex)
        {
            case 0:
                screenModeText.text = "FULLSCREEN";
                break;
            case 1:
                screenModeText.text = "WINDOWED";
                break;
            case 2:
                screenModeText.text = "BORDERLESS";
                break;
            default:
                break;
        }
    }

    public void SwitchResolution(int direction)
    {
        if(direction == 0)
        {
            resText.text = selectedResolution.width.ToString() + "x" + selectedResolution.height.ToString();

            return;
        }

        backText.text = "APPLY & BACK";
        int selectedIndex = 0;
        int count = Screen.resolutions.Length;
        for(int i = 0; i < count; i++)
        {
            if(selectedResolution.height == Screen.resolutions[i].height && selectedResolution.width == Screen.resolutions[i].width)
            {
                selectedIndex = i;
                break;
            }
        }

        do
        {
            if(selectedIndex == count - 1 && direction == 1)
                return;
            else if(selectedIndex == 0 && direction == -1)
                return;
            selectedIndex += direction;
        }
        while(Screen.resolutions[selectedIndex].height == selectedResolution.height && Screen.resolutions[selectedIndex].width == selectedResolution.width);

        selectedResolution = Screen.resolutions[selectedIndex];
        resText.text = selectedResolution.width.ToString() + "x" + selectedResolution.height.ToString();
        settingsChanged = true;
    }

    public void PlayButtonSound()
    {
        buttonSound.Play();
    }

    private string AddSpacesToSentence(string text, bool upper = false)
    {
        if(string.IsNullOrEmpty(text))
            return "";
        StringBuilder newText = new StringBuilder(text.Length * 2);
        newText.Append(text[0]);
        for(int i = 1; i < text.Length; i++)
        {
            if(char.IsUpper(text[i]) && text[i - 1] != ' ')
                newText.Append(' ');
            newText.Append(text[i]);
        }
        if(upper)
            return newText.ToString().ToUpper();
        else
            return newText.ToString();
    }

    private string KeyCodeToJoystickButton(KeyCode keyCode)
    {
        if(keyCode.ToString().Length < 8)
            return keyCode.ToString();
        string s = keyCode.ToString().Remove(8, 1);
        KeyCode newKeyCode = KeyCode.None;
        if(s.Substring(0, 8) == "Joystick")
        {
            newKeyCode = (KeyCode) System.Enum.Parse(typeof(KeyCode), s);
            switch(newKeyCode)
            {
                case KeyCode.JoystickButton0:
                return "A";
                case KeyCode.JoystickButton1:
                return "B";
                case KeyCode.JoystickButton2:
                return "X";
                case KeyCode.JoystickButton3:
                return "Y";
                case KeyCode.JoystickButton4:
                return "Left Bumper";
                case KeyCode.JoystickButton5:
                return "Right Bumper";
                case KeyCode.JoystickButton6:
                return "Back";
                case KeyCode.JoystickButton7:
                return "Start";
                case KeyCode.JoystickButton8:
                return "Left Stick Button";
                case KeyCode.JoystickButton9:
                return "Right Stick Button";
                default:
                break;
            }
        }
        return keyCode.ToString();
    }

    [Header("Control Texts")]
    [SerializeField] private Text[] inputTexts;
    [SerializeField] private Text[] fireTexts;
    [SerializeField] private Text[] turnTexts;
    [SerializeField] private Text[] boostTexts;
    [SerializeField] private Text[] breakTexts;
    [SerializeField] private GameObject[] controls;
    [SerializeField] private Animator[] controlsAnim;

    private void ShowControls(int only = 0)
    {
        if(only == 0)
        {
            HideControls();
            controls[0].SetActive(true);
            controls[1].SetActive(true);
        }
        else if(only == 1)
        {
            controls[0].SetActive(false);
            controls[0].SetActive(true);
        }
        else if(only == 2)
        {
            controls[1].SetActive(false);
            controls[1].SetActive(true);
        }

        if(GameControl.instance.player1Script.controllerInput)
        {
            inputTexts[0].text = "<color=#ffffffff>Gamepad</color>\n\n" +
"Fire\n\n" +
"Turn\n\n" +
"Boost\n\n" +
"Brake";
            fireTexts[0].text = "[" + keyTexts1[5].text.ToUpper() + "]";
            turnTexts[0].text = "[LEFT STICK]\n[D-PAD]";
            boostTexts[0].text = "[" + keyTexts1[4].text.ToUpper() + "]";
            breakTexts[0].text = "[" + keyTexts1[7].text.ToUpper() + "]";
        }
        else
        {
            inputTexts[0].text = "<color=#ffffffff>Keyboard</color>\n\n" +
"Fire\n\n" +
"Turn\n\n" +
"Boost\n\n" +
"Brake";
            fireTexts[0].text = "[MOUSE 1]\n<color=#ff0000ff>or</color> [" + keyTexts1[2].text.ToUpper() + "]";
            turnTexts[0].text = "[MOUSE]\n<color=#ff0000ff>or</color> [" + keyTexts1[1].text.ToUpper() + "]/" +
                "[" + keyTexts1[0].text.ToUpper() + "]";
            boostTexts[0].text = "[" + keyTexts1[3].text.ToUpper() + "]";
            breakTexts[0].text = "[" + keyTexts1[6].text.ToUpper() + "]";
        }

        if(GameControl.instance.player2Script.controllerInput)
        {
            inputTexts[1].text = "<color=#ffffffff>Gamepad</color>\n\n" +
"Fire\n\n" +
"Turn\n\n" +
"Boost\n\n" +
"Brake";
            fireTexts[1].text = "[" + keyTexts2[5].text.ToUpper() + "]";
            turnTexts[1].text = "[LEFT STICK]\n<color=#ff0000ff>or</color> [D-PAD]";
            boostTexts[1].text = "[" + keyTexts2[4].text.ToUpper() + "]";
            breakTexts[1].text = "[" + keyTexts2[7].text.ToUpper() + "]";
        }
        else
        {
            inputTexts[1].text = "<color=#ffffffff>Keyboard</color>\n\n" +
"Fire\n\n" +
"Turn\n\n" +
"Boost\n\n" +
"Brake";
            fireTexts[1].text = "[" + keyTexts2[2].text.ToUpper() + "]";
            turnTexts[1].text = "[" + keyTexts2[0].text.ToUpper() + "]\n " +
                "[" + keyTexts2[1].text.ToUpper() + "]";
            boostTexts[1].text = "[" + keyTexts2[3].text.ToUpper() + "]";
            breakTexts[1].text = "[" + keyTexts2[6].text.ToUpper() + "]";
        }
    }

    //public void UpdateStartWeapon()
    //{
    //    PickupManager.instance.player1.currentWeapon = (PlayerShip.Weapons)weaponDropdown.value;
    //}

    //public void UpdateBools()
    //{
    //    if (absoluteCooling.isOn)
    //    {
    //        PickupManager.instance.player1.absoluteCooling = true;
    //        coolSlider.value = 21;
    //        UpdateStartCooling();
    //        coolText.text = "Absolute Cooling";
    //    }
    //    else
    //    {
    //        UpdateStartCooling();
    //        PickupManager.instance.player1.absoluteCooling = false;
    //    }

    //    if (dimensionalReload.isOn)
    //    {
    //        PickupManager.instance.player1.dimensionalReload = true;
    //        fireSlider.value = 21;
    //        UpdateStartFireRate();
    //        fireText.text = "Dimensional Reload";
    //    }
    //    else
    //    {
    //        UpdateStartFireRate();
    //        PickupManager.instance.player1.dimensionalReload = false;
    //    }

    //    if (infiniteLives.isOn)
    //        GameControl.instance.infiniteLives = true;
    //    else
    //    {
    //        GameControl.instance.infiniteLives = false;
    //        UpdateLivesAndShields();
    //    }

    //    if (chaosBarrel.isOn)
    //        PickupManager.instance.player1.chaosGun = true;
    //    else
    //        PickupManager.instance.player1.chaosGun = false;

    //    if(laserSightToggle.isOn)
    //        PickupManager.instance.player1.laserSight = true;
    //    else
    //        PickupManager.instance.player1.laserSight = false;
    //}

    //public void UpdateLivesAndShields()
    //{
    //    PickupManager.instance.player1.currentShieldPoints = (int)shieldSlider.value;
    //    shieldText.text = "Shields " + (int)shieldSlider.value;
    //    GameControl.instance.player1Lives = (int)lifeSlider.value;
    //    lifeText.text = "Lives " + (int)lifeSlider.value;
    //}
}
