﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;

public class ScoreboardControl : MonoBehaviour {

    //(0-10) 0 is the player best texts, 1-10 are the other players (both from online leaderboards) [total of 11] 
    [Header("Text Elements")]
    [SerializeField]
    private Text[] playerRankTexts;
    [SerializeField]
    private Text[] playerNameTexts;
    [SerializeField]
    private Text[] playerScoreTexts;
    [SerializeField]
    private Text[] playerLevelTexts;
    [SerializeField]
    private Text[] playerFinishedTexts;

    private int[] ranksToDisplay = new int[11];
    private string[] namesToDisplay = new string[11];
    public int[] scoresToDisplay = new int[11];
    private int[] levelsToDisplay = new int[11];
    private int[] finishStatusToDisplay = new int[11];

    private bool gotDataFromSteam;
    private int currentStartRange;
    private int currentLeaderboardEntryCount;

    private SteamLeaderboard_t currentSteamLeaderboard;
    private SteamLeaderboardEntries_t currentLeaderboardEntries_t;
    private SteamLeaderboardEntries_t currentPlayerEntries_t;
    private LeaderboardEntry_t[] currentPlayerEntries;
    private LeaderboardEntry_t[] currentLeaderboardEntries;

    private CallResult<LeaderboardFindResult_t> callResultFind;
    private CallResult<LeaderboardScoreUploaded_t> callResultUpload;
    private CallResult<LeaderboardScoresDownloaded_t> callResultDownload;
    private CallResult<LeaderboardScoresDownloaded_t> callResultPersonaDownload;

    [SerializeField]
    private GameObject noConnectionPanel;
    [SerializeField]
    private GameObject loadingPanel;

    private float scoreUploadTimer;
    private bool uploaded;

    private void Start()
    {
        DownloadPersonaScore();
    }

    private void OnEnable()
    {
        callResultFind = CallResult<LeaderboardFindResult_t>.Create(OnFindLeaderboard);
        callResultUpload = CallResult<LeaderboardScoreUploaded_t>.Create(OnUploadScore);
        callResultDownload = CallResult<LeaderboardScoresDownloaded_t>.Create(OnDownloadScore);
        callResultPersonaDownload = CallResult<LeaderboardScoresDownloaded_t>.Create(OnPersonaScoreDownload);

        currentLeaderboardEntries = new LeaderboardEntry_t[10];
        currentStartRange = 0;
        noConnectionPanel.SetActive(false);
        FindLeaderboard();
    }

    public void NextPage()
    {
        currentStartRange += 11;
        if (currentLeaderboardEntryCount < 10)
            currentStartRange -= 11;
        else
            loadingPanel.SetActive(true);
        DownloadPersonaScore();
        DownloadScore(currentStartRange, currentStartRange + 9);
    }

    public void PreviousPage()
    {
        currentStartRange -= 11;
        if (currentStartRange < 0)
            currentStartRange = 0;
        else
            loadingPanel.SetActive(true);
        DownloadPersonaScore();
        DownloadScore(currentStartRange, currentStartRange + 9);
    }

    public void ShowTop10()
    {
        if(currentStartRange != 0)
            loadingPanel.SetActive(true);
        currentStartRange = 0;
        DownloadPersonaScore();
        DownloadScore(currentStartRange, currentStartRange + 9);
    }

    private void FindLeaderboard()
    {
        if (!SteamManager.Initialized)
        {
            noConnectionPanel.SetActive(true);
            return;
        }
        noConnectionPanel.SetActive(false);
        SteamAPICall_t steamAPICall = SteamUserStats.FindLeaderboard("Scoreboard");
        callResultFind.Set(steamAPICall);
    }

    public void UploadScore()
    {
        if (currentSteamLeaderboard == null || !SteamManager.Initialized)
        {
            //Debug.Log("No leaderboard found, please try again");
            FindLeaderboard();
            GameControl.instance.ChangeUploadButtonText("ERROR, TRY AGAIN");
            return;
        }

        SteamManager.Instance.UnlockAchievement("SCORE_UPLOAD");
        GameControl.instance.DisableUploadButton();
        GameControl.instance.ChangeUploadButtonText("UPLOADING...");
        GameControl.instance.SetSelectedToPlayAgain();
        scoreUploadTimer = 0;
        uploaded = false;
        scoresToDisplay[0] = GameControl.instance.GetTotalScore();

        SteamAPICall_t steamAPICall = SteamUserStats.UploadLeaderboardScore(currentSteamLeaderboard, ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodKeepBest, 
            GameControl.instance.GetTotalScore(), GameControl.instance.GetEndGameDetails(), 2);
    }

    private void OnUploadScore(LeaderboardScoreUploaded_t callback, bool bIOFailure)
    {
        if (callback.m_bSuccess != 1 || bIOFailure)
        {
            Debug.Log("Failed to upload score");
            return;
        }

        GameControl.instance.ChangeUploadButtonText("SCORE UPLOADED");
    }

    private void DownloadScore(int startRange, int endRange)
    {
        if (!SteamManager.Initialized)
            return;
        if (currentSteamLeaderboard == null)
        {
            Debug.Log("No leaderboard found, please try again");
            FindLeaderboard();
            return;
        }
        
        SteamAPICall_t steamAPICall = SteamUserStats.DownloadLeaderboardEntries(currentSteamLeaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, startRange, endRange+1);
        callResultDownload.Set(steamAPICall);
    }

    private void DownloadPersonaScore()
    {
        if (!SteamManager.Initialized)
            return;
        if (currentSteamLeaderboard == null)
        {
            Debug.Log("No leaderboard found, please try again");
            FindLeaderboard();
            return;
        }

        CSteamID[] steamID = new CSteamID[1];
        steamID[0] = SteamUser.GetSteamID();
        SteamAPICall_t steamAPICall = SteamUserStats.DownloadLeaderboardEntriesForUsers(currentSteamLeaderboard, steamID, 1);
        callResultPersonaDownload.Set(steamAPICall);
    }

    private void OnPersonaScoreDownload(LeaderboardScoresDownloaded_t callback, bool bIOFailure)
    {
        if (!bIOFailure)
        {
            if(callback.m_cEntryCount == 0)
            {
                namesToDisplay[0] = "NO SAVED HIGH SCORE";
                scoresToDisplay[0] = -1;
                ranksToDisplay[0] = -1;
                levelsToDisplay[0] = -1;
                finishStatusToDisplay[0] = -1;
                //Debug.Log("No entry found for the player");
                return;
            }
            else
            {
                currentPlayerEntries_t = callback.m_hSteamLeaderboardEntries;
                currentPlayerEntries = new LeaderboardEntry_t[callback.m_cEntryCount];

                SteamManager.Instance.UnlockAchievement("SCORE_UPLOAD");

                int[] details = new int[2];
                for (int i = 0; i < callback.m_cEntryCount; i++)
                {
                    SteamUserStats.GetDownloadedLeaderboardEntry(currentPlayerEntries_t, i, out currentPlayerEntries[i], details, 2);
                    
                    namesToDisplay[0] = SteamFriends.GetFriendPersonaName(currentPlayerEntries[i].m_steamIDUser);
                    scoresToDisplay[0] = currentPlayerEntries[i].m_nScore;
                    ranksToDisplay[0] = currentPlayerEntries[i].m_nGlobalRank;
                    levelsToDisplay[0] = details[0];
                    finishStatusToDisplay[0] = details[1];
                }
            }
            UpdateTexts();
            loadingPanel.SetActive(false);
        }
    }

    private void OnDownloadScore(LeaderboardScoresDownloaded_t callback, bool bIOFailure)
    {
        if (!bIOFailure)
        {
            currentLeaderboardEntryCount = callback.m_cEntryCount;
            
            currentLeaderboardEntries_t = callback.m_hSteamLeaderboardEntries;
            int completedEntryCount = 0;

            for (int i = 0; i < currentLeaderboardEntryCount; i++)
            {
                int[] details = new int[2];
                SteamUserStats.GetDownloadedLeaderboardEntry(currentLeaderboardEntries_t, i, out currentLeaderboardEntries[i], details, 2);
                ranksToDisplay[i + 1] = currentLeaderboardEntries[i].m_nGlobalRank;
                namesToDisplay[i + 1] = SteamFriends.GetFriendPersonaName(currentLeaderboardEntries[i].m_steamIDUser);
                scoresToDisplay[i + 1] = currentLeaderboardEntries[i].m_nScore;
                levelsToDisplay[i + 1] = details[0];
                finishStatusToDisplay[i + 1] = details[1];

                completedEntryCount++;
            }
            
            for (int i = 10; i > completedEntryCount; i--)
            {
                ranksToDisplay[i] = -1;
                namesToDisplay[i] = "";
                scoresToDisplay[i] = -1;
                levelsToDisplay[i] = -1;
                finishStatusToDisplay[i] = -1;
            }

            UpdateTexts();
            loadingPanel.SetActive(false);
        }
    }

    private void OnFindLeaderboard(LeaderboardFindResult_t callback, bool bIOFailure)
    {
        if (callback.m_bLeaderboardFound != 1 || bIOFailure)
        {
            //Debug.Log("Leaderboard not found");
            return;
        }

        currentSteamLeaderboard = callback.m_hSteamLeaderboard;
        DownloadPersonaScore();
        //Debug.Log("Got the leaderboard!");
    }

    public void DownloadNUpdate()
    {
        DownloadPersonaScore();
        DownloadScore(currentStartRange, currentStartRange + 9);
    }

    // paste current data to texts
    private void UpdateTexts()
    {
        for (int i = 0; i < 11; i++)
        {
            playerRankTexts[i].text = ranksToDisplay[i].ToString();
            playerNameTexts[i].text = namesToDisplay[i];
            playerScoreTexts[i].text = scoresToDisplay[i].ToString();
            playerLevelTexts[i].text = levelsToDisplay[i].ToString();
            if (finishStatusToDisplay[i] == 1)
                playerFinishedTexts[i].text = "YES";
            else if (finishStatusToDisplay[i] == 0)
                playerFinishedTexts[i].text = "NO";
            else
                playerFinishedTexts[i].text = "";
            if (ranksToDisplay[i] < 0)
                playerRankTexts[i].text = "";
            if (scoresToDisplay[i] < 0)
                playerScoreTexts[i].text = "";
            if (levelsToDisplay[i] < 0)
                playerLevelTexts[i].text = "";
        }
    }

    private void Update()
    {
        scoreUploadTimer += Time.deltaTime;
        if(!uploaded && scoreUploadTimer > 1)
        {
            uploaded = true;
            GameControl.instance.ChangeUploadButtonText("SCORE UPLOADED");
        }
    }
}
