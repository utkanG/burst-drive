﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.EventSystems;
using Steamworks;

public class GameControl : MonoBehaviour {

    public static GameControl instance;

    public int startLevel = 1;

    public delegate void PauseGame();
    public static event PauseGame OnPaused;
    public delegate void UnPauseGame();
    public static event PauseGame OnUnpaused;

    public bool infiniteLives;

    public float gameOverMenuTimer;
    private bool gameOver;
    public bool timeSlowed;
    public int timeSlowedBy; // 1 or 2, 0 to override
    private float slowTimer;

    [SerializeField]
    private GameObject[] slowTimeObjects;
    [SerializeField]
    private Text uploadeScoreButtonText;

    private float targetTimeScale = 1;

    [Serializable]
    public class ResolutionU
    {
        public int width;
        public int height;
        public int refreshRate;

        public Resolution ToResolution()
        {
            Resolution r = new Resolution();
            r.width = width;
            r.height = height;
            r.refreshRate = refreshRate;

            return r;
        }

        public static ResolutionU ToResolutionU(Resolution r)
        {
            return new ResolutionU(r.width, r.height, r.refreshRate);
        }

        public ResolutionU(int w, int h, int rr)
        {
            width = w;
            height = h;
            refreshRate = rr;
        }
    }
    

    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);

        //Save();
        Load();

        //player1Object = FindObjectOfType<PlayerShip>().gameObject;
        //player1Script = player1Object.GetComponent<PlayerShip>();

        //DontDestroyOnLoad(gameObject);
        Application.targetFrameRate = -1;
    }

    private void UpdateTimeSlow()
    {
        if (!paused)
        {
            if (slowTimer >= 0)
                Time.timeScale = Mathf.Lerp(Time.timeScale, targetTimeScale, Time.unscaledDeltaTime * 2);
            else if (Time.timeScale != 1)
                Time.timeScale = Mathf.Lerp(Time.timeScale, 1, Time.unscaledDeltaTime * 5);
        }
    }

    public void SlowTime(float duration, float timeScale)
    {
        slowTimer = duration;
        timeSlowed = true;
        targetTimeScale = timeScale;
        timeSlowedBy = 0;
        //slowTimeObjects[playerNo - 1].SetActive(true);
    }

    public void SlowTime(float duration, float timeScale, int playerNo)
    {
        if (!LevelManager.instance.level0Active)
        {
            slowTimer = duration;
            timeSlowed = true;
            targetTimeScale = timeScale;
            timeSlowedBy = playerNo;
            slowTimeUI[playerNo - 1].SetActive(true);
            //slowTimeObjects[playerNo - 1].SetActive(true);
            if(playerNo == 0)
                Invoke("NormalizeTime", duration);
        }
    }

    public void NormalizeTime()
    {
        if (!LevelManager.instance.level0Active)
        {
            //slowTimeObjects[0].SetActive(false);
            //slowTimeObjects[1].SetActive(false);
            slowTimer = 0;
            timeSlowedBy = 0;
            slowTimeUI[0].SetActive(false);
            slowTimeUI[1].SetActive(false);
        }
    }

    public void NormalizeTime(bool useThisToOverride)
    {
        //slowTimeObjects[0].SetActive(false);
        //slowTimeObjects[1].SetActive(false);
        slowTimer = 0;
        timeSlowedBy = 0;
    }

    public GameObject player1Object;
    public GameObject player2Object;
    public PlayerShip player1Script;
    public PlayerShip player2Script;
    public GameObject[] playerPanels;
    public GameObject[] playerScores;
    public float[] counters;
    public bool[] canSpawnOver;
    public Text[] counterTexts;
    public GameObject[] scoreOverwriteWarnings;
    public Text[] pressToFireTexts;
    public int player1Lives;
    public int player2Lives;
    public int p1Score;
    public int p2Score;

    public bool bossAlive;
    public int bossKillCount;
    public bool gameOn;
    public bool paused;
    private bool pressedPlay;

    [SerializeField]
    private GameObject gameOverMenu;
    public Text p1ScoreTextGameOver;
    public Text p2ScoreTextGameOver;
    public Text p1ScoreText;
    public Text p2ScoreText;
    public Text p1PineAppleTextGameOver;
    public Text p2PineAppleTextGameOver;
    public Text p1LevelTextGameOver;
    public Text p2LevelTextGameOver;
    public Text totalScoreText;
    public GameObject[] slowTimeUI;

    private float spawnTimer1;
    private float spawnTimer2;
    [SerializeField]
    private MenuUIControl menuSettings;

    [Space(10)]
    public int playerCount; // 1 or 2
    public int p1c;
    public int p2c;
    private float startGameTimer;
    public bool shopOpenedOnce;

    [SerializeField]
    ScoreboardControl scoreboardControl;
    [SerializeField]
    Button uploadScoreButton;
    [SerializeField]
    GameObject playAgainButtonObject;

    //public SettingsProfile settingsProfile;

    private void Start()
    {
        menuSettings = FindObjectOfType<MenuUIControl>();
        OnUnpaused();
        pressToFireTexts[0].gameObject.SetActive(false);
        pressToFireTexts[1].gameObject.SetActive(false);
        scoreOverwriteWarnings[0].gameObject.SetActive(false);
        scoreOverwriteWarnings[1].gameObject.SetActive(false);
        playerPanels[0].SetActive(false);
        playerPanels[1].SetActive(false);
        PickupManager.instance.peScript.enabled = false;
        player1Script.Awake();
        player2Script.Awake();
        startGameTimer = 0;
        pressedPlay = false;
        bossAlive = false;
        bossKillCount = 0;
    }

    public void Pause()
    {
        if (!paused)
        {
            OnPaused();
            paused = true;
            player1Script.particleBeam.SetActive(false);
            player2Script.particleBeam.SetActive(false);
            Time.timeScale = 0;
            //Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            paused = false;
            OnUnpaused();
            Time.timeScale = 1;
            //
            ;
            Cursor.visible = false;
        }
    }

    public void AddLife(int amount, string playerNo)
    {
        if (infiniteLives)
            return;

        if(playerNo == "1")
        {
            if(player1Lives > 4)
            {
                PickupManager.instance.SpawnPickUp(22, transform.position);
                PickupManager.instance.SpawnPickUp(22, transform.position);
                return;
            }
            if (amount < 0)
            {
                if (player1Lives > 0)
                    player1Lives += amount;
                return;
            }
            else if (player1Lives < 4)
            {
                player1Lives += amount;
                return;
            }
        }
        else if(playerNo == "2")
        {
            if(player2Lives > 4)
            {
                PickupManager.instance.SpawnPickUp(22, transform.position);
                PickupManager.instance.SpawnPickUp(22, transform.position);
                return;
            }
            if (amount < 0)
            {
                if (player2Lives > 0)
                    player2Lives += amount;
                return;
            }
            else if (player2Lives < 4)
            {
                player2Lives += amount;
                return;
            }
        }
    }

    public void SetSpawnTimer(float seconds, string playerNo)
    {
        if (playerNo == "1")
            spawnTimer1 = seconds;
        else if (playerNo == "2")
            spawnTimer2 = seconds;
    }

    private void CheckContinue()
    {
        if (p1c != 1 && p2c != 1 && gameOn)
        {
            ActivateGameOverMenu(1);
            pressToFireTexts[0].gameObject.SetActive(false);
            scoreOverwriteWarnings[0].SetActive(false);
            pressToFireTexts[1].gameObject.SetActive(false);
            scoreOverwriteWarnings[1].SetActive(false);
            return;
        }
        if(p1c != 1)
        {
            if(counters[0] > 0 && canSpawnOver[0])
            {
                if (!player1Script.controllerInput)
                {
                    if (Input.GetKeyDown(player1Script.keyCodes[2]))
                    {
                        ResetPlayer1();
                        p1c = 1;
                        playerCount++;
                    }
                }
                else
                {
                    if (Input.GetKeyDown(player1Script.keyCodes[5]))
                    {
                        ResetPlayer1();
                        p1c = 1;
                        playerCount++;
                    }
                }
                counters[0] -= Time.deltaTime;
                counterTexts[0].text = ((int)counters[0]).ToString();
            }
            if (counters[0] <= 0)
            {
                pressToFireTexts[0].gameObject.SetActive(false);
                canSpawnOver[0] = false;
            }
        }
        if (p2c != 1)
        {
            if (counters[1] > 0 && canSpawnOver[1])
            {
                if (!player2Script.controllerInput)
                {
                    if (Input.GetKeyDown(player2Script.keyCodes[2]))
                    {
                        ResetPlayer2();
                        p2c = 1;
                        playerCount++;
                    }
                }
                else
                {
                    if (Input.GetKeyDown(player2Script.keyCodes[5]))
                    {
                        ResetPlayer2();
                        p2c = 1;
                        playerCount++;
                    }
                }
                counters[1] -= Time.deltaTime;
                counterTexts[1].text = ((int)counters[1]).ToString();
            }
            if(counters[1] <= 0)
            {
                pressToFireTexts[1].gameObject.SetActive(false);
                canSpawnOver[1] = false;
            }
        }
    }

    private void Update()
    {
        if (gameOn || gameOver ||pressedPlay)
        {
            CheckContinue();
            if (Input.GetButtonDown("Pause") && !LevelManager.instance.shopActive)
                Pause();
        }
        if (gameOn)
        {
            spawnTimer1 -= Time.deltaTime;
            spawnTimer2 -= Time.deltaTime;
            if (spawnTimer1 < 0 && !player1Object.activeInHierarchy && player1Lives > 0)
            {
                RespawnPlayer("1");
            }
            if (spawnTimer2 < 0 && !player2Object.activeInHierarchy && player2Lives > 0)
            {
                RespawnPlayer("2");
            }

            if(playerCount <=0)
            {
                ActivateGameOverMenu();
            }
        }
        else if (gameOver)
        {
            gameOverMenuTimer -= Time.deltaTime;
            
            if(gameOverMenuTimer < 0)
            {
                //Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                gameOverMenu.SetActive(true);
                gameOver = false;
                scoreOverwriteWarnings[0].SetActive(false);
                scoreOverwriteWarnings[1].SetActive(false);
            }
        }

        if(timeSlowed)
        {
            slowTimer -= Time.deltaTime;
            UpdateTimeSlow();
        }

        if (pressedPlay)
        {
            startGameTimer -= Time.deltaTime;
            if (startGameTimer <= 0)
            {
                ActuallyStartTheGame();
                pressedPlay = false;
            }
        }
    }

    private void ActuallyStartTheGame()
    {
        bossAlive = false;
        bossKillCount = 0;
        gameOn = true;
        player1Object.SetActive(true);
        canSpawnOver[1] = true;
        pressToFireTexts[1].gameObject.SetActive(true);
        playerPanels[0].SetActive(true);
        playerPanels[1].SetActive(false);
        playerScores[0].SetActive(true);
        playerScores[1].SetActive(false);
    }

    public void StartGame(int pCount)
    {
        pressedPlay = true;
        startGameTimer = 0f; // TODO 2.5F
        LevelManager.instance.BackToNormal();
        LevelManager.instance.Looping = 0;

        if (infiniteLives)
        {
            player1Lives = 4;
            player2Lives = 4;
        }

        TutorialControl.instance.ResetTuto();
        LevelManager.instance.level0Ready = true;
        CameraShaker.instance.shakeDuration = 0;
        LevelManager.instance.currentLevelNo = startLevel;
        EnemyManager.instance.hpSlider.transform.parent.gameObject.SetActive(false);
        player1Script.ExitSecretDrunk();
        player2Script.ExitSecretDrunk();
        PickupManager.instance.peScript.enabled = false;

        p1c = 1;
        //p2c = 1;
        paused = true;
        
        player1Lives = 2;
        player2Lives = 0;
        
        gameOverMenuTimer = 0;
        gameOverMenu.SetActive(false);
        gameOver = false;

        paused = false;

        LevelManager.instance.Start();

        Camera.main.transform.position = Vector3.zero - Vector3.forward * 10;
        LevelManager.instance.CleanUpLevel();

        playerCount = pCount;
        
        p1Score = 0;
        p2Score = 0;
        
        player1Script.uiConnector.UpdateScore(0,1); 
        player2Script.uiConnector.UpdateScore(0,2);
        player1Script.ReadyForGameStart();
        player1Script.ResetPlayer();
        player2Script.ResetPlayer();
        player2Script.ReadyForGameStart();
        scoreOverwriteWarnings[0].SetActive(false);
        scoreOverwriteWarnings[1].SetActive(false);
        slowTimeUI[0].SetActive(false);
        slowTimeUI[1].SetActive(false);
        counters[1] = 10;
        //TutorialControl.instance.ResetTuto();
        //TutorialControl.instance.ActivateSkipTuto();
        //TutorialControl.instance.ActivateIntroText();
        //Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        pressToFireTexts[0].gameObject.SetActive(false);
        pressToFireTexts[1].gameObject.SetActive(false);
    }

    public void ResetPlayer1()
    {
        slowTimeUI[0].SetActive(false);
        //player1Object.transform.position = Vector3.zero;
        player1Script.ResetPlayer();
        p1Score = 0;
        p1ScoreText.text = " 0 ";
        gameOn = true;
        gameOver = false;
        gameOverMenuTimer = 0;
        player1Script.gemCount = 0;
        player1Script.pineappleCount = 0;
        player1Lives = 2;
        player1Object.SetActive(true);
        playerPanels[0].SetActive(true);
        playerScores[0].SetActive(true);
        player1Script.lifeLetters = new bool[5];
        player1Script.shieldLetters = new bool[5];
        player1Script.uiConnector.UpdateScore(0, 1);
        player1Script.droneCount = 0;
        player1Script.OnEnable();
        player1Script.laserSight = false;
    }

    public void ResetPlayer2()
    {
        slowTimeUI[1].SetActive(false);
        //player2Object.transform.position = Vector3.zero;
        player2Script.ResetPlayer();
        p2Score = 0;
        p2ScoreText.text = " 0 ";
        gameOn = true;
        gameOver = false;
        gameOverMenuTimer = 0;
        player2Script.gemCount = 0; 
        player2Script.pineappleCount = 0;
        player2Lives = 2;
        player2Object.SetActive(true);
        playerPanels[1].SetActive(true);
        playerScores[1].SetActive(true);
        player2Script.lifeLetters = new bool[5];
        player2Script.shieldLetters = new bool[5];
        player2Script.droneCount = 0;
        player2Script.uiConnector.UpdateScore(0, 1);
        player2Script.OnEnable();
        player2Script.laserSight = false;
    }

    public void EndGame()
    {
        startGameTimer = 0;
        instance.player1Script.drunk = false;
        instance.player2Script.drunk = false;
        pressToFireTexts[1].gameObject.SetActive(false);
        pressToFireTexts[0].gameObject.SetActive(false);
        paused = false;
        gameOn = false;
        pressedPlay = false;
        player1Script.laserSight = false;
        player2Script.laserSight = false;
        LevelManager.instance.CleanUpLevel();
        //if (!player1Object.activeInHierarchy)
        //{
        //    player1Object.SetActive(true);
        //    player1Object.transform.position = Vector3.up * -12.5f;
        //}
        //if (!player2Object.activeInHierarchy)
        //{
        //    player2Object.SetActive(true);
        //    player2Object.transform.position = Vector3.up * -12.5f;
        //}
        //Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        playerPanels[0].SetActive(false);
        playerPanels[1].SetActive(false);
        playerScores[0].SetActive(false);
        playerScores[1].SetActive(false);
        EnemyManager.instance.hpSlider.transform.parent.gameObject.SetActive(false);
        TutorialControl.instance.EndTuto();
        player1Script.ExitPhase();
        player2Script.ExitPhase();
        player1Script.hull.invulnerable = false;
        player1Script.hull.invulnerable = false;
        player1Script.hull.Damage(100, 0);
        player2Script.hull.Damage(100, 0);
        player1Object.transform.position = Vector3.up * 1000;
        player2Object.transform.position = Vector3.up * 1000;
        p1c = 0;
        p2c = 0;

    }
    
    public void RespawnPlayer(string playerNo)
    {
        if(playerNo == "1")
        {
            AddLife(-1, "1");
            player1Script.ReducePower();
            player1Object.SetActive(true);
            //player1Object.transform.position = Vector3.zero;
            player1Object.transform.rotation = Quaternion.identity;
            slowTimeUI[0].SetActive(false);
        }
        else if(playerNo == "2")
        {
            AddLife(-1, "2");
            player2Script.ReducePower();
            player2Object.SetActive(true);
            //player2Object.transform.position = Vector3.zero;
            player2Object.transform.rotation = Quaternion.identity;
            slowTimeUI[1].SetActive(false);
        }
    }

    public void ActivateGameOverMenu()
    {
        EventSystem.current.SetSelectedGameObject(playAgainButtonObject);
        gameOverMenuTimer = 11f;
        gameOn = false;
        gameOver = true;
        p1PineAppleTextGameOver.text = player1Script.pineappleCount.ToString() + "x1000";
        p2PineAppleTextGameOver.text = player2Script.pineappleCount.ToString() + "x1000";
        p1LevelTextGameOver.text = player1Script.currentLevel.ToString() + "x1000";
        p2LevelTextGameOver.text = player2Script.currentLevel.ToString() + "x1000";
        p1Score = p1Score + player1Script.pineappleCount * 1000 + player1Script.currentLevel * 1000;
        p2Score = p2Score + player2Script.pineappleCount * 1000 + player2Script.currentLevel * 1000;
        p1ScoreTextGameOver.text = p1Score.ToString();
        p2ScoreTextGameOver.text = p2Score.ToString();
        totalScoreText.text = GetTotalScore().ToString();

        if (scoreboardControl.scoresToDisplay[0] > GetTotalScore())
        {
            ChangeUploadButtonText("NOT YOUR HIGHEST");
            uploadScoreButton.interactable = false;
        }
        else
        {
            ChangeUploadButtonText("UPLOAD HIGH SCORE");
            uploadScoreButton.interactable = true;
        }

        if (!SteamManager.Initialized)
        {
            ChangeUploadButtonText("NO CONNECTION");
            uploadScoreButton.interactable = false;
        }
    }

    public void ActivateGameOverMenu(float seconds)
    {
        EventSystem.current.SetSelectedGameObject(playAgainButtonObject);
        gameOverMenuTimer = seconds;
        gameOn = false;
        gameOver = true;
        p1PineAppleTextGameOver.text = player1Script.pineappleCount.ToString() + "x1000";
        p2PineAppleTextGameOver.text = player2Script.pineappleCount.ToString() + "x1000";
        p1LevelTextGameOver.text = player1Script.currentLevel.ToString() + "x1000";
        p2LevelTextGameOver.text = player2Script.currentLevel.ToString() + "x1000";
        p1Score = p1Score + player1Script.pineappleCount * 1000 + player1Script.currentLevel * 1000;
        p2Score = p2Score + player2Script.pineappleCount * 1000 + player2Script.currentLevel * 1000;
        p1ScoreTextGameOver.text = p1Score.ToString();
        p2ScoreTextGameOver.text = p2Score.ToString();
        totalScoreText.text = GetTotalScore().ToString();

        if(SteamManager.Initialized)
            SteamFriends.SetRichPresence("steam_display", "Game Over - Score: " + GetTotalScore());

        if (scoreboardControl.scoresToDisplay[0] > GetTotalScore())
        {
            ChangeUploadButtonText("NOT YOUR HIGHEST");
            uploadScoreButton.interactable = false;
        }
        else
        {
            ChangeUploadButtonText("UPLOAD HIGH SCORE");
            uploadScoreButton.interactable = true;
        }

        if(!SteamManager.Initialized)
        {
            ChangeUploadButtonText("NO CONNECTION");
            uploadScoreButton.interactable = false;
        }
    }

    public void DisableUploadButton()
    {
        uploadScoreButton.interactable = false;

    }

    public void SetSelectedToPlayAgain()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(playAgainButtonObject);
    }

    public void ChangeUploadButtonText(string s)
    {
        uploadeScoreButtonText.text = s;
    }

    public int GetTotalScore()
    {
        return p1Score + p2Score;
    }

    public int[] GetEndGameDetails()
    {
        int[] details = new int[2];
        if (player1Script.currentLevel > player2Script.currentLevel)
            details[0] = player1Script.currentLevel;
        else
            details[0] = player2Script.currentLevel;

        if (player1Script.finishedGame)
            details[1] = 1;
        else
            details[1] = 0;

        return details;
    }

    [Serializable]
    class UserData
    {
        public float musicVolume;
        public float soundVolume;
        public ResolutionU resolution;
        public int screenModeIndex;
        public bool bloomOn;
        public bool cameraShakeOn;
        public bool[] playerControllerInputs;
        public int[] playerControllerIndices;
        public KeyCode[] player1Bindings;
        public KeyCode[] player2Bindings;
        public FullScreenMode screenMode;
        public bool shopOpenedOnce;

        //TODO
        // level, weapon, firerate, cooling, dimensional, absolute

        public UserData()
        {
            playerControllerIndices = new int[2];
            playerControllerInputs = new bool[2];
            player1Bindings = new KeyCode[8];
            player2Bindings = new KeyCode[8];
            resolution = ResolutionU.ToResolutionU(Screen.currentResolution);
            shopOpenedOnce = false;
        }
    }

    public void Save()
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/userData.pine", FileMode.Create);

        UserData data = new UserData();

        data.musicVolume = menuSettings.musicVol;
        data.soundVolume = menuSettings.soundVol;
        data.resolution = ResolutionU.ToResolutionU(Screen.currentResolution);
        data.screenModeIndex = menuSettings.currentScreenModeIndex;
        data.bloomOn = menuSettings.ppprofile.bloom.enabled;
        data.cameraShakeOn = menuSettings.camShaker.enabled;
        data.playerControllerInputs[0] = player1Script.controllerInput;
        data.playerControllerInputs[1] = player2Script.controllerInput;
        data.playerControllerIndices[0] = player1Script.controllerIndex;
        data.playerControllerIndices[1] = player2Script.controllerIndex;
        data.screenMode = menuSettings.currentScreenMode;
        data.shopOpenedOnce = shopOpenedOnce;
        
        for (int i = 0; i < 8; i++)
        {
            data.player1Bindings[i] = player1Script.keyCodes[i];
            data.player2Bindings[i] = player2Script.keyCodes[i];
        }

        binaryFormatter.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/userData.pine"))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/userData.pine", FileMode.Open);
            UserData data = (UserData)binaryFormatter.Deserialize(file);
            file.Close();

            menuSettings.musicVol = data.musicVolume;
            menuSettings.soundVol = data.soundVolume;
            menuSettings.selectedResolution = data.resolution.ToResolution();
            menuSettings.currentScreenModeIndex = data.screenModeIndex;
            menuSettings.ppprofile.bloom.enabled = data.bloomOn;
            menuSettings.camShaker.enabled = data.cameraShakeOn;
            player1Script.controllerInput = data.playerControllerInputs[0];
            player2Script.controllerInput = data.playerControllerInputs[1];
            player1Script.controllerIndex = data.playerControllerIndices[0];
            player2Script.controllerIndex = data.playerControllerIndices[1];
            menuSettings.currentScreenMode = data.screenMode;
            shopOpenedOnce = data.shopOpenedOnce;

            for (int i = 0; i < 8; i++)
            {
                player1Script.keyCodes[i] = data.player1Bindings[i];
                player2Script.keyCodes[i] = data.player2Bindings[i];
            }
        }
        else
        {
            menuSettings.musicVol = .6f;
            menuSettings.soundVol = .5f;
            menuSettings.selectedResolution = Screen.currentResolution;
            Save();
        }
    }
}
