﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : ScriptableObject
{
    [System.Serializable]
    public class PathElement
    {
        public Vector2[] positions;
        public float[] speeds;
        public List<Vector2> endPositions;
        public bool[] resetSpeed;
        public bool[] teleportToNext;
        public bool[] destroyAlien;
        public float[] velsAfterArrival;
        public float[] radiiOfCols;
        public bool loop;

        public PathElement(Path path)
        {
            int length = path.nodes.Length;

            positions = new Vector2[length];
            speeds = new float[length];
            resetSpeed = new bool[length];
            teleportToNext = new bool[length];
            destroyAlien = new bool[length];
            radiiOfCols = new float[length];
            velsAfterArrival = new float[length];
            loop = path.loop;

            for (int i = 0; i < length; i++)
            {
                positions[i] = path.nodes[i].transform.position;
                speeds[i] = path.nodes[i].speedMultiplier;
                endPositions = path.endPosScript.endPositions;
                resetSpeed[i] = path.nodes[i].resetVelocity;
                teleportToNext[i] = path.nodes[i].teleportToNext;
                destroyAlien[i] = path.nodes[i].destroyAlien;
                velsAfterArrival[i] = path.nodes[i].velocityAfterArrival;
                radiiOfCols[i] = path.nodes[i].radiusOfCol;
            }
        }
    }

    [System.Serializable]
    public class EnemyPositionElement
    {
        public Vector2 position;
        public int poolIndex;
        public int pathIndex;

        public EnemyPositionElement(Alien alien, int paIn)
        {
            position = alien.transform.position;
            poolIndex = alien.alienData.poolIndex;
            pathIndex = paIn;
        }

        public EnemyPositionElement(Alien alien)
        {
            position = alien.transform.position;
            poolIndex = alien.alienData.poolIndex;
            pathIndex = -1;
        }
    }

    [System.Serializable]
    public class SpecialAlienElement
    {
        public Vector2 position;
        public int poolIndex;

        public SpecialAlienElement(SpecialAlien alien) // create special alien script
        {
            position = alien.transform.position;
            poolIndex = alien.specialAlienData.poolIndex;
        }
    }
    
    public int levelNo = 1;
    public int maxProjectiles = 4;
    public int maxPickups = 10;
    public int bigAsteroidCount = 0;
    public int smallAsteroidCount = 0;
    public bool riftAppears = false;
    public List<PathElement> pathElements;
    public List<EnemyPositionElement> enemyPositionElements;
    public List<SpecialAlienElement> specialAlienElements;
    
    public void Initialize(List<Path> paths, List<Alien> aliens, List<SpecialAlien> sAliens)
    {
        pathElements = new List<PathElement>();
        enemyPositionElements = new List<EnemyPositionElement>();
        specialAlienElements = new List<SpecialAlienElement>();
        EnemyPositionElement alienElement;

        // temp list for obtaining path indexes
        List<Path> tempPathObjects = new List<Path>();
        int currentPathIndex = 0;

        for (int i = 0; i < aliens.Count; i++)
        {
            if (!aliens[i].path)
            {
                alienElement = new EnemyPositionElement(aliens[i]);
                enemyPositionElements.Add(alienElement);
                continue;
            }
            if(tempPathObjects.Contains(aliens[i].path))
            {
                currentPathIndex = tempPathObjects.IndexOf(aliens[i].path);
            }
            else
            {
                tempPathObjects.Add(aliens[i].path);
                currentPathIndex = tempPathObjects.Count - 1;

                PathElement pathElement = new PathElement(aliens[i].path);
                pathElements.Add(pathElement);
            }

            alienElement = new EnemyPositionElement(aliens[i], currentPathIndex);
            enemyPositionElements.Add(alienElement);
        }

        for (int i = 0; i < sAliens.Count; i++)
        {
            SpecialAlienElement specialAlienElement = new SpecialAlienElement(sAliens[i]);
            specialAlienElements.Add(specialAlienElement);
        }
    }
}
