﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rift : MonoBehaviour {

    public float lifeTime = 8;
    private float lifeTimer;
    private float speed = 3;
    public int enemyCount = 5;
    private int initialEnemyCount = 5;

    private float spawnTimer = 0;
    private int spawnIndex = 0;

    private Vector3 randomDirection;
    private Animator anim;

    private int changeDimension = 0;

    public LayerMask playerMask;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        SoundManager.instance.PlayFXSound(13, 1, 1, 4);
        speed = Random.Range(0f, .5f);
        randomDirection = Random.insideUnitCircle.normalized;
        SetLifeTime(8);
        anim.ResetTrigger("shutdown");
        spawnTimer = 3;
        initialEnemyCount = enemyCount;
        changeDimension = 0;
    }

    public void SetEnemyCount(int count)
    {
        enemyCount = Mathf.Clamp(count, 0, 150);
        initialEnemyCount = enemyCount;
    }

    public void SetLifeTime(float duration)
    {
        lifeTime = duration;
        lifeTimer = 0;
    }

    public void SetSpawnIndex(int index)
    {
        spawnIndex = index;
    }

    private void Update()
    {
        lifeTimer += Time.deltaTime;
        transform.position += Time.deltaTime * randomDirection * speed;
        
        if(lifeTimer >= lifeTime)
        {
            anim.SetTrigger("shutdown");
        }
        if(lifeTimer >= lifeTime + .15f)
        {
            GameObject e = ExplosionPooler.instance.GetExplosion(43);
            if (CheckForPlayers())
            {
                e = ExplosionPooler.instance.GetExplosion(65);
                e.transform.position = Vector3.zero;
                e.SetActive(true);
                LevelManager.instance.ClearEnemies();
                LevelManager.instance.ToOtherDimension();
            }
            e = ExplosionPooler.instance.GetExplosion(43);
            ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
            e.transform.position = transform.position;
            e.SetActive(true);
            EnemyManager.instance.enemyCount--;
            gameObject.SetActive(false);
        }

        spawnTimer -= Time.deltaTime;
        if(spawnTimer <= 0 && lifeTimer > 3 && lifeTimer < lifeTime - 1.5f && enemyCount > 0)
        {
            SpawnEnemy(spawnIndex);
        }
    }

    public void Ripple()
    {
        SoundManager.instance.PlayFXSound(4, 1.5f, 1f, 2);
        SoundManager.instance.PlayFXSound(8, 1.5f, 1f, 2);
        ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
    }

    private void SpawnEnemy(int index)
    {
        spawnTimer = 4f/initialEnemyCount;

        SoundManager.instance.PlayFXSound(11, .3f, .25f, 1);
        GameObject alienObject = EnemyManager.instance.GetAlien(index);
        Alien alien = alienObject.GetComponent<Alien>();
        EnemyManager.instance.AddAlien(alien.hull);

        alien.path = null;
        alienObject.transform.position = Random.insideUnitCircle.normalized * 4.1f + (Vector2)transform.position;
        alienObject.SetActive(true);
        alien.SetNextPosition(transform.position);
        enemyCount--;
    }

    private bool CheckForPlayers()
    {
        if(Physics2D.OverlapCircle(transform.position, 3f, playerMask))
            return true;
        else
            return false;
    }
}
