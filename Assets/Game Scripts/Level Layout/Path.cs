﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Path : MonoBehaviour {

    public PathNode[] nodes;
    public EndPositions endPosScript;
    public bool loop;

    private void OnDrawGizmos()
    {
        if (nodes.Length > 1)
        {
            for (int i = 0; i < nodes.Length; i++)
            {
                if (i == 0)
                    Gizmos.color = Color.green;
                else if (i == nodes.Length - 1)
                    Gizmos.color = Color.red;
                else
                    Gizmos.color = Color.cyan;

                Gizmos.DrawCube(nodes[i].transform.position, Vector3.one / 2);

                if (i != 0)
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine(nodes[i - 1].transform.position, nodes[i].transform.position);
                }
            }
        }
    }

    public Vector2 GetLocation()
    {
        int rand = Random.Range(0, endPosScript.endPositions.Count);
        Vector2 vectorToReturn = endPosScript.endPositions[rand];
        endPosScript.endPositions.RemoveAt(rand);
        return vectorToReturn;
    }

    [ContextMenu("Get Path")]
    public void GetPath()
    {
        nodes = new PathNode[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            nodes[i] = transform.GetChild(i).gameObject.GetComponent<PathNode>();
            nodes[i].index = i;
        }
    }
}
