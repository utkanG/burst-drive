﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boundary : MonoBehaviour {

    public int boundaryIndex = 0;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.layer != 11)
        {
            TeleportObject(col.gameObject);
            return;
        }
    }

    private void TeleportObject(GameObject go)
    {
        switch (boundaryIndex)
        {
            case 0:
                go.transform.position = new Vector3(-go.transform.position.x - 1, go.transform.position.y);
                break;
            case 1:
                go.transform.position = new Vector3(-go.transform.position.x + 1, go.transform.position.y);
                break;
            case 2:
                go.transform.position = new Vector3(go.transform.position.x, -go.transform.position.y - 1);
                break;
            case 3:
                go.transform.position = new Vector3(go.transform.position.x, -go.transform.position.y + 1);
                break;
            default:
                break;
        }
    }
}
