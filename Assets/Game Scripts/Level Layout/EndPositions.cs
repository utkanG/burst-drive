﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EndPositions : MonoBehaviour {

    public List<Vector2> endPositions;

    [ContextMenu("Get Positions")]
    public void GetPositions()
    {
        endPositions.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            endPositions.Add(transform.GetChild(i).position);
        }
    }

    private void Awake()
    {
        GetPositions();
    }
}
