﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathNode : MonoBehaviour {

    public int index;
    public float speedMultiplier = 1;
    public bool resetVelocity = false;
    public bool teleportToNext = false;
    public bool destroyAlien = false;
    public float velocityAfterArrival = 0;
    public float radiusOfCol = 2;

    public CircleCollider2D col;

    private void Awake()
    {
        col = GetComponent<CircleCollider2D>();
        col.radius = radiusOfCol;
    }

    private void OnEnable()
    {
        col.radius = radiusOfCol;
    }
}
