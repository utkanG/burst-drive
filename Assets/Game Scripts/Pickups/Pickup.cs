﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class Pickup : MonoBehaviour {

    public PickupData data;

    protected float lifeTime;
    protected float currentLife;

    protected Vector3 direction;
    protected Rigidbody2D rb2d;
    private GameObject explosion;
    private Animator anim;

    private bool spriteWentOut;
    protected bool pickedUp;

    protected virtual void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    protected virtual void OnEnable()
    {
        lifeTime = Random.Range(10, 20);
        currentLife = lifeTime;
        //direction = (Vector3.zero - transform.position).normalized * Random.Range(0, 3f);
        rb2d.velocity = Random.insideUnitCircle * 1.5f;
        pickedUp = false;
    }

    protected void Update()
    {
        currentLife -= Time.deltaTime;
        //if (!PickupManager.instance.player1.accelerating)
        //{
        //    direction = (PickupManager.instance.player1.transform.position - transform.position).normalized;
        //    rb2d.velocity = direction * 2;
        //}
        CheckDistance();

        if (currentLife > 0)
            anim.SetFloat("speed", lifeTime / currentLife);
        if (currentLife < 0)
            EndLife();
    }

    private void CheckDistance()
    {
        if(!PickupManager.instance.player1.accelerating && GameControl.instance.player1Object.activeInHierarchy)
        {
            if ((PickupManager.instance.player1.transform.position - transform.position).sqrMagnitude < 10)
            {
                direction = (PickupManager.instance.player1.transform.position - transform.position).normalized;
                rb2d.velocity = direction * 2.5f;
            }
        }
        if (!PickupManager.instance.player2.accelerating && GameControl.instance.player2Object.activeInHierarchy)
        {
            if ((PickupManager.instance.player2.transform.position - transform.position).sqrMagnitude < 10)
            {
                direction = (PickupManager.instance.player2.transform.position - transform.position).normalized;
                rb2d.velocity = direction * 2.5f;
            }
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        if(pickedUp) return;
        if (col.CompareTag("Player1")) // TODO player1 and 2
        {
            PickupManager.instance.player1.uiConnector.ActivatePickUpText(data.name);
            SoundManager.instance.PlayPickupSound(data.poolIndex);
        }
        else if (col.CompareTag("Player2"))
        {
            PickupManager.instance.player2.uiConnector.ActivatePickUpText(data.name);
            SoundManager.instance.PlayPickupSound(data.poolIndex);
        }
        EndLife();
    }

    protected void EndLife()
    {
        explosion = ExplosionPooler.instance.GetExplosion(29);
        explosion.transform.position = transform.position;
        explosion.SetActive(true);
        PickupManager.instance.ReturnPickup(data.poolIndex, gameObject);
        pickedUp = true;
    }
}
