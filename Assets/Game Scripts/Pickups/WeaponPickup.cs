﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickup : Pickup {

    [SerializeField]
    string pickUpName;

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player1"))
        {
            switch (pickUpName)
            {
                case "molten":
                    if (PickupManager.instance.player1.currentWeapon == PlayerShip.Weapons.SINGLE_SHOT)
                        PickupManager.instance.SpawnPickUp(5, PickupManager.instance.player1.transform.position);
                    else
                        PickupManager.instance.player1.SetWeapon(PlayerShip.Weapons.SINGLE_SHOT);
                    break;
                case "double":
                    if (PickupManager.instance.player1.currentWeapon == PlayerShip.Weapons.GEMINI_BLAST)
                        PickupManager.instance.SpawnPickUp(5, PickupManager.instance.player1.transform.position);
                    else
                        PickupManager.instance.player1.SetWeapon(PlayerShip.Weapons.GEMINI_BLAST);
                    break;
                case "triple":
                    if (PickupManager.instance.player1.currentWeapon == PlayerShip.Weapons.TRIPLE_SHOT)
                        PickupManager.instance.SpawnPickUp(5, PickupManager.instance.player1.transform.position);
                    else
                        PickupManager.instance.player1.SetWeapon(PlayerShip.Weapons.TRIPLE_SHOT);
                    break;
                case "ion":
                    if (PickupManager.instance.player1.currentWeapon == PlayerShip.Weapons.ION_CANNON)
                        PickupManager.instance.SpawnPickUp(5, PickupManager.instance.player1.transform.position);
                    else
                        PickupManager.instance.player1.SetWeapon(PlayerShip.Weapons.ION_CANNON);
                    break;
                default:
                    break;
            }
        }
        else if (col.CompareTag("Player2"))
        {
            switch (pickUpName)
            {
                case "molten":
                    if (PickupManager.instance.player2.currentWeapon == PlayerShip.Weapons.SINGLE_SHOT)
                        PickupManager.instance.SpawnPickUp(5, PickupManager.instance.player2.transform.position);
                    else
                        PickupManager.instance.player2.SetWeapon(PlayerShip.Weapons.SINGLE_SHOT);
                    break;
                case "double":
                    if (PickupManager.instance.player2.currentWeapon == PlayerShip.Weapons.GEMINI_BLAST)
                        PickupManager.instance.SpawnPickUp(5, PickupManager.instance.player2.transform.position);
                    else
                        PickupManager.instance.player2.SetWeapon(PlayerShip.Weapons.GEMINI_BLAST);
                    break;
                case "triple":
                    if (PickupManager.instance.player2.currentWeapon == PlayerShip.Weapons.TRIPLE_SHOT)
                        PickupManager.instance.SpawnPickUp(5, PickupManager.instance.player2.transform.position);
                    else
                        PickupManager.instance.player2.SetWeapon(PlayerShip.Weapons.TRIPLE_SHOT);
                    break;
                case "ion":
                    if (PickupManager.instance.player2.currentWeapon == PlayerShip.Weapons.ION_CANNON)
                        PickupManager.instance.SpawnPickUp(5, PickupManager.instance.player2.transform.position);
                    else
                        PickupManager.instance.player2.SetWeapon(PlayerShip.Weapons.ION_CANNON);
                    break;
                default:
                    break;
            }
        }
        base.OnTriggerEnter2D(col);
    }
}
