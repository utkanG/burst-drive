﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiecePickup : Pickup {

    public int pieceIndex = 0; // 0 Sp1, 1 Sp2, 2 Hp1, 3 Hp2

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            //PickupManager.instance.player1.AddBuildPiece(pieceIndex);
        }
        base.OnTriggerEnter2D(col);
    }
}
