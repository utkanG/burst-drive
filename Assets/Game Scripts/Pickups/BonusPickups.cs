﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusPickups : Pickup {

    [SerializeField]
    string pickUpName;
    
    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player1"))
        {
            switch (pickUpName)
            {
                case "life":
                    GameControl.instance.AddLife(1, "1");
                    PickupManager.instance.player1.uiConnector.UpdateLives();
                    break;
                case "shield":
                    PickupManager.instance.player1.PickUpShieldPoint();
                    break;
                case "firerate":
                    PickupManager.instance.player1.AddFireRate();
                    break;
                case "cooling":
                    PickupManager.instance.player1.AddCooling();
                    break;
                case "random":
                    PickupManager.instance.SpawnPickUp(Random.Range(0, PickupManager.instance.pickupTypes.Count), PickupManager.instance.player1.transform.position);
                    break;
                case "guardian":
                    PickupManager.instance.player1.AddGuardianShield();
                    break;
                case "target":
                    PickupManager.instance.player1.ActivateLaserSight();
                    break;
                case "x2":
                    PickupManager.instance.player1.Activatex2();
                    break;
                case "chaos":
                    PickupManager.instance.player1.ActivateChaosGun();
                    break;
                case "phase":
                    PickupManager.instance.player1.EnterPhaseFor(7);
                    break;
                case "drunk":
                    PickupManager.instance.player1.EnterDrunkMode();
                    //PickupManager.instance.ppCam.SetActive(true);
                    PickupManager.instance.peScript.enabled = true;
                    //PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .4f);
                    break;
                case "emp":
                    PickupManager.instance.player1.EMP();
                    GameObject e = ExplosionPooler.instance.GetExplosion(37);
                    e.transform.position = transform.position;
                    SoundManager.instance.PlayFXSound(8, 1, 1, 2, 2);
                    e.SetActive(true);
                    break;
                case "drones":
                    PickupManager.instance.player1.SpawnDrones();
                    break;
                case "slow":
                    GameControl.instance.SlowTime(3.5f, .65f, 1);
                    PickupManager.instance.player1.uiConnector.ActivateTimer(5, 3.5f);
                    break;
                case "reflex":
                    GameControl.instance.SlowTime(1f, .65f, 1);
                    PickupManager.instance.player1.uiConnector.ActivateTimer(5, 1f);
                    break;
                default:
                    break;
            }
        }
        else if(col.CompareTag("Player2"))
        {
            switch (pickUpName)
            {
                case "life":
                    GameControl.instance.AddLife(1 , "2");
                    PickupManager.instance.player2.uiConnector.UpdateLives();
                    break;
                case "shield":
                    PickupManager.instance.player2.PickUpShieldPoint();
                    break;
                case "firerate":
                    PickupManager.instance.player2.AddFireRate();
                    break;
                case "cooling":
                    PickupManager.instance.player2.AddCooling();
                    break;
                case "random":
                    PickupManager.instance.SpawnPickUp(Random.Range(0, PickupManager.instance.pickupTypes.Count), PickupManager.instance.player2.transform.position);
                    break;
                case "guardian":
                    PickupManager.instance.player2.AddGuardianShield();
                    break;
                case "target":
                    PickupManager.instance.player2.ActivateLaserSight();
                    break;
                case "x2":
                    PickupManager.instance.player2.Activatex2();
                    break;
                case "chaos":
                    PickupManager.instance.player2.ActivateChaosGun();
                    break;
                case "phase":
                    PickupManager.instance.player2.EnterPhaseFor(7);
                    break;
                case "drunk":
                    PickupManager.instance.player2.EnterDrunkMode();
                    //PickupManager.instance.ppCam.SetActive(true);
                    PickupManager.instance.peScript.enabled = true;
                    //PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .4f);
                    break;
                case "emp":
                    PickupManager.instance.player2.EMP();
                    GameObject e = ExplosionPooler.instance.GetExplosion(37);
                    SoundManager.instance.PlayFXSound(8, 1, 1, 2, 2);
                    e.transform.position = transform.position;
                    e.SetActive(true);
                    break;
                case "drones":
                    PickupManager.instance.player2.SpawnDrones();
                    break;
                case "slow":
                    GameControl.instance.SlowTime(3.5f, .6f, 2);
                    PickupManager.instance.player2.uiConnector.ActivateTimer(5, 3.5f);
                    break;
                case "reflex":
                    GameControl.instance.SlowTime(1f, .6f, 2);
                    PickupManager.instance.player2.uiConnector.ActivateTimer(5, 1f);
                    break;
                default:
                    break;
            }
        }
        base.OnTriggerEnter2D(col);
    }
}
