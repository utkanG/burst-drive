﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : Pickup {

    public int value;

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if(pickedUp) return;
        if (col.CompareTag("Player1"))
        {
            PickupManager.instance.player1.gemCount += value;
            SoundManager.instance.PlayPickupSound(data.poolIndex);
            PickupManager.instance.player1.uiConnector.UpdateInventory();
        }
        else if (col.CompareTag("Player2"))
        {
            PickupManager.instance.player2.gemCount += value;
            SoundManager.instance.PlayPickupSound(data.poolIndex);
            PickupManager.instance.player2.uiConnector.UpdateInventory();
        }
        EndLife();
    }
}
