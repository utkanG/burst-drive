﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pineapple : Pickup {

    protected override void OnEnable()
    {
        base.OnEnable();
        lifeTime = Random.Range(5, 10);
        currentLife = lifeTime;
        rb2d.velocity = Random.onUnitSphere * 3;
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if(pickedUp) return;
        SoundManager.instance.PlayPickupSound(data.poolIndex);
        if (col.CompareTag("Player1"))
        {
            PickupManager.instance.player1.pineappleCount++;
            PickupManager.instance.player1.uiConnector.UpdateInventory();
            //TutorialControl.instance.ActivatePineappleTuto();
        }
        else if (col.CompareTag("Player2"))
        {
            PickupManager.instance.player2.pineappleCount++;
            PickupManager.instance.player2.uiConnector.UpdateInventory();
        }
        EndLife();
    }
}
