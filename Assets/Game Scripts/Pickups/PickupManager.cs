﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickupManager : MonoBehaviour {

    public static PickupManager instance;

    public List<PickUpType> pickupTypes;
    public List<List<GameObject>> pickupPools = new List<List<GameObject>>();

    public PlayerShip player1;
    public PlayerShip player2;
    //public GameObject ppCam;
    public Image bloomInhibImage;
    public PostEffectScript peScript;

    public List<PickupData> normalPickupData = new List<PickupData>();

    public bool canSpawnPickups;
    public int spawnedPickupCount;

    private List<GameObject> spawnedPickups = new List<GameObject>();

    [System.Serializable]
    public class PickUpType
    {
        public string name;
        public GameObject prefab;
        public int initialPoolAmount = 5;
    }

    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        normalPickupData.Sort();
        InitializePools();
        ClearPickups();
    }

    private void InitializePools()
    {
        for (int i = 0; i < pickupTypes.Count; i++)
        {
            List<GameObject> a = new List<GameObject>();
            pickupPools.Add(a);

            for (int j = 0; j < pickupTypes[i].initialPoolAmount; j++)
            {
                GameObject go = Instantiate(pickupTypes[i].prefab, transform);
                go.SetActive(false);
                a.Add(go);
            }
        }
    }

    public GameObject GetPickup(int poolIndex)
    {
        if (pickupPools[poolIndex].Count > 0)
        {
            GameObject a = pickupPools[poolIndex][0];
            pickupPools[poolIndex].Remove(pickupPools[poolIndex][0]);
            return a;
        }
        else
        {
            GameObject a = Instantiate(pickupTypes[poolIndex].prefab, transform);
            a.SetActive(false);
            return a;
        }
    }

    public void ReturnPickup(int poolIndex, GameObject a)
    {
        pickupPools[poolIndex].Add(a);
        a.SetActive(false);
    }

    public void SpawnPickUp(int index, Vector2 location)
    {
        GameObject p = GetPickup(index);
        spawnedPickups.Add(p);
        p.transform.position = location;
        p.SetActive(true);
        spawnedPickupCount++;
    }

    public void AttemptPickupDrop(Vector2 location)
    {
        if (spawnedPickupCount > LevelManager.instance.currentLevelData.maxPickups)
            return;

        float randomF = Random.Range(0, 84f);
        if (randomF > 15.37f)
            return;

        float checkChance = 0;
        for (int i = 0; i < normalPickupData.Count; i++)
        {
            checkChance += normalPickupData[i].dropChance;
            if (randomF < checkChance)
            {
                SpawnPickUp(normalPickupData[i].poolIndex, location);
                //if (spawnedPickupCount >= LevelManager.instance.currentLevelData.maxPickups)
                //    canSpawnPickups = false;
                return;
            }
        }
    }

    public void RemovePickupFromPool()
    {

    }
    
    public void ClearPickups()
    {
        foreach (GameObject go in spawnedPickups)
        {
            go.SetActive(false);
        }

        canSpawnPickups = true;
        spawnedPickups.Clear();
        spawnedPickupCount = 0;
    }
}
