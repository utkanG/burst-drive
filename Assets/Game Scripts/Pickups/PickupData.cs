﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "", menuName = "PickupData", order = 1)]
public class PickupData : ScriptableObject, IComparable<PickupData> {
    
    public int poolIndex = 0;
    public float dropChance = 0;

    public int CompareTo(PickupData other)
    {
        if (dropChance > other.dropChance)
            return 1;
        else if (dropChance == other.dropChance)
            return 0;
        else
            return -1;
    }
}
