﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Letter : Pickup {

    public bool isShield;
    public int letterIndex;

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player1"))
        {
            PickupManager.instance.player1.BuildExtra(letterIndex, isShield);
        }
        else if (col.CompareTag("Player2"))
        {
            PickupManager.instance.player2.BuildExtra(letterIndex, isShield);
        }
        base.OnTriggerEnter2D(col);
    }
}
