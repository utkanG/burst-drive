﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIConnector : MonoBehaviour {

    public int playerNo;
    [SerializeField]
    private PlayerShip playerShip;
    [SerializeField]
    private Text gemCount;
    [SerializeField]
    private Image[] lives = new Image[4];
    // for player to reach
    public Image[] shields = new Image[3];
    [SerializeField]
    private Slider fireRateSlider;
    [SerializeField]
    private Slider coolingSlider;
    [SerializeField]
    private Slider heatMeter;
    [SerializeField]
    private Image heatMeterFill;
    [SerializeField]
    private Image heatMeterBorder;
    [SerializeField]
    private Text weaponName;
    [SerializeField]
    private Text levelNo;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text scoreBonusText;
    [SerializeField]
    private Animator scoreAnim;
    [SerializeField]
    private Image scoreSlider;
    //[SerializeField]
    //private image[] shieldimages = new image[2];
    //[serializefield]
    //private image[] heartimages = new image[2];
    //[SerializeField]
    //private GameObject[] powerUpTimerObjs;
    //[SerializeField]
    //private Slider[] powerUpSliders;
    //[SerializeField]
    //private Image[] powerUpFills;
    [SerializeField]
    private GameObject guardianShieldObj;
    [SerializeField]
    private GameObject avengerShieldObj;
    [SerializeField]
    private GameObject dimensionalReloadObj;
    [SerializeField]
    private GameObject absoluteCoolingObj;
    [SerializeField]
    private Text pineappleText;
    [SerializeField]
    private Image[] lifeLetters;
    [SerializeField]
    private Image[] shieldLetters;
    //[HideInInspector]
    //public Vector3 lastKillPosition;
    [SerializeField]
    private RectTransform bonusTextRect;
    [SerializeField]
    private GameObject bonusTextObject;
    [SerializeField]
    private RectTransform[] pickupTextRects;
    [SerializeField]
    private Text[] pickupTexts;
    [SerializeField]
    private GameObject droneCountObj;
    [SerializeField]
    private Text droneCountText;
    [SerializeField]
    private Image bulletTimeSlider;
    [SerializeField]
    private Image shieldTimer;
    [SerializeField]
    private RectTransform powerupUI;
    [SerializeField]
    private Image[] powerupTimers;
    [SerializeField]
    private GameObject[] powerupObjects;
    private float[] powerupDurations;

    private bool[] powerUpsActive;
    //private int nextAvailablePowerUpIndex;

    [SerializeField]
    private GameObject[] itemsOnUI; // first 3 is "lost on death" ones

    private float scoreTime;
    private int bonusPoints;
    public int alienKilled;
    private Vector2 viewportPos = Vector2.zero;
    private int nextPickupTextIndex = 0;

    private void Awake()
    {
        powerUpsActive = new bool[powerupTimers.Length];
        powerupDurations = new float[powerupTimers.Length];
    }

    public void TogglePowerupUI(bool active)
    {
        powerupUI.gameObject.SetActive(active);
    }

    public void UpdatePowerupUI()
    {
        viewportPos = Camera.main.WorldToViewportPoint(playerShip.transform.position);

        powerupUI.anchorMin = viewportPos;
        powerupUI.anchorMax = viewportPos;

        //if (!playerShip.gameObject.activeInHierarchy)
        //    powerupUI.gameObject.SetActive(false);
        //else
        //    powerupUI.gameObject.SetActive(true);
    }

    public void UpdateDroneCount()
    {
        if(playerShip.droneCount > 0)
        {
            droneCountObj.SetActive(true);
            droneCountText.text = "x" + playerShip.droneCount;
        }
        else
        {
            droneCountObj.SetActive(false);
        }
    }

    public void UpdateShieldTimer()
    {
        if (shieldTimer.fillAmount >= 1 || shieldTimer.fillAmount <= 0 || !playerShip.gameObject.activeInHierarchy)
            shieldTimer.gameObject.SetActive(false);
        else
            shieldTimer.gameObject.SetActive(true);

        shieldTimer.fillAmount = playerShip.tempShield / playerShip.shieldRegenTime;
    }

    private void Start()
    {
        scoreTime = 0;
        bonusPoints = 0;
        alienKilled = 0;
        nextPickupTextIndex = 0;
    }

    private void Update()
    {
        scoreTime -= Time.deltaTime;
        if(scoreTime >= 0 && alienKilled >= 4)
        {
            scoreSlider.gameObject.SetActive(true);
            scoreSlider.fillAmount = scoreTime / 1.25f;
        }

        if (alienKilled >= 4 && scoreTime <= 0)
            UpdateScoreBonus(bonusPoints, playerNo); // TODO have bonus points text appear at the the position of the last enemy killed
        if (scoreTime <= 0)
        {
            alienKilled = 0;
            bonusPoints = 0;
            scoreSlider.gameObject.SetActive(false);
        }

        //UpdateTimers();
        UpdatePowerupTimers();
        UpdatePowerupUI();
    }

    public void UpdateExtraShields()
    {
        if (playerShip.guardianShield)
            guardianShieldObj.SetActive(true);
        else
            guardianShieldObj.SetActive(false);

        if (playerShip.avengerShield)
            avengerShieldObj.SetActive(true);
        else
            avengerShieldObj.SetActive(false);
    }

    public void UpdateLetters()
    {
        for (int i = 0; i < 5; i++)
        {
            if (playerShip.lifeLetters[i])
                lifeLetters[i].color = Color.white;
            else
                lifeLetters[i].color = Color.black;

            if (playerShip.shieldLetters[i])
                shieldLetters[i].color = Color.white;
            else
                shieldLetters[i].color = Color.black;
        }
    }

    public void UpdateInventory()
    {
        gemCount.text = playerShip.gemCount.ToString();
        pineappleText.text = playerShip.pineappleCount.ToString();
    }

    public void ResetAllItems()
    {
        foreach(GameObject item in itemsOnUI)
            item.SetActive(false);
    }

    public void ResetItemsForDeath()
    {
        for(int i = 0; i < 3; i++)
            itemsOnUI[i].SetActive(false);
    }

    public void UpdateLives()
    {
        for (int i = 0; i < 4; i++)
        {
            if (playerNo == 1)
            {
                if (GameControl.instance.player1Lives > i)
                    lives[i].color = Color.red;
                else
                    lives[i].color = Color.black;
            }
            else
            {
                if (GameControl.instance.player2Lives > i)
                    lives[i].color = Color.red;
                else
                    lives[i].color = Color.black;
            }
        }
    }

    public void UpdateFireRate()
    {
        fireRateSlider.value = playerShip.bonusFireRatePoints + 1;
        if (playerShip.dimensionalReload)
        {
            fireRateSlider.value = 21;
            dimensionalReloadObj.SetActive(true);
        }
        else
            dimensionalReloadObj.SetActive(false);
    }

    public void UpdateCooling()
    {
        coolingSlider.value = playerShip.bonusCoolingPoints + 1;
        if (playerShip.absoluteCooling)
        {
            coolingSlider.value = 21;
            absoluteCoolingObj.SetActive(true);
        }
        else
            absoluteCoolingObj.SetActive(false);
    }

    public void UpdateShields()
    {
        for (int i = 0; i < 3; i++)
        {
            if (playerShip.currentShieldPoints > i)
                shields[i].color = Color.white;
            else
            {
                shields[i].fillAmount = 1;
                shields[i].color = Color.black;
            }
        }
    }

    public void UpdateHeatMeter()
    {
        heatMeter.value = playerShip.heat;
        if (playerShip.heat < .25)
            heatMeterFill.color = Color.green;
        else if (playerShip.heat < .5)
            heatMeterFill.color = Color.yellow;
        else if (playerShip.heat < 1)
            heatMeterFill.color = Color.red;
    }

    public void UpdateWeaponName()
    {
        weaponName.text = playerShip.weapons[(int)playerShip.currentWeapon].name;
    }

    public void UpdateLevelNo()
    {
        levelNo.text = "LEVEL " + LevelManager.instance.GetLevelNoToShow();
    }

    public void UpdateEverything()
    {
        UpdateFireRate();
        UpdateCooling();
        UpdateInventory();
        UpdateShields();
        UpdateHeatMeter();
        UpdateLevelNo();
        UpdateWeaponName();
        UpdateScore(0, playerNo);
        UpdateLives();
        UpdateExtraShields();
        DisableTimerObjects();
        UpdateLetters();
        UpdateDroneCount();
    }

    public void ActivatePickUpText(string sToDisplay)
    {
        if (sToDisplay == "Random")
            return;
        if (nextPickupTextIndex >= 5)
            nextPickupTextIndex = 0;

        pickupTextRects[nextPickupTextIndex].gameObject.SetActive(false);

        viewportPos = Camera.main.WorldToViewportPoint(playerShip.transform.position + Vector3.up * nextPickupTextIndex / 2f);
        pickupTextRects[nextPickupTextIndex].anchorMin = viewportPos;
        pickupTextRects[nextPickupTextIndex].anchorMax = viewportPos;
        pickupTexts[nextPickupTextIndex].text = sToDisplay.ToUpper();
        pickupTextRects[nextPickupTextIndex].gameObject.SetActive(true);

        nextPickupTextIndex++;
    }

    public void UpdateScore(int value, int playerNo)
    {
        if(playerNo == 1)
        {
            if (playerShip.drunk)
                GameControl.instance.p1Score += value * 30;
            else if (playerShip.x2Points)
                GameControl.instance.p1Score += value * 20;
            else
                GameControl.instance.p1Score += value * 10;
            scoreText.text = " " + GameControl.instance.p1Score + " ";
        }
        else
        {
            if (playerShip.drunk)
                GameControl.instance.p2Score += value * 30;
            else if (playerShip.x2Points)
                GameControl.instance.p2Score += value * 20;
            else
                GameControl.instance.p2Score += value * 10;
            scoreText.text = " " + GameControl.instance.p2Score + " ";
        }

        if (scoreTime <= 0)
            scoreTime = 0;
        bonusPoints += 5 + (int)(scoreTime * 10);
        scoreTime = 1.25f;
        if (value == 0)
        {
            bonusPoints = 0;
            scoreTime = 0;
        }
        scoreAnim.Play("Score Update");
    }

    public void UpdateBulletTime()
    {
        bulletTimeSlider.fillAmount = playerShip.bulletTime / 1.2f;
    }

    public void UpdateScoreBonus(int value, int playerNo)
    {
        bonusTextObject.SetActive(false);
        if (playerNo == 1)
        {
            if (playerShip.drunk)
                GameControl.instance.p1Score += value * 30;
            else if (playerShip.x2Points)
                GameControl.instance.p1Score += value * 20;
            else
                GameControl.instance.p1Score += value * 10;
            scoreText.text = " " + GameControl.instance.p1Score + " ";
            viewportPos = Camera.main.WorldToViewportPoint(GameControl.instance.player1Object.transform.position);
        }
        else
        {
            if (playerShip.drunk)
                GameControl.instance.p2Score += value * 30;
            else if (playerShip.x2Points)
                GameControl.instance.p2Score += value * 20;
            else
                GameControl.instance.p2Score += value * 10;
            scoreText.text = " " + GameControl.instance.p2Score + " ";
            viewportPos = Camera.main.WorldToViewportPoint(GameControl.instance.player2Object.transform.position);
        }

        SoundManager.instance.PlayFXSound(27, 1f, .3f, 1f, true);
        bonusPoints = 0;
        alienKilled = 0;

        if (playerShip.drunk)
            scoreBonusText.text = "+" + (value * 30);
        else if (playerShip.x2Points)
            scoreBonusText.text = "+" + (value * 20);
        else
            scoreBonusText.text = "+" + (value * 10);

        bonusTextRect.anchorMin = viewportPos;
        bonusTextRect.anchorMax = viewportPos;
        bonusTextObject.SetActive(true);
    }

    public void ActivateItemImage(int index)
    {
        itemsOnUI[index].SetActive(true);
    }

    //public void UpdateBuildSlots()
    //{
    //    if (playerShip.buildingShield)
    //    {
    //        shieldImages[0].gameObject.SetActive(true);
    //        shieldImages[1].gameObject.SetActive(true);
    //        if (playerShip.buildSlots[0])
    //        {
    //            shieldImages[0].color = Color.white;
    //            shieldImages[1].color = Color.black;
    //        }
    //        else if (playerShip.buildSlots[1])
    //        {
    //            shieldImages[1].color = Color.white;
    //            shieldImages[0].color = Color.black;
    //        }
    //    }
    //    else if (playerShip.buildingLife)
    //    {
    //        heartImages[0].gameObject.SetActive(true);
    //        heartImages[1].gameObject.SetActive(true);
    //        if (playerShip.buildSlots[0])
    //        {
    //            heartImages[0].color = Color.white;
    //            heartImages[1].color = Color.black;
    //        }
    //        else if (playerShip.buildSlots[1])
    //        {
    //            heartImages[1].color = Color.white;
    //            heartImages[0].color = Color.black;
    //        }
    //    }
    //    else
    //    {
    //        shieldImages[0].gameObject.SetActive(false);
    //        shieldImages[1].gameObject.SetActive(false);
    //        heartImages[0].gameObject.SetActive(false);
    //        heartImages[1].gameObject.SetActive(false);
    //    }
    //}

    //private void UpdateTimers()
    //{
    //    for (int i = 0; i < powerUpsActive.Length; i++)
    //    {
    //        if (powerUpsActive[i])
    //        {
    //            powerUpSliders[i].value -= Time.deltaTime;

    //            if (powerUpSliders[i].value <= 0)
    //            {
    //                powerUpTimerObjs[i].SetActive(false);
    //                powerUpsActive[i] = false;
    //            }
    //            else if(powerUpSliders[i].value <= 1f)
    //            {
    //                powerUpFills[i].color = Color.red;
    //            }
    //            else if (powerUpSliders[i].value <= 3.5f)
    //            {
    //                powerUpFills[i].color = Color.yellow;
    //            }
    //        }
    //    }
    //}

    private void UpdatePowerupTimers()
    {
        for (int i = 0; i < powerUpsActive.Length; i++)
        {
            if (powerUpsActive[i])
            {
                powerupTimers[i].fillAmount -= (Time.deltaTime / powerupDurations[i]);
                
                if (powerupTimers[i].fillAmount <= 0f)
                {
                    powerupObjects[i].SetActive(false);
                    powerUpsActive[i] = false;
                }
                else if (powerupTimers[i].fillAmount * powerupDurations[i] <= 1f)
                {
                    powerupTimers[i].color = Color.red;
                }
                else if (powerupTimers[i].fillAmount * powerupDurations[i] <= 3.5f)
                {
                    powerupTimers[i].color = Color.yellow;
                }
            }
        }
    }

    public void ActivateTimer(int index, float duration)
    {
        //powerUpTimerObjs[index].SetActive(true);
        //powerUpSliders[index].maxValue = duration;
        //powerUpSliders[index].value = duration;
        //powerUpFills[index].color = Color.green;

        powerUpsActive[index] = true;
        
        powerupObjects[index].SetActive(true);
        powerupTimers[index].fillAmount = 1;
        powerupTimers[index].color = Color.green;
        powerupDurations[index] = duration;
    }

    private void DisableTimerObjects()
    {
        for (int i = 1; i < powerupObjects.Length; i++)
        {
            powerupObjects[i].SetActive(false);
        }
    }
}
