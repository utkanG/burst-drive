﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundVelocity : MonoBehaviour {

    [SerializeField]
    private Rigidbody2D[] rigidbody2Ds;

    [SerializeField]
    private ParticleSystem particleSystem;
    private ParticleSystem.ForceOverLifetimeModule velocityModule;

    private void Awake()
    {
        velocityModule = particleSystem.forceOverLifetime;
    }

    Vector2 velocity;
    private void Update()
    {
        velocity = (rigidbody2Ds[0].velocity + rigidbody2Ds[1].velocity);
        velocityModule.xMultiplier = -velocity.x *2f;
        velocityModule.yMultiplier = -velocity.y *2f;
	}
}
