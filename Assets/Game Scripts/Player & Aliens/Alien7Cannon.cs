﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien7Cannon : CannonAlien {

    protected override void Fire()
    {
        SoundManager.instance.PlayFXSound(2, Random.Range(.9f,1.1f), .2f, .3F, true);
        fireCd = cannonAlienData.fireRate;
        GameObject tempProjectile = ProjectilePooler.instance.GetProjectile(cannonAlienData.projectileIndex);

        tempProjectile.transform.position = transform.position + transform.right * -.5f;
        tempProjectile.transform.localRotation = Quaternion.AngleAxis(transform.localRotation.eulerAngles.z + 90, Vector3.forward); ;
        tempProjectile.SetActive(true);
        tempProjectile = ProjectilePooler.instance.GetProjectile(cannonAlienData.projectileIndex);
        tempProjectile.transform.position = transform.position + transform.right*.5f;
        tempProjectile.transform.localRotation = Quaternion.AngleAxis(transform.localRotation.eulerAngles.z + 270, Vector3.forward);
        tempProjectile.SetActive(true);
        
        ChangeTarget();
    }
}
