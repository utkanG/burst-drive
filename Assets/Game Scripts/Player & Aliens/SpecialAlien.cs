﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(Hull))]
public class SpecialAlien : MonoBehaviour {

    [HideInInspector]
    public ScriptableSpecialAlien specialAlienData;

    protected Rigidbody2D rb2d;
    [HideInInspector]
    public Hull hull;

    protected Animator anim;
    protected int targetPlayer; // 1 or 2
    public bool canFire;

    private int specialMaxHp;

    protected virtual void Awake()
    {
        tag = "Special Alien";
        hull = GetComponent<Hull>();
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    protected virtual void Update()
    {
        if (EnemyManager.instance.empActive)
            anim.speed = 0;
        else
            anim.speed = 1;
    }

    protected virtual void OnEnable()
    {
        targetPlayer = Random.Range(GameControl.instance.p1c, GameControl.instance.p2c + GameControl.instance.playerCount);

        int hpMult = LevelManager.instance.Looping;
        specialMaxHp = specialAlienData.hitPoints * (hpMult/2 + 1) + 25 * hpMult;
        hull.hitPoints = specialMaxHp;
        EnemyManager.instance.enemyCount++;
        EnemyManager.instance.AddAlien(hull);
        hull.invulnerable = true;
        hull.blewUp = false;
        anim.SetFloat("speed", 1);
        anim.StartPlayback();
        hull.MakeSpritesMasked();
        canFire = false;
    }

    protected void ChangeTarget()
    {
        targetPlayer = Random.Range(GameControl.instance.p1c, GameControl.instance.p2c + GameControl.instance.playerCount);
    }

    protected virtual void OnDisable()
    {
        Rigidbody2D tempRb;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, specialAlienData.explosionRadius, specialAlienData.explosionMask);
        foreach (Collider2D collider in colliders)
        {
            tempRb = collider.GetComponent<Rigidbody2D>();
            tempRb.AddForce((collider.transform.position - transform.position).normalized * 10, ForceMode2D.Impulse);
        }
        CameraShaker.instance.ShakeCamera(1.5f, .2f);
        EnemyManager.instance.enemyCount--;
        EnemyManager.instance.RemoveAlien(hull);
        SoundManager.instance.PlayFXSound(4, 1, 1.5f, 3);
        SoundManager.instance.PlayFXSound(18, Random.Range(.9f, 1.25f), .7f,2);
    }

    protected void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.layer == 10 || col.gameObject.layer == 21)
        {
            hull.invulnerable = false;
            anim.StopPlayback();
            hull.MakeSpritesUnmasked();
            canFire = true;
        }

        if (col.gameObject.layer == 9 && hull.hitPoints > 0)
        {
            anim.SetFloat("speed", (float) specialMaxHp / hull.hitPoints);
        }
    }
}
