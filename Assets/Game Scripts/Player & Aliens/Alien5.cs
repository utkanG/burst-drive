﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien5 : Alien {

    [SerializeField]
    private Transform[] arms;

    protected override void OnEnable()
    {
        transform.rotation = Quaternion.identity;
        foreach (Transform t in arms)
        {
            t.rotation = Quaternion.AngleAxis(Random.Range(-50f, 50f), Vector3.forward);
        }

        base.OnEnable();
    }
}
