﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : Hull {

    public bool big;

    Rigidbody2D rb2d;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        if (tag != "Player1" || tag != "Player2")
            spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
    }

    private void OnEnable()
    {
        rb2d.AddTorque(Random.Range(-360f, 360f));
        rb2d.velocity = Random.insideUnitCircle * 5;
        EnemyManager.instance.AddAlien(this);
        if (big)
            hitPoints = 5;
        else
            hitPoints = 2;
        blewUp = false;
    }

    protected override void BlowUp()
    {
        if (big && !blewUp)
        {
            SoundManager.instance.PlayFXSound(3, 1, 1);
            PickupManager.instance.SpawnPickUp(Random.Range(0, 4), transform.position);
            if(hitPoints >-5)
                for (int i = 0; i < 2; i++)
                {
                    GameObject a = EnemyManager.instance.GetAlien(Random.Range(10, 13));
                    a.transform.position = transform.position;
                    a.SetActive(true);
                }
        }
        base.BlowUp();
    }

    public void SeriousBlowUp()
    {
        hitPoints = -6;
        BlowUp();
    }

    private void OnDisable()
    {
        CameraShaker.instance.ShakeCamera(1f, .1f);
        EnemyManager.instance.RemoveAlien(this);

        EnemyManager.instance.spawnedAsteroids.Remove(this);
    }
}
