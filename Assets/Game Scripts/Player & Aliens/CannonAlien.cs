﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class CannonAlien : SpecialAlien {
    
    public ScriptableCannonAlien cannonAlienData;
    
    protected float fireCd;
    private Vector2 randomPosition;
    protected Vector2 distanceVec;
    protected Quaternion targetRot;

    protected override void Awake()
    {
        GetRandomPosition();
        specialAlienData = cannonAlienData;
        base.Awake();
    }

    [ContextMenu("Remove that object reference error!")]
    public void GetSpecialAlien()
    {
        specialAlienData = cannonAlienData;
        base.Awake();
    }

    protected void SetNextPosition(Vector2 position)
    {
        randomPosition = position;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        fireCd = Random.Range(2f, cannonAlienData.fireRate*5);
        GetRandomPosition();
    }

    protected override void Update()
    {
        base.Update();
        if (!GameControl.instance.paused && !EnemyManager.instance.empActive)
        {
            if (specialAlienData.rotateTowardPlayer)
            {
                if (targetPlayer == 1)
                    RotateToward(EnemyManager.instance.playerObject1.transform.position);
                else
                    RotateToward(EnemyManager.instance.playerObject2.transform.position);
            }

            fireCd -= Time.deltaTime;
            if (fireCd <= 0 && canFire/* && EnemyManager.instance.projectileCount <= EnemyManager.instance.maxProjectileCount*/)
                Fire();

            if (Extras.CompareVectors(transform.position, randomPosition, .1f))
                GetRandomPosition();
            else
                MoveTowardsPoint();
        }
    }

    private void GetRandomPosition()
    {
        randomPosition = Random.insideUnitCircle * 8;
    }

    protected void RotateToward(Vector3 target)
    {
        distanceVec = transform.position - target;
        targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, specialAlienData.turnSpeed * Time.deltaTime * 75);
    }

    protected void RotateToward(Vector3 target, float speed)
    {
        distanceVec = transform.position - target;
        targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, specialAlienData.turnSpeed * speed * Time.deltaTime * 75);
    }

    private void MoveTowardsPoint()
    {
        Vector2 direction = (randomPosition - (Vector2)transform.position).normalized;
        rb2d.velocity += direction * cannonAlienData.speed * Time.deltaTime;
    }

    protected virtual void Fire()
    {
        fireCd = cannonAlienData.fireRate;
        GameObject tempProjectile = ProjectilePooler.instance.GetProjectile(cannonAlienData.projectileIndex);
        SoundManager.instance.PlayFXSound(2, 1f, 1f);

        if (specialAlienData.rotateTowardPlayer)
        {
            tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-cannonAlienData.weaponSpread, cannonAlienData.weaponSpread) + transform.rotation.eulerAngles.z, Vector3.forward);
        }
        else
        {
            if (targetPlayer == 1)
            {
                distanceVec = transform.position - EnemyManager.instance.playerObject1.transform.position;
            }
            else
            {
                distanceVec = transform.position - EnemyManager.instance.playerObject2.transform.position;
            }

            targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
            tempProjectile.transform.rotation = targetRot;
        }

        tempProjectile.transform.position = transform.position;
        tempProjectile.SetActive(true);

        GameObject e = ExplosionPooler.instance.GetExplosion(13);
        e.transform.position = transform.position + transform.up * .5f;
        e.SetActive(true);
        EnemyManager.instance.projectileCount++;
        ChangeTarget();
    }
}
