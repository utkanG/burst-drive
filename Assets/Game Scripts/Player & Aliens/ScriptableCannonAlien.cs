﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Cannon Alien", menuName = "Alien/Cannon", order = 1)]
public class ScriptableCannonAlien : ScriptableSpecialAlien {

    [Header("Cannon Alien Properties")]
    public float fireRate = 1;
    public float weaponSpread = 5;
    public float speed = 1;
    public int projectileIndex = 2;
}
