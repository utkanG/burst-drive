﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleShotAlien : CannonAlien {

    protected override void Fire()
    {
        SoundManager.instance.PlayFXSound(1, 1f, .9f);
        fireCd = Random.Range(cannonAlienData.fireRate, cannonAlienData.fireRate * 2f);

        GameObject tempProjectile = ProjectilePooler.instance.GetProjectile(cannonAlienData.projectileIndex);
        tempProjectile.transform.rotation = transform.rotation;
        tempProjectile.transform.position = transform.position + transform.right * .75f + transform.up * .75f;
        tempProjectile.SetActive(true);
        tempProjectile = ProjectilePooler.instance.GetProjectile(cannonAlienData.projectileIndex);
        tempProjectile.transform.rotation = transform.rotation;
        tempProjectile.transform.position = transform.position + transform.right * -.75f + transform.up * .75f;
        tempProjectile.SetActive(true);

        GameObject e = ExplosionPooler.instance.GetExplosion(13);
        e.transform.position = transform.position + transform.right * .75f + transform.up * .75f;
        e.SetActive(true);
        e = ExplosionPooler.instance.GetExplosion(13);
        e.transform.position = transform.position + transform.right * -.75f + transform.up * .75f; ;
        e.SetActive(true);

        EnemyManager.instance.projectileCount++;
        EnemyManager.instance.projectileCount++;
    }
}
