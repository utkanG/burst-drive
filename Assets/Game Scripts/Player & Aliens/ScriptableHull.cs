﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Hull", menuName = "Hull", order = 1)]
public class ScriptableHull : ScriptableObject {
    
    public int explosionIndex = 0;
    public int alienIndex = 0;
    public int scoreValue = 10;
    public bool dropsPickups = true;
}
