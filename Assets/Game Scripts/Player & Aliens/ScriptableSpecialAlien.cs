﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableSpecialAlien : ScriptableObject {

    [Header("Special Alien Properties")]
    public int hitPoints = 1;
    [Tooltip("For enemy pooler")]
    public int poolIndex = 0;
    [Tooltip(".3 is fast")]
    public float turnSpeed = .1f;
    public bool rotateTowardPlayer = true;
    public float explosionRadius = 5;
    public LayerMask explosionMask;
}
