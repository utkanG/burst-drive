﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArrow : MonoBehaviour {

    public Transform target;
    private GameObject sprite;

    private void Awake()
    {
        sprite = transform.GetChild(0).gameObject;
    }

    private void Update()
    {
        if (Mathf.Abs(target.position.x) >= 17.25f || Mathf.Abs(target.position.y) >= 11.5f)
        {
            if (!target.gameObject.activeInHierarchy)
            {
                sprite.SetActive(false);
                return;
            }

            transform.position = new Vector2(Mathf.Clamp(target.position.x, -17f, 17f), Mathf.Clamp(target.position.y, -11f, 11f));
            transform.rotation = Quaternion.AngleAxis(Vector2.SignedAngle(Vector2.right, target.position - transform.position), Vector3.forward);
            sprite.SetActive(true);
        }
        else
            sprite.SetActive(false);
    }
}
