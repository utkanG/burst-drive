﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Beam Alien", menuName = "Alien/Beam", order = 1)]
public class ScriptableBeamAlien : ScriptableSpecialAlien {

    [Header("Beam Alien Properties")]
    public float fireCooldown = 1;
    public float beamDuration = 1;
    public float range = 5;
    public float speed = 1;
    public float turnSpeedWhenFiring = 0;
}
