﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class BigBoss : CannonAlien {

    public Transform[] turrets;
    public int currentStage = 0;
    public float turretFireCd;
    public bool readyForNextStage;

    private Transform[] turretTargets;
    public bool disabled;
    private float disableTimer;
    private float turretCd;
    private int nextTurret = 0;
    private Vector2 nextPosition;
    [SerializeField]
    private GameObject chargeBeam;
    [SerializeField]
    private GameObject disableEffect;
    [SerializeField]
    private GameObject shield;
    private bool cannonCharged;
    private float cannonTime;
    private float chargeTime;
    private int nextTargetPlayer; // 0 or 1
    [SerializeField]
    private GameObject[] tempCannonProjectile = new GameObject[3];
    private Rigidbody2D[] tempRb2d = new Rigidbody2D[3];
    private bool chargingCannon;
    [SerializeField]
    private GameObject[] damageStateObjects;

    private List<GameObject> companions = new List<GameObject>();
    private Transform missileTarget;

    private int maxHp;

    protected override void Awake()
    {
        base.Awake();
        tag = "Boss";
        turretTargets = new Transform[2];
    }

    private void GetRandomPosition()
    {
        nextPosition = Random.insideUnitCircle * 5;
    }

    protected void Start()
    {
        GetTurretTargets();
    }

    protected override void OnDisable()
    {
        for (int i = 0; i < 3; i++)
        {
            if (tempCannonProjectile[i])
                tempCannonProjectile[i].SetActive(false);
        }
        base.OnDisable();
    }

    private void GetTurretTargets()
    {
        if (GameControl.instance.playerCount == 2)
        {
            turretTargets[0] = GameControl.instance.player1Object.transform;
            turretTargets[1] = GameControl.instance.player2Object.transform;
        }
        else
        {
            if(GameControl.instance.p1c == 1)
            {
                turretTargets[0] = GameControl.instance.player1Object.transform;
                turretTargets[1] = GameControl.instance.player1Object.transform;
            }
            else if(GameControl.instance.p2c == 1)
            {
                turretTargets[0] = GameControl.instance.player2Object.transform;
                turretTargets[1] = GameControl.instance.player2Object.transform;
            }
        }
    }

    protected override void OnEnable()
    {
        damageStateObjects[0].SetActive(false);
        damageStateObjects[1].SetActive(false);
        damageStateObjects[2].SetActive(false);
        damageStateObjects[3].SetActive(false);
        chargeBeam.SetActive(false);
        missileTarget = GameControl.instance.player1Object.transform;
        disabled = false;
        shield.SetActive(false);
        disableEffect.SetActive(false);
        currentStage = 0;
        base.OnEnable();
        Start();
        nextTargetPlayer = Random.Range(0, 2);
        maxHp = specialAlienData.hitPoints * (LevelManager.instance.Looping + 1);
        EnemyManager.instance.hpSlider.maxValue = maxHp;
        EnemyManager.instance.hpSlider.value = maxHp;
        hull.hitPoints = maxHp;
        if (GameControl.instance.gameOn)
            EnemyManager.instance.hpSlider.transform.parent.gameObject.SetActive(true);
    }

    private void MoveTowardsPoint()
    {
        Vector2 direction = (nextPosition - (Vector2)transform.position).normalized;
        rb2d.velocity += direction * cannonAlienData.speed * Time.deltaTime;
    }

    private void ChargeAndFireCannon()
    {
        cannonTime -= Time.deltaTime;
        if(cannonTime <= 0 && !chargingCannon)
        {
            tempCannonProjectile[0] = ProjectilePooler.instance.GetProjectile(16);
            tempRb2d[0] = tempCannonProjectile[0].GetComponent<Rigidbody2D>();

            SoundManager.instance.PlayFXSound(13, 1.15f, 1, 3);
            tempCannonProjectile[0].transform.position = transform.position + transform.up * 3;
            tempCannonProjectile[0].SetActive(true);
            chargeBeam.SetActive(true);
            chargeTime = 3;
            chargingCannon = true;
            cannonTime = Random.Range(3.25f, 5f);
            hull.invulnerable = false;
            shield.SetActive(false);
        }
        else if(chargingCannon)
        {
            tempCannonProjectile[0].transform.position = transform.position + transform.up * 3;
            chargeTime -= Time.deltaTime;
            RotateToward(turretTargets[nextTargetPlayer].position, .5f * Time.deltaTime * 75);

            if (chargeTime <= 0)
            {
                SoundManager.instance.PlayFXSound(7, 1f, 1f);
                SoundManager.instance.PlayFXSound(12, 1, 1, 3);
                SoundManager.instance.PlayFXSound(24, 1, 1, 2);
                chargingCannon = false;
                tempCannonProjectile[0].transform.rotation = transform.rotation;
                chargeBeam.SetActive(false);
                tempRb2d[0].velocity = transform.up * 20;
                GetTurretTargets();
                nextTargetPlayer++;
                if (nextTargetPlayer >= 2)
                    nextTargetPlayer = 0;
            }
        }
        else
        {
            RotateToward(turretTargets[nextTargetPlayer].position, .5f * Time.deltaTime * 75);
        }
    }

    private void ChargeAndFireTriCannon()
    {
        cannonTime -= Time.deltaTime;
        for (int i = 0; i < 3; i++)
        {
            if (cannonTime <= 0 && !chargingCannon)
            {
                tempCannonProjectile[i] = ProjectilePooler.instance.GetProjectile(16);
                tempRb2d[i] = tempCannonProjectile[i].GetComponent<Rigidbody2D>();

                SoundManager.instance.PlayFXSound(13, 1.15f, 1, 3);
                tempCannonProjectile[i].transform.position = transform.position + transform.up * 3;
                tempCannonProjectile[i].SetActive(true);

                if(i == 2)
                {
                    chargeBeam.SetActive(true);
                    chargeTime = 3;
                    chargingCannon = true;
                    hull.invulnerable = false;
                    shield.SetActive(false);
                    cannonTime = Random.Range(4f, 6f);
                }
            }
            else if (chargingCannon)
            {
                tempCannonProjectile[i].transform.position = transform.position + transform.up * 3;
                if (i == 0)
                {
                    RotateToward(turretTargets[nextTargetPlayer].position + turretTargets[nextTargetPlayer].up * 2, .5f);
                    chargeTime -= Time.deltaTime;
                }

                if (chargeTime <= 0)
                {
                    SoundManager.instance.PlayFXSound(7, 1f, 1f);
                    SoundManager.instance.PlayFXSound(12, 1, 1, 3);
                    SoundManager.instance.PlayFXSound(24, 1, 1, 2);
                    tempCannonProjectile[i].transform.rotation = Quaternion.AngleAxis(transform.rotation.eulerAngles.z + (i-1)*50, Vector3.forward);
                    tempRb2d[i].velocity = tempCannonProjectile[i].transform.up * 20;
                    if (i == 2)
                    {
                        chargingCannon = false;
                        chargeBeam.SetActive(false);
                        GetTurretTargets();
                        nextTargetPlayer++;
                        if (nextTargetPlayer >= 2)
                            nextTargetPlayer = 0;
                    }
                }
            }
            else
            {
                if (i == 2)
                    RotateToward(turretTargets[nextTargetPlayer].position + turretTargets[nextTargetPlayer].up * 2, .5f);
            }
        }
    }

    private void ChangeStageTo(int stageInt)
    {
        cannonTime = 2;
        currentStage = stageInt;
        readyForNextStage = false;
        chargeTime = 3;
        chargingCannon = false;

        if (stageInt == 2)
            SpawnCompanions(2 + LevelManager.instance.Looping);
    }

    protected override void Update()
    {
        if (!GameControl.instance.paused)
        {
            if (!disabled)
            {
                if (readyForNextStage)
                    ChangeStageTo(++currentStage);
                switch (currentStage)
                {
                    case 0:
                        RotateAndFireTurrets();
                        RotateToward(rb2d.velocity + (Vector2)transform.position);
                        if (Extras.CompareVectors(transform.position, nextPosition, 3f))
                            GetRandomPosition();
                        else
                            MoveTowardsPoint();
                        if (!readyForNextStage)
                        {
                            if ((float)hull.hitPoints / maxHp < .75f)
                            {
                                DisableFor(3);
                                hull.hitPoints = maxHp / 4 * 3;
                            }
                        }
                        break;
                    case 1:
                        ChargeAndFireCannon();
                        RotateAndFireTurrets();
                        if (Extras.CompareVectors(transform.position, nextPosition, 3f))
                            GetRandomPosition();
                        else
                            MoveTowardsPoint();
                        if (!readyForNextStage)
                        {
                            if ((float)hull.hitPoints / maxHp < .5f)
                            {
                                hull.hitPoints = maxHp / 2;
                                DisableFor(3);
                                turretFireCd = 1f;
                            }
                        }
                        break;
                    case 2:
                        RotateAndFireTurrets();
                        RotateToward(rb2d.velocity + (Vector2)transform.position);
                        if (Extras.CompareVectors(transform.position, nextPosition, 3f))
                            GetRandomPosition();
                        else
                            MoveTowardsPoint();
                        hull.invulnerable = true;
                        CheckCompanions();
                        break;
                    case 3:
                        RotateAndFireTurrets();
                        MoveTowardsPlayer();
                        RotateToward(turretTargets[targetPlayer].position, .5f);

                        if (!readyForNextStage)
                        {
                            if ((float)hull.hitPoints / maxHp < .25f)
                            {
                                DisableFor(3);
                                hull.hitPoints = maxHp / 4;
                                turretFireCd = .75f;
                                rb2d.drag = .3f;
                            }
                        }
                        break;
                    case 4:
                        RotateAndFireTurrets();
                        ChargeAndFireTriCannon();
                        if (Extras.CompareVectors(transform.position, nextPosition, 3f))
                            GetRandomPosition();
                        else
                            MoveTowardsPoint();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                hull.invulnerable = true;
                disableTimer -= Time.deltaTime;
                if (disableTimer <= 0)
                {
                    disabled = false;
                    disableEffect.SetActive(false);
                    hull.invulnerable = false;
                    shield.SetActive(false);
                }
            }
        }
    }

    private void MoveTowardsPlayer()
    {
        Vector2 direction = Vector2.zero;
        if ((turretTargets[0].position - transform.position).sqrMagnitude > (turretTargets[1].position - transform.position).sqrMagnitude)
        {
            direction = (turretTargets[1].position - transform.position).normalized;
            targetPlayer = 1;
        }
        else
        {
            direction = (turretTargets[0].position - transform.position).normalized;
            targetPlayer = 0;
        }
        rb2d.velocity += direction * cannonAlienData.speed * Time.deltaTime * 2.5f;
    }
    
    private void RotateAndFireTurrets()
    {
        turretCd += Time.deltaTime;
        for (int i = 0; i < 2; i++)
        {
            RotateToward(turrets[i], turretTargets[i]);

            if(turretCd >= turretFireCd)
            {
                FireTurret(nextTurret);
                nextTurret++;
                if (nextTurret == 2)
                    nextTurret = 0;
            }
        }
    }
    private void SwitchMissileTarget()
    {
        if(missileTarget == GameControl.instance.player1Object.transform)
            missileTarget = GameControl.instance.player2Object.transform;
        else
            missileTarget = GameControl.instance.player1Object.transform;
    }

    int counter = 0;
    private void FireTurret(int turretIndex)
    {
        GameObject tempProjectile;

        if(counter >= 10)
        {
            tempProjectile = EnemyManager.instance.GetAlien(34);
            SoundManager.instance.PlayFXSound(23, Random.Range(.9f, 1.1f), .7f, .5f);

            DestroyableMissile missile = tempProjectile.GetComponent<DestroyableMissile>();
            if(missile)
            {
                missile.SetSpeed(3);
                missile.SetTarget(missileTarget);
            }
            if(counter == 11)
            {
                turretCd = -turretFireCd * 3f;
                SwitchMissileTarget();
                counter = 0;
            }
            else
            {
                counter++;
                turretCd = turretFireCd / 1.05f;
            }
        }
        else
        {
            SoundManager.instance.PlayFXSound(2, Random.Range(.9f, 1.2f), 1, .5f);
            tempProjectile = ProjectilePooler.instance.GetProjectile(17);
            counter++;
            turretCd = turretFireCd / 1.1f;
        }

        tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-10,10) + turrets[turretIndex].rotation.eulerAngles.z, Vector3.forward);

        tempProjectile.transform.position = turrets[turretIndex].position + turrets[turretIndex].up;
        tempProjectile.SetActive(true);

        GameObject e = ExplosionPooler.instance.GetExplosion(36);
        e.transform.position = turrets[turretIndex].position + turrets[turretIndex].up * .7f;
        e.transform.rotation = turrets[turretIndex].rotation;
        e.SetActive(true);
        EnemyManager.instance.projectileCount++;
        GetTurretTargets();
    }

    private void RotateToward(Transform rotateThis, Transform target)
    {
        Vector3 distanceVec = rotateThis.position - (target.position + target.up * 5);
        Quaternion targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
        rotateThis.rotation = Quaternion.RotateTowards(rotateThis.rotation, targetRot, 10f);
    }
    
    private void DisableFor(float time)
    {
        SoundManager.instance.PlayFXSound(4, 1, 1, 2);
        CameraShaker.instance.ShakeCamera(2f, .25f);
        ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
        damageStateObjects[currentStage].SetActive(true);
        disabled = true;
        disableTimer = time;
        LevelManager.instance.currentLevelNo++;
        LevelManager.instance.nextLevelReady = true;
        readyForNextStage = true;
        disableEffect.SetActive(true);
        shield.SetActive(true);
        hull.invulnerable = true;
        chargingCannon = false;
        chargeBeam.SetActive(false);
        for (int i = 0; i < 3; i++)
        {
            if (tempCannonProjectile[i])
                tempCannonProjectile[i].SetActive(false);
        }

        GameObject e = ExplosionPooler.instance.GetExplosion(43);
        e.transform.position = transform.position;
        e.SetActive(true);
    }

    private void SpawnCompanions(int count)
    {
        hull.invulnerable = true;
        shield.SetActive(true);
        for (int i = 0; i < count; i++)
        {
            GameObject alien = EnemyManager.instance.GetAlien(17);
            Hull h = alien.GetComponent<Hull>();
            companions.Add(alien);
            alien.transform.position = transform.position;
            alien.SetActive(true);
            h.invulnerable = false;
            h.MakeSpritesUnmasked();
            SpecialAlien a = alien.GetComponent<SpecialAlien>();
            a.canFire = true;
        }
    }

    private void CheckCompanions()
    {
        if(companions.Count == 0)
        {
            DisableFor(3);
            turretFireCd = .8f;
            rb2d.drag = 1;
        }
        else if (!companions[0].activeInHierarchy)
        {
            companions.RemoveAt(0);
        }
    }
}
