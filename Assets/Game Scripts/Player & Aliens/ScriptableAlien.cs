﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Alien", menuName = "Alien/Normal", order = 1)]
public class ScriptableAlien : ScriptableObject {

    public int hitPoints = 1;
    public float fireCooldown = 1;
    public float speed = 5;
    public float lastPartSpeed = 3;
    public float standingSpeed = 1;
    public float standingPeriod = 1;
    [Tooltip("Min lifetime for missiles")]
    public float weaponSpread = 5;
    public int projectileIndex = 2;
    [Tooltip("For enemy pooler")]
    public int poolIndex = 0;
    [Tooltip(".3 is fast")]
    public float turnSpeed = .1f;
    public bool rotateWhenNoPath; // for no path
    public bool rotateWhenThereIsPath;
    public bool rotateTowardsVelocity = true;
}
