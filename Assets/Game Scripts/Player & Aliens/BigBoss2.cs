﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class BigBoss2 : CannonAlien {

    public int currentStage = 0;

    private float burstCannonTimer;
    private float missileLaunchTimer;
    public Transform cannonTarget;
    public Transform missileTarget;

    private float currentTurnSpeed;
    private bool disabled = false;
    private float disableTimer;
    private bool readyForNextStage;
    [SerializeField]
    private GameObject dimensionChangerObject;

    private int currentCannonNo; // 0,1,2

    [SerializeField]
    private Transform[] missileLaunchers;
    [SerializeField]
    private Transform[] burstCannons;
    GameObject tempProjectile;
    GameObject tempExplosion;
    public LayerMask playerMask;
    [SerializeField]
    private GameObject disabledEffect;
    [SerializeField]
    private GameObject[] damageObjects;
    [SerializeField]
    private AudioSource beamSound;

    private int maxHp;

    protected override void Awake()
    {
        base.Awake();
        tag = "Boss";
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        canFire = true;
        hull.MakeSpritesUnmasked();
        disabled = false;
        missileTarget = GameControl.instance.player1Object.transform;
        cannonTarget = GameControl.instance.player1Object.transform;
        maxHp = specialAlienData.hitPoints * (LevelManager.instance.Looping + 1);
        transform.position = Vector3.zero;
        EnemyManager.instance.hpSlider.maxValue = maxHp;
        EnemyManager.instance.hpSlider.value = maxHp;
        hull.hitPoints = maxHp;
        if(LevelManager.instance.currentLevelNo == 46)
        {
            currentStage = 0;
        }
        else
        {
            currentStage = 4;
            hull.hitPoints = maxHp/4-20;
            EnemyManager.instance.hpSlider.value = hull.hitPoints;
        }

        for (int i = 0; i < 3; i++)
        {
            beamObjects[i].SetActive(false);
            damageObjects[i].SetActive(false);
        }
        disabledEffect.SetActive(false);
        dimensionChangerObject.SetActive(false);
        hull.invulnerable = false;
        hull.blewUp = false;
        StopBeams();
        currentTurnSpeed = 5;
        anim.StopPlayback();
        anim.SetFloat("speed", 1);
        if (GameControl.instance.gameOn)
            EnemyManager.instance.hpSlider.transform.parent.gameObject.SetActive(true);
        ChangeStageTo(currentStage);
    }

    public void PlayAppearSound()
    {
        SoundManager.instance.PlayFXSound(4, 1.5f, 1, 5);
    }

    public void ReadyToFire()
    {
        canFire = true;

        ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
        GameObject e = ExplosionPooler.instance.GetExplosion(65);
        e.transform.position = transform.position;
        e.SetActive(true);
    }

    private void ChangeStageTo(int stage)
    {
        if(stage > 4)
            stage = 4;
        currentStage = stage;
        readyForNextStage = false;
        switch (stage)
        {
            case 0: // missiles only
                rb2d.AddTorque(10);
                missileLaunchCooldown = 2f;
                break;
            case 1: // arm cannnons added
                rb2d.AddTorque(-20);
                burstCannonCooldown = 1.5f;
                missileLaunchCooldown = 2f;
                break;
            case 2: // laser added
                rb2d.AddTorque(35);
                missileLaunchCooldown = 2f;
                burstCannonCooldown = 1.25f;
                damageObjects[0].SetActive(true);
                break;
            case 3: // spawn mini aliens (swarm to defend the core)
                missileLaunchCooldown = 3f;
                rb2d.AddTorque(-50);
                damageObjects[1].SetActive(true);
                break;
            case 4:
                burstCannonCooldown = .4f;
                damageObjects[2].SetActive(true);
                rb2d.angularVelocity = 0;
                rb2d.AddTorque(25);
                spawnTimer = 5;
                laserCooldown = 1;
                laserDuration = 1;
                dimensionChangerObject.SetActive(false);
                if(LevelManager.instance.currentLevelNo > 46)
                    LevelManager.instance.ToOtherDimension();
                ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
                tempAlien = ExplosionPooler.instance.GetExplosion(37);
                tempAlien.transform.position = transform.position;
                tempAlien.SetActive(true);
                healTimer = 2f;
                StartCoroutine(EscapeRoutine());
                break;
            default:
                break;
        }
    }

    GameObject tempAlien;
    private float spawnTimer;
    private void SpawnAliens()
    {
        if(EnemyManager.instance.enemyCount < 20)
        {
            spawnTimer -= Time.deltaTime;
            if(spawnTimer < 0)
            {
                SoundManager.instance.PlayFXSound(11, Random.Range(.3f,.4f), .25f, 1);
                tempAlien = EnemyManager.instance.GetAlien(35);
                tempAlien.transform.position = transform.position;
                tempAlien.SetActive(true);

                Alien alien = tempAlien.GetComponent<Alien>();
                EnemyManager.instance.AddAlien(alien.hull);

                tempAlien = ExplosionPooler.instance.GetExplosion(75);
                tempAlien.transform.position = transform.position;
                tempAlien.SetActive(true);
                spawnTimer = .25f;
            }
        }
    }

    protected override void OnDisable()
    {
        StopAllCoroutines();
        EnemyManager.instance.hpSlider.transform.parent.gameObject.SetActive(false);
        if(hull.hitPoints > 0)
        {
            CameraShaker.instance.ShakeCamera(.25f, .2f);
            EnemyManager.instance.enemyCount--;
            EnemyManager.instance.ReturnAlienToPool(33, gameObject);
            EnemyManager.instance.RemoveAlien(hull);
            SoundManager.instance.PlayFXSound(18, Random.Range(.9f, 1.25f), .7f, 2);
            SoundManager.instance.PlayFXSound(6, .5f, 1f, 3, true);
            SoundManager.instance.PlayFXSound(4, 1.5f, 1, 5);
            GameControl.instance.bossAlive = true;
            ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
            GameObject e = ExplosionPooler.instance.GetExplosion(49);
            e.transform.position = Vector3.zero;
            e.SetActive(true);
            transform.position = Vector3.up * 1000;
        }
        else
        {
            Rigidbody2D tempRb;
            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, specialAlienData.explosionRadius, specialAlienData.explosionMask);
            foreach(Collider2D collider in colliders)
            {
                tempRb = collider.GetComponent<Rigidbody2D>();
                tempRb.AddForce((collider.transform.position - transform.position).normalized * 10, ForceMode2D.Impulse);
            }
            CameraShaker.instance.ShakeCamera(1.5f, .2f);
            EnemyManager.instance.enemyCount--;
            LevelManager.instance.addedElites = true;
            EnemyManager.instance.RemoveAlien(hull);
            SoundManager.instance.PlayFXSound(4, 1, 1.5f, 3);
            SoundManager.instance.PlayFXSound(18, Random.Range(.9f, 1.25f), .7f, 2);
            GameControl.instance.bossAlive = false;
            GameControl.instance.bossKillCount++;
            GameControl.instance.player1Script.finishedGame = true;
        }

        for (int i = 0; i < 3; i++)
        {
            if (tempShots[i])
                tempShots[i].SetActive(false);
        }
    }

    private IEnumerator BurstLaunchMissiles(int count, float timeBetweenMissiles)
    {
        WaitForSeconds waitForSeconds = new WaitForSeconds(timeBetweenMissiles);

        for (int i = 0; i < count; i++)
        {
            FireMissiles();
            yield return waitForSeconds; 
        }

        yield return new WaitForEndOfFrame();
    }

    private void DisableFor(float time)
    {
        SoundManager.instance.PlayFXSound(4, 1, 1, 2);
        CameraShaker.instance.ShakeCamera(2f, .25f);
        ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
        disabled = true;
        disableTimer = time;
        LevelManager.instance.currentLevelNo++;
        LevelManager.instance.nextLevelReady = true;
        readyForNextStage = true;
        StopBeams();
        disabledEffect.SetActive(true);
        //shield.SetActive(true);
        hull.invulnerable = true;
        for (int i = 0; i < 3; i++)
        {
            if (tempShots[i])
                tempShots[i].SetActive(false);
        }

        if(currentStage == 3)
            dimensionChangerObject.SetActive(true);

        GameObject e = ExplosionPooler.instance.GetExplosion(43);
        e.transform.position = transform.position;
        e.SetActive(true);
        StartCoroutine(BurstLaunchMissiles((currentStage+1) * 3, .15f));
    }

    private void CheckForStage(float healthPercentage)
    {
        if (!readyForNextStage)
        {
            if ((float)hull.hitPoints / maxHp < healthPercentage)
            {
                DisableFor(5);
                hull.hitPoints = (int)(maxHp * healthPercentage);
            }
        }
    }

    protected override void Update()
    {
        if (!GameControl.instance.paused)
        {
            if (!disabled && canFire)
            {
                if (readyForNextStage)
                    ChangeStageTo(++currentStage);
                switch (currentStage)
                {
                    case 0:
                        UpdateMissileMechanism();
                        CheckForStage(.9f);
                        break;
                    case 1:
                        ChargeCannons();
                        UpdateCannonMechanism();
                        UpdateMissileMechanism();
                        CheckForStage(.65f);
                        break;
                    case 2:
                        ChargeCannons();
                        UpdateCannonMechanism();
                        UpdateTriLaserMechanism();
                        CheckForStage(.45f);
                        break;
                    case 3:
                        ChargeCannons();
                        UpdateCannonMechanism();
                        UpdateMissileMechanism();
                        UpdateTriLaserMechanism();
                        CheckForStage(.25f);
                        break;
                    case 4:
                        ChargeCannons();
                        UpdateCannonMechanism();
                        UpdateTriLaserMechanism();
                        SpawnAliens();
                        Heal();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                hull.invulnerable = true;
                disableTimer -= Time.deltaTime;

                rb2d.rotation = Mathf.Lerp(rb2d.rotation, 0, Time.deltaTime);
                if (currentStage == 3)
                    rb2d.AddTorque(10);

                if (disableTimer <= 0)
                {
                    disabled = false;
                    hull.invulnerable = false;
                    disabledEffect.SetActive(false);
                }
            }
        }
    }

    private float healTimer;
    private void Heal()
    {
        healTimer -= Time.deltaTime;
        if(healTimer < 0)
        {
            hull.hitPoints += 4;
            healTimer = .2f;
            EnemyManager.instance.hpSlider.value = hull.hitPoints;
        }
    }

    private IEnumerator EscapeRoutine()
    {
        yield return new WaitForSeconds(5);
        SoundManager.instance.PlayFXSound(13, .75f, 1f, 5, true);
        dimensionChangerObject.SetActive(true);
        yield return new WaitForSeconds(5);
        gameObject.SetActive(false);
    }

    private void FireMissiles()
    {
        SwitchMissileTarget();
        for (int i = 0; i < 3; i++)
        {
            tempExplosion = ExplosionPooler.instance.GetExplosion(31);
            tempExplosion.transform.position = missileLaunchers[i].position;
            tempExplosion.transform.rotation = missileLaunchers[i].rotation;
            tempExplosion.SetActive(true);
            SoundManager.instance.PlayFXSound(23, Random.Range(.9f, 1), .7f, .5f);

            tempProjectile = EnemyManager.instance.GetAlien(34);
            DestroyableMissile missile = tempProjectile.GetComponent<DestroyableMissile>();

            tempProjectile.transform.position = missileLaunchers[i].position;
            tempProjectile.transform.rotation = missileLaunchers[i].rotation;
            tempProjectile.SetActive(true);

            if (missile)
            { 
                missile.SetSpeed(Random.Range(5,10));
                missile.SetTarget(missileTarget);
            }
        }
    }

    private float missileLaunchCooldown = 2f;
    private void UpdateMissileMechanism()
    {
        missileLaunchTimer -= Time.deltaTime;

        if(missileLaunchTimer <= 0)
        {
            FireMissiles();
            missileLaunchTimer = missileLaunchCooldown;
        }
    }

    private GameObject[] tempShots = new GameObject[3];
    private Rigidbody2D[] tempRb2d = new Rigidbody2D[3];
    
    private void FireBurstCannon(int cannonIndex)
    {
        SwitchCannonTarget();
        SoundManager.instance.PlayFXSound(6, Random.Range(.7f, .8f), 1f);
        SoundManager.instance.PlayFXSound(7, Random.Range(.7f,.9f), 1f);
        distanceVec = (cannonTarget.position - burstCannons[cannonIndex].transform.position).normalized;
        Quaternion targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg - 90, Vector3.forward);
        tempShots[cannonIndex].transform.rotation = targetRot;
        
        tempRb2d[cannonIndex] = tempShots[cannonIndex].GetComponent<Rigidbody2D>();
        tempRb2d[cannonIndex].velocity = tempShots[cannonIndex].transform.up * 15;
        tempShots[cannonIndex] = null;
    }

    private float burstCannonCooldown = 1f;
    private void UpdateCannonMechanism()
    {
        burstCannonTimer -= Time.deltaTime;

        if(burstCannonTimer <= 0)
        {
            currentCannonNo++;
            if (currentCannonNo > 2)
                currentCannonNo = 0;

            burstCannonTimer = burstCannonCooldown; // shouldn't be lower than .34

            tempExplosion = ExplosionPooler.instance.GetExplosion(74);
            tempExplosion.transform.position = burstCannons[currentCannonNo].transform.position;
            tempExplosion.transform.SetParent(transform);
            tempExplosion.transform.localScale = Vector3.one;
            tempExplosion.SetActive(true);

            tempShots[currentCannonNo] = ProjectilePooler.instance.GetProjectile(41);
            tempShots[currentCannonNo].transform.position = burstCannons[currentCannonNo].position;
            tempShots[currentCannonNo].SetActive(true);

            chargeTimers[currentCannonNo] = 1f; // constant, depends on the animation of projectile and charge effect
        }
    }

    private float[] chargeTimers = new float[3];
    private void ChargeCannons()
    {
        for (int i = 0; i < 3; i++)
        {
            if(chargeTimers[i] > 0)
            {
                if (tempShots[i])
                    tempShots[i].transform.position = burstCannons[i].transform.position;
                chargeTimers[i] -= Time.deltaTime;
                if(chargeTimers[i] <= 0)
                {
                    FireBurstCannon(i);
                }
            }
        }
    }

    private void SwitchMissileTarget()
    {
        if (missileTarget == GameControl.instance.player1Object.transform)
            missileTarget = GameControl.instance.player2Object.transform;
        else
            missileTarget = GameControl.instance.player1Object.transform;
    }
    private void SwitchCannonTarget()
    {
        if (cannonTarget == GameControl.instance.player1Object.transform)
        {
            if (GameControl.instance.p2c == 1)
                cannonTarget = GameControl.instance.player2Object.transform;
            else
                cannonTarget = GameControl.instance.player1Object.transform;
        }
        else
        {
            if(GameControl.instance.p1c == 1)
                cannonTarget = GameControl.instance.player1Object.transform;
            else
                cannonTarget = GameControl.instance.player2Object.transform;
        }
    }

    [SerializeField]
    private GameObject[] lasers;
    [SerializeField]
    private GameObject[] beamObjects;

    private float laserTimer; // laserSight
    private float fireTimer;
    private float laserAttackTimer;

    private void ActivateLaserSights()
    {
        for (int i = 0; i < 3; i++)
        {
            lasers[i].SetActive(true);
        }

        laserTimer = 1f;  // laser duration
    }

    private float laserDuration = 5;
    private float laserCooldown = 2;
    private void FireLasers()
    {
        for (int i = 0; i < 3; i++)
        {
            RaycastHit2D hit = Physics2D.Raycast(beamObjects[i].transform.position, beamObjects[i].transform.up, 25, playerMask);
            if (hit)
            {
                Hull h = hit.collider.GetComponent<Hull>();
                if(h.invulnerable)
                {
                    if(h.CompareTag("Player1"))
                        PickupManager.instance.player1.DamageShield();
                    else if(h.CompareTag("Player2"))
                        PickupManager.instance.player2.DamageShield();
                }
                else
                    h.Damage(1, 0);
            }
        }
    }

    private void ActivateBeams()
    {
        fireTimer = laserDuration; // fire duration
        beamSound.Play();
        for (int i = 0; i < 3; i++)
        {
            lasers[i].SetActive(false);
            beamObjects[i].SetActive(true);
        }
    }

    private void StopBeams()
    {
        beamSound.Stop();
        laserAttackTimer = laserCooldown; // cooldown between shots
        firingLasers = false;
        for (int i = 0; i < 3; i++)
        {
            lasers[i].SetActive(false);
            beamObjects[i].SetActive(false);
        }
    }

    private bool firingLasers = false;
    private void UpdateTriLaserMechanism()
    {
        laserAttackTimer -= Time.deltaTime;

        if (!firingLasers && laserAttackTimer <= 0)
        {
            ActivateLaserSights();
            firingLasers = true;
        }

        if (laserAttackTimer <= 0 && firingLasers)
        {
            if (laserTimer > 0)
            {
                laserTimer -= Time.deltaTime;

                if (laserTimer <= 0)
                    ActivateBeams();
            }

            if(fireTimer > 0)
            {
                fireTimer -= Time.deltaTime;
                FireLasers();

                if (fireTimer <= 0)
                {
                    StopBeams();
                }
            }
        }
    }

    // does nothing, weapons are divided into other functions
    protected override void Fire() { }
}
