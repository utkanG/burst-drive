﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alien8Cannon : CannonAlien
{
    protected override void OnEnable()
    {
        base.OnEnable();
        SetNextPosition(Vector3.zero);
    }

    protected override void Fire()
    {
        SoundManager.instance.PlayFXSound(2, Random.Range(.9f,1.1f), 1f);
        fireCd = Random.Range(0.5f, cannonAlienData.fireRate);
        GameObject tempProjectile = ProjectilePooler.instance.GetProjectile(cannonAlienData.projectileIndex);

        if (specialAlienData.rotateTowardPlayer)
        {
            tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-cannonAlienData.weaponSpread, cannonAlienData.weaponSpread) + transform.rotation.eulerAngles.z, Vector3.forward);
        }
        else
        {
            if (targetPlayer == 1)
            {
                distanceVec = transform.position - EnemyManager.instance.playerObject1.transform.position;
            }
            else
            {
                distanceVec = transform.position - EnemyManager.instance.playerObject2.transform.position;
            }

            targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
            tempProjectile.transform.rotation = targetRot;
        }

        tempProjectile.transform.position = transform.position + transform.up * 1.5f;
        tempProjectile.SetActive(true);

        GameObject e = ExplosionPooler.instance.GetExplosion(1);
        e.transform.position = transform.position + transform.up * 1.5f;
        e.SetActive(true);
        EnemyManager.instance.projectileCount++;
        ChangeTarget();
    }
}
