﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hull : MonoBehaviour {

    public ScriptableHull hullData;

    public int hitPoints;

    public bool invulnerable;

    [HideInInspector]
    public bool hitByGemini;
    private float geminiCooldown;
    [HideInInspector]
    public bool blewUp = false;
    [SerializeField]
    protected SpriteRenderer[] spriteRenderers;
    [SerializeField]
    private ParticleSystemRenderer particleRenderer;

    private float flashTimer = 0;

    private void Awake()
    {
        if(tag != "Player1" || tag != "Player2")
        {
            spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
            particleRenderer = GetComponentInChildren<ParticleSystemRenderer>();
        }
    }

    public virtual void Damage(int damage, int source) // source = 1 for p1, 2 for p2, 0 for rest
    {
        hitPoints -= damage;
        FlashColor();

        if (hitPoints <= 0)
        {
            if (!blewUp)
            {
                if (source == 1)
                {
                    GameControl.instance.player1Script.uiConnector.UpdateScore(hullData.scoreValue, 1);
                    GameControl.instance.player1Script.uiConnector.alienKilled++;
                }
                else if (source == 2)
                {
                    GameControl.instance.player2Script.uiConnector.UpdateScore(hullData.scoreValue, 2);
                    GameControl.instance.player2Script.uiConnector.alienKilled++;
                }
            }
            BlowUp();
        }
        else
        {
            SoundManager.instance.PlayFXSound(11, Random.Range(.5f, .75f), .15f, .25f);
        }
    }

    public void GeminiHit()
    {
        geminiCooldown = .01f;
        hitByGemini = true;
    }

    protected virtual void Update()
    {
        geminiCooldown -= Time.deltaTime;
        if (hitByGemini && geminiCooldown <= 0)
            hitByGemini = false;

        if(flashTimer >= 0)
        {
            flashTimer -= Time.deltaTime;
            if(flashTimer <= 0)
            {
                ResetColor();
            }
        }
    }

    protected virtual void BlowUp()
    {
        if (!blewUp)
        {
            if (gameObject.layer == 11)
            {
                if(!EnemyManager.instance.enemyPools[hullData.alienIndex].Contains(gameObject)) // temp check
                    EnemyManager.instance.ReturnAlienToPool(hullData.alienIndex, gameObject);
            }

            if (hullData.dropsPickups)
            {
                if (CompareTag("Alien"))
                    PickupManager.instance.AttemptPickupDrop(transform.position);
                else if (CompareTag("Special Alien"))
                {
                    for (int i = 0; i < ((int)(LevelManager.instance.currentLevelNo / 25) + 2) * GameControl.instance.playerCount; i++)
                        PickupManager.instance.SpawnPickUp(22, transform.position);
                }
            }

            GameObject e = ExplosionPooler.instance.GetExplosion(hullData.explosionIndex);
            e.transform.position = transform.position;
            e.transform.rotation = transform.rotation;
            // if gemfusion active
            // PickupManager.instance.SpawnPickUp(Random.Range(0,4), transform.position); // TEMP
            e.SetActive(true);
            gameObject.SetActive(false);
            blewUp = true;

            //if(hullData.hasSound)
            //    SoundManager.instance.PlayFXSound(hullData.sfxIndex, 1, 1, 2);
        }
    }

    protected void FlashColor()
    {
        for (int i = 0; i < spriteRenderers.Length; i++)
            spriteRenderers[i].color = Color.red;
        flashTimer = .05f;
    }

    private void ResetColor()
    {
        for (int i = 0; i < spriteRenderers.Length; i++)
            spriteRenderers[i].color = Color.white;
        flashTimer = -1;
    }

    public void MakeSpritesUnmasked()
    {
        foreach (SpriteRenderer s in spriteRenderers)
        {
            s.maskInteraction = SpriteMaskInteraction.None;
        }
        if (particleRenderer)
            particleRenderer.maskInteraction = SpriteMaskInteraction.None;
    }

    public void MakeSpritesMasked()
    {
        foreach (SpriteRenderer s in spriteRenderers)
        {
            s.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        }
        if (particleRenderer)
            particleRenderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
    }
}
