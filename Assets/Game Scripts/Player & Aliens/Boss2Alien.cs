﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class Boss2Alien : RiftAlien {

    protected override void Update()
    {
        if (Extras.CompareVectors(transform.position, randomPosition, 1f))
            GetRandomPosition();
        else
            MoveTowardsPoint();
    }
    
    protected override void MoveTowardsPoint()
    {
        Vector2 direction = (randomPosition - (Vector2)transform.position).normalized;
        rb2d.velocity += direction * alienData.speed * Time.deltaTime;
    }
}
