﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHull : Hull {

    public int bossNo = 1; // 1 or 2

    private void OnEnable()
    {
        UpdateHPBar();
    }

    public void SetHp(int newHp)
    {
        hitPoints = newHp;
    }

    public override void Damage(int damage, int source)
    {
        hitPoints -= damage;
        CameraShaker.instance.ShakeCamera(.65f, .1f);
        //FlashColor();

        if (source == 1)
            GameControl.instance.player1Script.uiConnector.UpdateScore(damage * 50, 1);
        else if (source == 2)
            GameControl.instance.player2Script.uiConnector.UpdateScore(damage * 50, 2);

        if (hitPoints <= 0)
        {
            if (!blewUp)
            {
                if (source == 1)
                {
                    GameControl.instance.player1Script.uiConnector.UpdateScore(hullData.scoreValue, 1);
                    GameControl.instance.player1Script.uiConnector.alienKilled++;
                }
                else if (source == 2)
                {
                    GameControl.instance.player2Script.uiConnector.UpdateScore(hullData.scoreValue, 2);
                    GameControl.instance.player2Script.uiConnector.alienKilled++;
                }

                if(source != 0)
                {
                    if(bossNo == 1)
                        SteamManager.Instance.UnlockAchievement("BOSS_1");
                    else
                        SteamManager.Instance.UnlockAchievement("BOSS_2");
                }
                
                SpawnPickups();
                BlowUp();
                CameraShaker.instance.ShakeCamera(3f, 1f);
                ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));

                EnemyManager.instance.hpSlider.transform.parent.gameObject.SetActive(false);
            }
        }
        else
        {
            SoundManager.instance.PlayFXSound(11, Random.Range(.3f, .6f), .5f, .25f);
        }

        UpdateHPBar();
    }

    private void SpawnPickups()
    {
        for (int i = 0; i < Random.Range(15, 25); i++)
        {
            PickupManager.instance.SpawnPickUp(Random.Range(0, 4), Random.insideUnitCircle * 2.5f + (Vector2)transform.position);
        }

        for (int i = 0; i < Random.Range(6, 10); i++)
        {
            PickupManager.instance.SpawnPickUp(22, Random.insideUnitCircle * 2.5f + (Vector2)transform.position);
        }
    }

    private void UpdateHPBar()
    {
        EnemyManager.instance.hpSlider.value = hitPoints;
    }

    protected override void BlowUp()
    {
        if (!blewUp)
        {
            blewUp = true;
            SoundManager.instance.PlayFXSound(9, 1, 1, 5);
            if (gameObject.layer == 11)
            {
                if (!EnemyManager.instance.enemyPools[hullData.alienIndex].Contains(gameObject)) // temp check
                    EnemyManager.instance.ReturnAlienToPool(hullData.alienIndex, gameObject);
            }

            if (hullData.dropsPickups)
            {
                if (CompareTag("Alien"))
                    PickupManager.instance.AttemptPickupDrop(transform.position);
            }
            else if (CompareTag("Special Alien"))
            {
                for (int i = 0; i < (int)(LevelManager.instance.currentLevelNo / 25) + 1; i++)
                    PickupManager.instance.SpawnPickUp(22, transform.position);
            }

            GameObject e = ExplosionPooler.instance.GetExplosion(hullData.explosionIndex);
            e.transform.position = transform.position;
            e.transform.rotation = transform.rotation;
            // if gemfusion active
            // PickupManager.instance.SpawnPickUp(Random.Range(0,4), transform.position); // TEMP
            e.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
