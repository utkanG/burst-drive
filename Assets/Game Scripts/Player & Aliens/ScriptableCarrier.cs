﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Carrier Alien", menuName = "Alien/Carrier", order = 1)]
public class ScriptableCarrier : ScriptableSpecialAlien {

    [Header("Carrier Alien Properties")]
    public float speed;
    public float barrageCooldown;
    public float spawnCooldown;
    public int barrageCount;
    public int spawnPerBarrage;
}
