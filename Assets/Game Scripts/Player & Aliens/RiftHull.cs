﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiftHull : Hull {

    protected override void BlowUp()
    {
        if (!blewUp)
        {
            GameObject tempProjectile = ProjectilePooler.instance.GetProjectile(34);
            tempProjectile.transform.rotation = Quaternion.AngleAxis(transform.rotation.eulerAngles.z + 90, Vector3.forward);
            tempProjectile.transform.position = transform.position;
            tempProjectile.SetActive(true);
            tempProjectile = ProjectilePooler.instance.GetProjectile(34);
            tempProjectile.transform.rotation = Quaternion.AngleAxis(transform.rotation.eulerAngles.z - 90, Vector3.forward);
            tempProjectile.transform.position = transform.position;
            EnemyManager.instance.projectileCount += 2;
            tempProjectile.SetActive(true);
            blewUp = true;
            if (gameObject.layer == 11)
            {
                if (!EnemyManager.instance.enemyPools[hullData.alienIndex].Contains(gameObject)) // temp check
                    EnemyManager.instance.ReturnAlienToPool(hullData.alienIndex, gameObject);
            }

            PickupManager.instance.SpawnPickUp(0, transform.position);

            GameObject e = ExplosionPooler.instance.GetExplosion(hullData.explosionIndex);
            e.transform.position = transform.position;
            // if gemfusion active
            // PickupManager.instance.SpawnPickUp(Random.Range(0,4), transform.position); // TEMP
            e.SetActive(true);
            gameObject.SetActive(false);
        }
        blewUp = true;
    }
}
