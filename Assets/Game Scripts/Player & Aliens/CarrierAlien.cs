﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class CarrierAlien : SpecialAlien {

    public ScriptableCarrier carrierData;

    public int[] alienIndex;
    public Vector2[] spawnPositionOffsets;

    private Vector2 randomPosition;
    private Vector2 distanceVec;
    private Quaternion targetRot;

    private float barrageTimer;
    private float spawnTimer;
    private int firedBarrageCount;
    private int alienCount;

    [ContextMenu("Remove that object reference error!")]
    public void GetSpecialAlien()
    {
        specialAlienData = carrierData;
        base.Awake();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        barrageTimer = carrierData.barrageCooldown;
        alienCount = 0;
        firedBarrageCount = 0;
    }

    private Alien a;
    public void SpawnAlien()
    {
        SoundManager.instance.PlayFXSound(11, Random.Range(.3f, .4f), .25f, 1);
        GameObject go = EnemyManager.instance.GetAlien(alienIndex[Random.Range(0, alienIndex.Length-1)]);
        int rand = Random.Range(0, spawnPositionOffsets.Length - 1);
        Vector2 position = transform.position + spawnPositionOffsets[rand].x * transform.right + transform.up * spawnPositionOffsets[rand].y;
        a = go.GetComponent<Alien>();
        go.transform.position = position;
        go.SetActive(true);
        a.Activate();
        a.ActivateFighterMode();

        go = ExplosionPooler.instance.GetExplosion(75);
        go.transform.position = position;
        go.SetActive(true);

        spawnTimer = carrierData.spawnCooldown;
        alienCount++;
    }

    protected override void Update()
    {
        base.Update();
        if (!GameControl.instance.paused && !EnemyManager.instance.empActive)
        {
            if (specialAlienData.rotateTowardPlayer)
            {
                if (targetPlayer == 1)
                    RotateToward(EnemyManager.instance.playerObject1.transform.position);
                else
                    RotateToward(EnemyManager.instance.playerObject2.transform.position);
            }

            barrageTimer -= Time.deltaTime;
            if(barrageTimer< 0 && canFire)
            {
                if (firedBarrageCount < carrierData.barrageCount)
                {
                    spawnTimer -= Time.deltaTime;
                    if (spawnTimer < 0)
                    {
                        SpawnAlien();
                        if (alienCount >= Mathf.Clamp(LevelManager.instance.Looping * carrierData.spawnPerBarrage, 2, 4))
                        {
                            barrageTimer = carrierData.barrageCooldown - Mathf.Clamp(LevelManager.instance.Looping, 1, 2);
                            alienCount = 0;
                            firedBarrageCount++;
                        }
                    }
                }
            }

            if (Extras.CompareVectors(transform.position, randomPosition, .1f))
                GetRandomPosition();
            else
                MoveTowardsPoint();
        }
    }

    private void GetRandomPosition()
    {
        randomPosition = Random.insideUnitCircle * 8;
    }

    private void MoveTowardsPoint()
    {
        Vector2 direction = (randomPosition - (Vector2)transform.position).normalized;
        rb2d.velocity += direction * carrierData.speed * Time.deltaTime;
    }

    protected void RotateToward(Vector3 target)
    {
        distanceVec = transform.position - target;
        targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, specialAlienData.turnSpeed * Time.deltaTime * 75);
    }

    protected void RotateToward(Vector3 target, float speed)
    {
        distanceVec = transform.position - target;
        targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, specialAlienData.turnSpeed * speed * Time.deltaTime * 75);
    }
}
