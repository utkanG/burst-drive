﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

[RequireComponent(typeof(Rigidbody2D), typeof(Hull))]
public class Alien : MonoBehaviour {

    public ScriptableAlien alienData;
    
    public Path path;
    private int nextPathNodeIndex = 0;

    private float fireCd;
    private float standingCd; 
    private bool inPosition;
    private bool movingToPosition;
    private bool onLeft;
    private bool hasPath;
    private bool canFire;
    private int targetPlayer; // 1 or 2

    private Vector3 endPosition;
    protected Vector2 randomPosition;

    protected Rigidbody2D rb2d;
    [HideInInspector]
    public Hull hull;
    protected Animator anim;

    protected Vector2 distanceVec;
    Quaternion targetRot;

    protected virtual void Awake()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        hull = GetComponent<Hull>();
        tag = "Alien";
    }

    private void Start()
    {
        fireCd = Random.Range(0, alienData.fireCooldown * 3); // temp solution for no continuous firing
    }

    protected virtual void OnEnable()
    {
        fireCd = Random.Range(0, alienData.fireCooldown * 4); // temp solution for no continuous firing

        targetPlayer = Random.Range(GameControl.instance.p1c, GameControl.instance.p2c + GameControl.instance.playerCount);
        nextPathNodeIndex = 0;
        inPosition = false;
        movingToPosition = false;
        rb2d.drag = 1;
        hull.hitPoints = alienData.hitPoints + (LevelManager.instance.Looping);
        hull.invulnerable = true;
        hull.blewUp = false;
        canFire = false;

        hull.MakeSpritesMasked();
        EnemyManager.instance.enemyCount++;

        if (!path)
        {
            hasPath = false;
            GetRandomPosition();
        }
        else
            hasPath = true;

        //Invoke("Activate", 10);
    }

    protected virtual void OnDisable()
    {
        CameraShaker.instance.ShakeCamera(1f, .08f);
        EnemyManager.instance.enemyCount--;
        EnemyManager.instance.RemoveAlien(hull);
        if(Mathf.Abs(transform.position.x) < 22 && Mathf.Abs(transform.position.y) < 13)
            SoundManager.instance.PlayFXSound(3, Random.Range(.65f, 1.35f), 1f);
    }

    private void GetInPosition()
    {
        movingToPosition = true;
        endPosition = path.GetLocation();
    }

    private void MoveTowardsEndPosition()
    {
        rb2d.drag = 10;
        rb2d.velocity += (Vector2)(endPosition - transform.position)/10 * alienData.lastPartSpeed;

        if (Extras.CompareVectors(transform.position, endPosition, .1f))
            inPosition = true;
    }

    private void FollowPath()
    {
        Vector2 direction = (path.nodes[nextPathNodeIndex].transform.position - transform.position).normalized;
        rb2d.velocity += direction * path.nodes[nextPathNodeIndex].speedMultiplier * Time.deltaTime;

        //rotation handled by velocity
        if (alienData.rotateTowardsVelocity)
        {
            targetRot = Quaternion.AngleAxis(Mathf.Atan2(rb2d.velocity.y, rb2d.velocity.x) * Mathf.Rad2Deg - 90, Vector3.forward);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, alienData.turnSpeed);
        }
    }

    private bool fighterMode;
    protected virtual void Update()
    {
        if (EnemyManager.instance.empActive)
            anim.speed = 0;

        if (!GameControl.instance.paused && !EnemyManager.instance.empActive)
        {
            anim.speed = 1;
            if (hasPath)
            {
                standingCd += Time.deltaTime;
                if (inPosition)
                {
                    PingPong();
                    if (alienData.rotateWhenThereIsPath)
                    {
                        if (targetPlayer == 1)
                            RotateToward(EnemyManager.instance.playerObject1.transform.position);
                        else
                            RotateToward(EnemyManager.instance.playerObject2.transform.position);
                    }
                }
                else if (movingToPosition)
                {
                    MoveTowardsEndPosition();
                    if (alienData.rotateWhenThereIsPath)
                    {
                        if (targetPlayer == 1)
                            RotateToward(EnemyManager.instance.playerObject1.transform.position);
                        else
                            RotateToward(EnemyManager.instance.playerObject2.transform.position);
                    }
                }
                else
                {
                    FollowPath();
                }
            }
            else if(!fighterMode)
            {
                if (alienData.rotateWhenNoPath)
                    RotateToward(randomPosition);
                else if (alienData.rotateWhenThereIsPath)
                {
                    if (targetPlayer == 1)
                        RotateToward(EnemyManager.instance.playerObject1.transform.position);
                    else
                        RotateToward(EnemyManager.instance.playerObject2.transform.position);
                }

                fireCd -= Time.deltaTime;

                if (Extras.CompareVectors(transform.position, randomPosition, 1f))
                    GetRandomPosition();
                else
                    MoveTowardsPoint();
            }
            else
            {
                if (alienData.rotateWhenNoPath)
                    RotateToward(randomPosition);
                else if (alienData.rotateWhenThereIsPath)
                {
                    if (targetPlayer == 1)
                        RotateToward(EnemyManager.instance.playerObject1.transform.position);
                    else
                        RotateToward(EnemyManager.instance.playerObject2.transform.position);
                }

                if(targetPlayer == 1)
                    randomPosition = (Vector2) EnemyManager.instance.playerObject1.transform.position + offset;
                else
                    randomPosition = (Vector2) EnemyManager.instance.playerObject2.transform.position + offset;

                MoveTowardsPoint();
            }

            fireCd -= Time.deltaTime;
            if (fireCd <= 0 && EnemyManager.instance.projectileCount < EnemyManager.instance.maxProjectileCount)
            {
                //if (EnemyManager.instance.playerObject.activeInHierarchy)
                if(canFire)
                    Fire();
            }
        }

    }

    private Vector2 offset;
    public void ActivateFighterMode()
    {
        fighterMode = true;
        if(targetPlayer == 1)
            offset = Random.insideUnitCircle * 5f;
        rb2d.AddForce(Random.insideUnitCircle * 10f, ForceMode2D.Impulse);
    }

    protected virtual void MoveTowardsPoint()
    {
        Vector2 direction = (randomPosition - (Vector2)transform.position).normalized;
        rb2d.velocity += direction * alienData.speed * Time.deltaTime;
    }

    protected void RotateToward(Vector3 target)
    {
        distanceVec = transform.position - target;
        targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, alienData.turnSpeed);
    }

    private void PingPong()
    {
        if (onLeft)
        {
            rb2d.velocity += (Vector2)transform.right * alienData.standingSpeed;
        }
        else
        {
            rb2d.velocity -= (Vector2)transform.right * alienData.standingSpeed;
        }

        if (standingCd >= alienData.standingPeriod)
        {
            standingCd = 0;
            onLeft = !onLeft;
        }
    }

    protected virtual void Fire()
    {
        fireCd = Random.Range(alienData.fireCooldown, alienData.fireCooldown * 3f); // temp solution for no continiuos firing

        SoundManager.instance.PlayFXSound(1, 1.6f, .5f);
        GameObject e = ExplosionPooler.instance.GetExplosion(1);
        e.transform.position = transform.position + transform.up * .5f;
        e.SetActive(true);

        GameObject tempProjectile = ProjectilePooler.instance.GetProjectile(alienData.projectileIndex);
        if (alienData.rotateWhenThereIsPath)
        {
            tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-alienData.weaponSpread, alienData.weaponSpread) + transform.rotation.eulerAngles.z, Vector3.forward);
        }
        else
        {
            if(targetPlayer == 1)
                distanceVec = transform.position - EnemyManager.instance.playerObject1.transform.position;
            else
                distanceVec = transform.position - EnemyManager.instance.playerObject2.transform.position;
            targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
            tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-alienData.weaponSpread, alienData.weaponSpread) + targetRot.eulerAngles.z, Vector3.forward);
        }
        tempProjectile.transform.position = transform.position;
        tempProjectile.SetActive(true);
        EnemyManager.instance.projectileCount++;
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        if (!movingToPosition && hasPath && !EnemyManager.instance.empActive)
        {
            if (col.gameObject.layer == 13)
            {
                if (col.gameObject == path.nodes[nextPathNodeIndex].gameObject)
                {
                    if (path.nodes[nextPathNodeIndex].destroyAlien)
                    {
                        hull.Damage(1000, 0);
                        return;
                    }

                    if (path.nodes[nextPathNodeIndex].teleportToNext)
                    {
                        if (nextPathNodeIndex + 1 < path.nodes.Length)
                            transform.position = path.nodes[nextPathNodeIndex + 1].transform.position;
                    }

                    if (path.nodes[nextPathNodeIndex].resetVelocity)
                    {
                        rb2d.velocity = Vector2.zero;
                        if (nextPathNodeIndex < path.nodes.Length-1)
                            rb2d.velocity = path.nodes[nextPathNodeIndex].velocityAfterArrival * (path.nodes[nextPathNodeIndex + 1].transform.position - transform.position).normalized;
                    }

                    nextPathNodeIndex++;
                    
                    if (nextPathNodeIndex >= path.nodes.Length)
                    {
                        if (path.loop)
                        {
                            if (path.nodes[nextPathNodeIndex-1].resetVelocity)
                            {
                                rb2d.velocity = Vector2.zero;
                                if (nextPathNodeIndex - 1 < path.nodes.Length)
                                    rb2d.velocity = path.nodes[nextPathNodeIndex-1].velocityAfterArrival * (path.nodes[0].transform.position - transform.position).normalized;
                            }
                            if (path.nodes[nextPathNodeIndex-1].teleportToNext)
                            {
                                if (nextPathNodeIndex - 1 < path.nodes.Length)
                                    transform.position = path.nodes[0].transform.position;
                            }

                            nextPathNodeIndex = 0;
                            return;
                        }
                        else
                        {
                            GetInPosition();
                            return;
                        }
                    }
                }
            }
        }

        if(inPosition && col.gameObject.layer == 10)
        {
            hull.Damage(10000, 0);
            return;
        }

        if (col.gameObject.layer == 10 || col.gameObject.layer == 21)
        {
            Activate();
        }
    }

    protected virtual void GetRandomPosition()
    {
        randomPosition = Random.insideUnitCircle * 12;
    }

    public void SetNextPosition(Vector3 position)
    {
        randomPosition = position;
    }

    public void Activate()
    {
        if(!hull.invulnerable) return;
        hull.invulnerable = false;
        anim.StopPlayback();
        hull.MakeSpritesUnmasked();
        canFire = true;
    }
}
