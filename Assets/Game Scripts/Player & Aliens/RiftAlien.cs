﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class RiftAlien : Alien {

    public TrailRenderer[] trails;
    
    private float randomRotSpeed = 1;
    private Gradient gradient = new Gradient();

    public float movementRadius = 5;
    public bool startWithoutMask = false;

    protected override void Awake()
    {
        base.Awake();
        trails[0].endColor = new Color(1, 1, 1, 0);
        trails[1].endColor = new Color(1, 1, 1, 0);
    }

    protected override void OnEnable()
    {
        trails[0].startColor = Random.ColorHSV(0, 1, 25, 100, 35, 100);
        trails[1].startColor = Random.ColorHSV(0, 1, 25, 100, 35, 100);
        randomRotSpeed = Random.Range(-720f, 720);
        base.OnEnable();
        if (startWithoutMask)
        {
            hull.invulnerable = false;
            anim.StopPlayback();
            hull.MakeSpritesUnmasked();
        }
    }

    protected override void Update()
    {
        if (Extras.CompareVectors(transform.position, randomPosition, 1f))
            GetRandomPosition();
        else
            MoveTowardsPoint();

        if (!GameControl.instance.paused)
            transform.Rotate(0, 0, transform.rotation.z + randomRotSpeed * Time.deltaTime);
    }

    protected override void GetRandomPosition()
    {
        randomPosition = Random.insideUnitCircle * movementRadius;
    }
}
