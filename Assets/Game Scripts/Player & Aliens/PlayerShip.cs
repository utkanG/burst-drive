﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : MonoBehaviour {

    public bool chaosGun;
    public bool dimensionalReload;
    public bool absoluteCooling;
    public bool laserSight;

    [Header("Movement")]
    public float acceleration = 2;
    public float turnSpeed = 1;
    public float slowDownSpeed = 5;
    public float forwardThrust = 1;
    [Space(10)]

    [Header("Weapon")]
    [Range(0, 20)]
    public int bonusFireRatePoints = 0;
    public Weapons currentWeapon; // for projectile pool index
    public float shipFireRate = 2;
    [SerializeField]
    private float weaponFireRateMultiplier = 1;
    [SerializeField]
    private float weaponSpreadAngle = 5;
    private float fireCooldown = 0;
    [SerializeField]
    private float recoilForce = 0;
    public bool autoTurretOn = false;
    public float autoTurretRange = 3;
    private float autoTurretCd;
    private Transform currentTarget;
    [SerializeField]
    private bool targetingCPU;
    [Space(10)]

    [Header("Cooling")] // all heat options are between 0 and 1
    [Range(0,20)]
    public int bonusCoolingPoints = 0;
    [SerializeField]
    private float cooling = .1f;
    [SerializeField]
    private float weaponHeatGen = 0;
    public float heat = 0;
    [Space(10)]

    [Header("Weapon Options")]
    public WeaponType[] weapons;
    [Space(10)]

    [Header("Input")]
    public bool controllerInput;
    public string playerNo;
    public KeyCode[] keyCodes;
    public int controllerIndex;

    [Header("Other")]
    [Tooltip("Max Shields")]
    public int currentShieldPoints = 1;
    public float shieldRegenTime = .2f;
    [HideInInspector]
    public float tempShield = 0;
    private bool shieldTimerSet = false;
    [SerializeField]
    private ParticleSystem engineExhaust;
    [SerializeField]
    private ParticleSystem dashLinePS;
    [SerializeField]
    private ParticleSystem heatSmoke;
    [SerializeField]
    private GameObject shieldObject;
    [SerializeField]
    private GameObject phaseObject;
    [SerializeField]
    private ParticleSystem warp;
    private LineRenderer[] ionTrails;
    private LineRenderer[] laserTrails;
    private int trailIndex = 0;
    public bool drunk = false;
    private float drunkTimer = 0;
    private bool breaking;
    private float dashTimer;
    private bool canDash;

    public int droneCount;
    
    public GameObject particleBeam;
    [SerializeField]
    private GameObject laserObject;
    [SerializeField]
    private GameObject guardianShieldObj;
    [SerializeField]
    private GameObject avengerShieldObj;
    [SerializeField]
    private ParticleSystem shockwaveSystem;
    [SerializeField]
    private ParticleSystem inverseShockwaveSystem;

    public UIConnector uiConnector;

    private float photonBurstCount = 0;
    private float photonCooldown = 0;

    [HideInInspector]
    public bool accelerating = true;
    public int shieldPoints = 1;

    private float warpChargeAcc = 1;

    private ParticleSystem.MainModule exhaustMain;
    private ParticleSystem.MainModule burstMain;
    private ParticleSystem.MainModule smokeMain;
    private Rigidbody2D rb2d;
    [HideInInspector]
    public Hull hull;

    private GameObject tempProjectile;
    private GeminiProjectile tempGemini;
    private GameObject tempExplosion;

    public int currentLevel;
    public int gemCount;
    public int pineappleCount;

    private bool phasing = false;
    private float phaseTimer = 0;
    private bool warpActive = false;
    private float warpDelayTimer;
    private float animTimer;

    public LayerMask alienMask;
    private float chaosTimer;

    public bool[] shieldLetters = new bool[5];
    public bool[] lifeLetters = new bool[5];

    public bool x2Points;
    private float x2Timer;

    public float bulletTime;

    public bool guardianShield;
    public bool avengerShield;

    private AudioSource phaseSound;

    public enum Weapons { SINGLE_SHOT, GEMINI_BLAST, ION_CANNON, PHOTON_MISSILE, SCATTER_LASER, ARC_SPHERE, GAMMARANG, PARTICLE_BEAM,
        DOUBLE_SHOT, TRIPLE_SHOT, S_MOLTEN_SHOT, MICRO_ROCKETS, GATLING_LASER, RAIL_GUN, CHAIN_GUN, POMEGRANATE, HULL_BUSTER, PLASMA, SPARK_MISSILES, UNSTABLE_ION };
    private Weapons weaponBeforeChaos;

    private bool bulletTimeReady;
    private bool bulletTimeStarted;
    private bool[] inputBools;
    [SerializeField]
    public PlayerAIControl playerAIControl;
    private Animator anim;
    public bool finishedGame; // TODO last boss
    [SerializeField]
    Transform weaponBarrel;

    [SerializeField]
    private GameObject forwardThrusterObj;
    [SerializeField]
    private LineRenderer dashIonBeam;

    [SerializeField]
    private LayerMask explosionMask;
    private Collider2D col;

    public bool advancedVents;
    public float shieldRegenMultiplier;
    [Header("Dash Bools")]
    public bool increasedPhase;
    public bool particleDash;
    public bool duplicateDash;
    public bool freeDash;
    public bool ionCannonDash;
    public bool dashMine;
    public bool chaosDash;
    public bool doubleDash;
    public bool pushAtDestDash;
    public bool pullAtOriginDash;
    public bool dashPickup;
    private Camera mainCam;

    [HideInInspector]
    public int dashUpgradeCount;

    private int pNo;
    [SerializeField]
    private GameObject cursor;

    [System.Serializable]
    public class WeaponType
    {
        public string name;
        public float fireRateMultiplier;
        public float spreadAngle;
        public float recoilForce;
        [Range(0,1)]
        public float heatGeneration;
    }

    public void Awake()
    {
        pickupCollider.enabled = false;
        ionTrails = new LineRenderer[5];
        laserTrails = new LineRenderer[10];
        rb2d = GetComponent<Rigidbody2D>();
        hull = GetComponent<Hull>();
        exhaustMain = engineExhaust.main;
        burstMain = dashLinePS.main;
        smokeMain = heatSmoke.main;
        x2Points = false;
        inputBools = new bool[6];
        anim = GetComponent<Animator>();

        phaseSound = phaseObject.GetComponent<AudioSource>();
        col = GetComponent<Collider2D>();

        if(playerNo == "1")
            pNo = 1;
        else
            pNo = 2;

        mainCam = Camera.main;
    }

    public void SetWeapon(Weapons w)
    {
        if(w == Weapons.SINGLE_SHOT || w == Weapons.GEMINI_BLAST)
        {
            if(currentWeapon == Weapons.ARC_SPHERE || currentWeapon == Weapons.GAMMARANG || currentWeapon == Weapons.PARTICLE_BEAM || currentWeapon == Weapons.PLASMA)
                SteamManager.Instance.UnlockAchievement("DOWNGRADE");
        }

        currentWeapon = w;
        weaponBeforeChaos = w;
        weaponSpreadAngle = weapons[(int)w].spreadAngle;
        weaponFireRateMultiplier = weapons[(int)w].fireRateMultiplier;
        recoilForce = weapons[(int)w].recoilForce;
        weaponHeatGen = weapons[(int)w].heatGeneration;
        uiConnector.UpdateWeaponName();
        photonBurstCount = 0;
        trailIndex = 0;
        heat = 0;
        fireCooldown = 0;
        particleBeam.SetActive(false);
        if (laserSight)
            laserObject.SetActive(true);
        else
            laserObject.SetActive(false);

        if(currentWeapon == Weapons.PARTICLE_BEAM || currentWeapon == Weapons.PLASMA)
            SteamManager.Instance.UnlockAchievement("TIER_5");
    }

    public void AddDrones(int count)
    {
        droneCount += count;
        uiConnector.UpdateDroneCount();
    }

    private void Start()
    {
        tempExplosion = ExplosionPooler.instance.GetExplosion(1);
        OnEnable();
        gemCount = 0;
        for (int i = 0; i < 5; i++)
        {
            ionTrails[i] = ExplosionPooler.instance.GetExplosion(19).GetComponent<LineRenderer>();
            ionTrails[i].transform.position = Vector3.zero;
        }
        for (int i = 0; i < 10; i++)
        {
            laserTrails[i] = ExplosionPooler.instance.GetExplosion(57).GetComponent<LineRenderer>();
            laserTrails[i].transform.position = Vector3.zero;
        }
        //transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward);
    }

    public void ResetWeaponStuff()
    {
        duplicateDashed = 0;
        if(chaosDashed > 0 || chaosGun)
            currentWeapon = weaponBeforeChaos;
        chaosDashed = 0;
    }

    [ContextMenu("Update State")]
    public void OnEnable()
    {
        duplicateDashed = 0;
        if(chaosDashed > 0 || chaosGun)
            currentWeapon = weaponBeforeChaos;
        chaosDashed = 0;
        SoundManager.instance.PlayFXSound(4, 1.5f, 1, 5);
        SoundManager.instance.PlayFXSound(18, 1.25f, 1, 5);
        EndWarp();
        warpDelayTimer = 0;
        animTimer = 0;
        anim.enabled = true;
        anim.Play(0);
        bulletTimeReady = true;
        bulletTimeStarted = false;
        hull.blewUp = false;
        hull.invulnerable = true;
        shieldTimerSet = false;
        shieldPoints = currentShieldPoints;
        SetWeapon(currentWeapon);
        shieldObject.SetActive(true);
        heat = 0;
        photonBurstCount = 0;
        UpdateStats();
        ToggleWarpCharge(false);
        tempShield = 0;
        fireCooldown = .2f;
        for (int i = 0; i < currentShieldPoints; i++)
            uiConnector.shields[i].fillAmount = 1;
        EnterPhaseFor(3);
        x2Points = false;
        bulletTime = 1;
        guardianShieldObj.SetActive(false);
        avengerShieldObj.SetActive(false);
        drunk = false;
        drunkTimer = 0;
        uiConnector.UpdateEverything();
        weaponBarrel.rotation = transform.rotation;
        if (GameControl.instance.timeSlowedBy.ToString() == playerNo)
        {
            GameControl.instance.NormalizeTime();
        }
        SetAcceleration(true);
    }

    public void ActivateAutoTurret()
    {
        autoTurretOn = true;
    }

    private bool driftTrailSpawned = false;
    private float driftTimer = 0;
    private ParticleSystem tempDrift;
    private ParticleSystem.MainModule tempDriftMain;
    private void SpawnDriftTrail()
    {
        if(heat < .15f) return;
        tempDrift = ExplosionPooler.instance.GetExplosion(50).GetComponent<ParticleSystem>();
        tempDriftMain = tempDrift.main;
        if(advancedVents)
            tempDriftMain.startColor = new ParticleSystem.MinMaxGradient(Color.cyan);
        else
            tempDriftMain.startColor = new ParticleSystem.MinMaxGradient(Color.white);
        tempDrift.transform.position = transform.position;
        tempDrift.transform.SetParent(transform);
        tempDrift.gameObject.SetActive(true);
        tempDrift.Play();
        driftTrailSpawned = true;
    }

    private Vector3 mouseDelta;
    private Vector3 mousePos;
    private bool lastInputWasMouse;
    private void Update()
    {
        if (!GameControl.instance.paused)
        {
            UpdateWeaponBarrel();
            if (anim.enabled)
            {
                animTimer += Time.deltaTime;
                if (animTimer > .165f)
                {
                    tempExplosion = ExplosionPooler.instance.GetExplosion(49);
                    tempExplosion.transform.position = transform.position;
                    tempExplosion.SetActive(true);
                    ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
                    uiConnector.TogglePowerupUI(true);
                    anim.enabled = false;
                }
            }

            if (driftTrailSpawned)
            {
                if(!advancedVents)
                    heat -= Time.deltaTime /4;
                else
                    heat -= Time.deltaTime;
                driftTimer += Time.deltaTime;
                if (rb2d.velocity.magnitude < 5f)
                {
                    driftTrailSpawned = false;
                    tempDrift.Stop();
                    tempDrift.transform.parent = null;
                }
                if (driftTimer >= 1f)
                {
                    driftTimer = 0;
                    driftTrailSpawned = false;
                    tempDrift.Stop();
                    tempDrift.transform.parent = null;
                }
            }
            else
            {
                if(rb2d.velocity.magnitude > 8f && Vector2.Angle(rb2d.velocity, transform.up) > 45 && !warpActive)
                {
                    SpawnDriftTrail();
                    driftTimer = 0;
                }
            }

            float rotationInput;
            inputBools[0] = Input.GetKey(keyCodes[0]);
            inputBools[1] = Input.GetKey(keyCodes[1]);
            inputBools[2] = Input.GetKey(keyCodes[2]);
            inputBools[3] = Input.GetKeyDown(keyCodes[3]);
            inputBools[4] = Input.GetKeyUp(keyCodes[3]);
            inputBools[5] = Input.GetKeyUp(keyCodes[2]);
            playerAIControl.GetInputs(ref inputBools);
            
            if (autoTurretOn)
            {
                autoTurretCd -= Time.deltaTime;
                if (autoTurretCd < 0)
                {
                    RaycastHit2D hit = Physics2D.CircleCast(transform.position, autoTurretRange, transform.position, 0, alienMask);
                    Vector2 distance;

                    if (hit)
                    {
                        currentTarget = hit.transform;
                    }

                    if (currentTarget)
                        distance = (currentTarget.transform.position - transform.position);
                    else
                        distance = Vector2.one * 500;

                    if (distance.magnitude <= autoTurretRange && currentTarget.gameObject.activeInHierarchy)
                    {
                        SoundManager.instance.PlayFXSound(0, 1 / 1f, .15f);
                        tempProjectile = ProjectilePooler.instance.GetProjectile(19);
                        tempProjectile.transform.rotation = Quaternion.AngleAxis(Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg - 90, Vector3.forward);
                        tempProjectile.transform.position = transform.position;
                        tempProjectile.tag = "p" + playerNo;
                        tempProjectile.SetActive(true);
                        autoTurretCd = 1 / shipFireRate;
                        if (dimensionalReload)
                            autoTurretCd = 1 / 12f;
                    }
                    else
                        currentTarget = null;
                }
            }

            if (!controllerInput)
            {
                if(inputBools[0])
                {
                    rotationInput = 1;
                    lastInputWasMouse = false;
                }
                else if(inputBools[1])
                {
                    rotationInput = -1;
                    lastInputWasMouse = false;
                }
                else
                {
                    rotationInput = 0;
                }

                mouseDelta = Input.mousePosition - mousePos;
                mousePos = Input.mousePosition;
                if(mouseDelta.sqrMagnitude > 0.1f)
                    lastInputWasMouse = true;
                else
                    cursor.SetActive(false);

                if(pNo == 1 && lastInputWasMouse)
                {
                    Vector2 mouseInWorld = mainCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
                    cursor.SetActive(true);
                    cursor.transform.position = mouseInWorld;
                    mouseInWorld -= (Vector2)transform.position;
                    float angle = Mathf.Atan2(mouseInWorld.y, mouseInWorld.x) * Mathf.Rad2Deg + 90;
                    float deltaAngle = Mathf.DeltaAngle(transform.rotation.eulerAngles.z, angle);

                    if(180 - Mathf.Abs(deltaAngle) > 2)
                    {
                        if(deltaAngle > 2f)
                            rotationInput = 1f;
                        else if(deltaAngle < -2f)
                            rotationInput = -1f;
                    }
                    else if(!warpActive  && (!particleBeam.activeInHierarchy))
                    {
                        transform.rotation = Quaternion.Euler(0, 0, angle + 180);
                        rotationInput = 0;
                    }
                }

                if (Input.GetKey(keyCodes[6]))
                    ForwardThrust();
                else
                {
                    breaking = false;
                    forwardThrusterObj.SetActive(false);
                }
            }
            else
            {
                cursor.SetActive(false);
                rotationInput = Input.GetAxis("Horizontal" + controllerIndex);
                if (Input.GetKey(keyCodes[7]))
                    ForwardThrust();
                else
                {
                    breaking = false;
                    forwardThrusterObj.SetActive(false);
                }
            }

            if(chaosGun)
            {
                chaosTimer -= Time.deltaTime;
                if (chaosTimer < 0)
                {
                    if(chaosDashed <= 0)
                        SetWeapon(weaponBeforeChaos);
                    chaosGun = false;
                }
            }
            if(x2Points)
            {
                x2Timer -= Time.deltaTime;
                if (x2Timer < 0)
                    x2Points = false;
            }
            if (drunk)
            {
                drunkTimer -= Time.deltaTime;
                if (drunkTimer < 0)
                {
                    drunk = false;
                    if (!GameControl.instance.player1Script.drunk || !GameControl.instance.player2Script.drunk)
                    {
                        //PickupManager.instance.ppCam.SetActive(true);
                        PickupManager.instance.peScript.enabled = false;
                        PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .08f);
                    }
                }
            }
            if(photonCooldown > 0)
            {
                photonCooldown -= Time.deltaTime;
                if(photonCooldown <= 0)
                    photonBurstCount = 0;
            }

            if (!warpActive && warpDelayTimer < 0)
            {
                transform.position = new Vector2(Mathf.Clamp(rb2d.position.x, -19.5f, 19.5f), Mathf.Clamp(rb2d.position.y, -13.75f, 13.75f));

                if(GameControl.instance.gameOn)
                    phaseTimer -= Time.deltaTime;
                if (phaseTimer <= 0)
                    ExitPhase();

                if (particleBeam.activeInHierarchy)
                {
                    rotationInput /= 5;
                    heat += Time.deltaTime;
                }
                //if(!drunk)
                //if (GameControl.instance.timeSlowedBy.ToString() == playerNo)
                //{
                //    if (!accelerating)
                //        transform.rotation = Quaternion.AngleAxis(transform.rotation.eulerAngles.z - rotationInput * turnSpeed * Time.deltaTime * 55 * Time.timeScale * 1.5f, Vector3.forward);
                //    else
                //        transform.rotation = Quaternion.AngleAxis(transform.rotation.eulerAngles.z - rotationInput * turnSpeed / 1.25f * Time.deltaTime * 55 * Time.timeScale * 1.5f, Vector3.forward);
                //}
                //else
                {
                    if (!accelerating)
                        transform.rotation = Quaternion.AngleAxis(transform.rotation.eulerAngles.z - rotationInput * turnSpeed * Time.deltaTime * 55, Vector3.forward);
                    else
                        transform.rotation = Quaternion.AngleAxis(transform.rotation.eulerAngles.z - rotationInput * turnSpeed / 1.25f * Time.deltaTime * 55, Vector3.forward);
                }
                //else
                //    transform.rotation = Quaternion.AngleAxis(transform.rotation.eulerAngles.z - rotationInput * - turnSpeed, Vector3.forward);

                if (GameControl.instance.gameOn)
                {
                    if (!controllerInput)
                    {
                        if (inputBools[3])
                            SetAcceleration(false);
                        else if (inputBools[4])
                            SetAcceleration(true);
                    }
                    else
                    {
                        if (Input.GetKeyDown(keyCodes[4]))
                            SetAcceleration(false);
                        else if (Input.GetKeyUp(keyCodes[4]))
                            SetAcceleration(true);
                    }
                    
                    if (currentWeapon == Weapons.PARTICLE_BEAM)
                    {
                        particleBeam.transform.position = transform.position;
                        particleBeam.transform.rotation = weaponBarrel.rotation;

                        if (!controllerInput)
                        {
                            if (inputBools[2] || (pNo == 1 && Input.GetMouseButton(0)))
                            {
                                if (heat > .55f)
                                    particleBeam.SetActive(false);
                                else if (fireCooldown <= 0)
                                    particleBeam.SetActive(true);
                            }
                        }
                        else
                        {
                            if (Input.GetKey(keyCodes[5]))
                            {
                                if (heat > .55f)
                                    particleBeam.SetActive(false);
                                else if (fireCooldown <= 0)
                                    particleBeam.SetActive(true);
                            }
                        }
                    }
                    else
                        particleBeam.SetActive(false);

                    if (controllerInput)
                    {
                        if (Input.GetKeyUp(keyCodes[5]))
                            particleBeam.SetActive(false);

                        if (Input.GetKey(keyCodes[5]))
                        {
                            if (fireCooldown <= 0)
                            {
                                Fire();
                                heat += weaponHeatGen*1.35f;
                                if(duplicateDashed > 0)
                                {
                                    duplicateDashed--;
                                    Fire();
                                }
                                if (heat >= 1)
                                    heat = 1;
                            }
                        }
                    }
                    else
                    {
                        if (inputBools[5] || (pNo == 1 && Input.GetMouseButtonUp(0)))
                            particleBeam.SetActive(false);

                        if (inputBools[2] || (pNo == 1 && Input.GetMouseButton(0)))
                        {
                            if (fireCooldown <= 0)
                            {
                                Fire();
                                heat += weaponHeatGen*1.2f;
                                if(duplicateDashed > 0)
                                {
                                    duplicateDashed--;
                                    Fire();
                                }
                                if (heat >= 1)
                                    heat = 1;
                            }
                        }
                    }

                    dashTimer -= Time.deltaTime;
                    if(canDash)
                    {
                        if(!controllerInput)
                        {
                            if(Input.GetKeyDown(keyCodes[3]))
                                DashStep();
                        }
                        else
                        {
                            if(Input.GetKeyDown(keyCodes[4]))
                                DashStep();
                        }
                    }
                    if(dashTimer < 0)
                    {
                        canDash = true;
                    }
                }
            }
            else if(warpActive)
            {
                cursor.SetActive(false);
                rb2d.velocity += new Vector2(rotationInput, (mainCam.transform.position - transform.position).normalized.y * 6);
            }

            fireCooldown -= Time.deltaTime;

            if (heat > 0)
            {
                float totalCooling = cooling * Time.deltaTime * heat * 2f;
                totalCooling = Mathf.Clamp(totalCooling, 0.0015f, 1);
                heat -= totalCooling;
            }
            else
            {
                heat = 0;
            }

            if (heat > .24f)
            {
                smokeMain.startSize = heat;
                smokeMain.startSpeed = heat * 5f;
            }
            else
            {
                heatSmoke.Stop();
            }

            if (shieldPoints < currentShieldPoints)
            { 
                tempShield += Time.deltaTime *shieldRegenMultiplier;
                if (shieldPoints < 3)
                    uiConnector.shields[shieldPoints].fillAmount += tempShield / (shieldRegenTime / shieldRegenMultiplier) * Time.deltaTime;
                if (!shieldTimerSet)
                {
                    tempShield = 0;
                    shieldTimerSet = true;
                }
                else
                {
                    if (tempShield >= shieldRegenTime)
                    {
                        SoundManager.instance.PlayFXSound(6, 3, .4f, .5f, 100);
                        GameObject e = ExplosionPooler.instance.GetExplosion(12);
                        e.transform.position = transform.position;
                        e.SetActive(true);
                        hull.invulnerable = true;
                        shieldPoints++;
                        uiConnector.UpdateShields();
                        shieldObject.SetActive(true);
                        shieldTimerSet = false;
                        guardianShieldObj.SetActive(false);
                        avengerShieldObj.SetActive(false);
                    }
                }
            }

            uiConnector.UpdateHeatMeter();
            uiConnector.UpdateShieldTimer();
            warpDelayTimer -= Time.deltaTime;
            if (!warpActive)
                HandleAcceleration();
            CheckLevel();

            if (LevelManager.instance.warping && !warpActive)
            {
                StartWarp();
                ToggleWarpCharge(false);
            }
            else if (!LevelManager.instance.warping && warpActive)
            {
                EndWarp();
            }
        }
    }

    private void SetAcceleration(bool value)
    {
        if(value)
        {
            accelerating = false;
            rb2d.drag = 0f;
            exhaustMain.startSpeed = 1f;
            exhaustMain.startSize = .5f;
        }
        else
        {
            //if (tempDrift)
            //{
            //    tempDrift.transform.parent = null;
            //    driftTrailSpawned = false;
            //}
            accelerating = true;
            rb2d.drag = 5;
            exhaustMain.startSpeed = 3.8f;
            exhaustMain.startSize = 1f;
        }
    }

    private void ForwardThrust()
    {
        SetAcceleration(true);
        breaking = true;
        rb2d.velocity = Vector2.Lerp(rb2d.velocity, Vector2.zero, Time.deltaTime * 2);
        //rb2d.velocity -= (Vector2)transform.up * forwardThrust * Time.deltaTime;
        forwardThrusterObj.SetActive(true);
    }

    private int duplicateDashed;
    private int chaosDashed;
    private float velMag;
    private void DashStep()
    {
        float timerSet = .5f;
        if(increasedPhase)
            timerSet = 1f;

        if(dashTimer > timerSet)
        {
            if(heat < .5f)
            {
                col.enabled = false;
                bulletTimeStarted = false;
                hull.blewUp = true;
                Invoke("EnableCollider", .1f);
                //GameControl.instance.SlowTime(.15f, .075f);

                SoundManager.instance.PlayFXSound(28, Random.Range(.9f, 1.1f), .5f, 1f, true);
                dashLinePS.Play();
                rb2d.MovePosition(transform.position + transform.up * 5f);
                dashTimer = timerSet; // cd
                canDash = false;

                if(doubleDash)
                {
                    if(velMag < 8)
                    {
                        StartCoroutine(LateDash());
                        dashTimer = 1.25f;
                    }
                }

                if(!freeDash)
                    heat += .3f;

                if(dashPickup)
                {
                    if(doubleDash && velMag < 8)
                    {
                        pickupCollider.size = new Vector2(1.5f, 12);
                        pickupCollider.offset = new Vector2(0, 6);
                    }
                    else
                    {
                        pickupCollider.size = new Vector2(1.5f, 6);
                        pickupCollider.offset = new Vector2(0, 3);
                    }
                    pickupCollider.transform.position = transform.position;
                    pickupCollider.transform.rotation = transform.rotation;
                    StartCoroutine(PickUpCollider());
                }

                HandleSimpleDashProperties(); // for stuff that also happens with double dash
            }
            else
            {
                SoundManager.instance.PlayFXSound(21, 1.25f, .5f, .5f);
                dashTimer = 0;
            }
        }
        else
        {
            dashTimer = timerSet + .25f; // the difference between to .5 and .75 is the interval for dash input (double tap), .5 is the cooldown
            velMag = rb2d.velocity.sqrMagnitude;
        }
    }

    private void HandleSimpleDashProperties()
    {
        if(increasedPhase)
            EnterPhaseFor(.75f);
        else
            EnterPhaseFor(.25f);

        if(particleDash)
        {
            for(int i = 0; i < 4; i++)
            {
                tempProjectile = ProjectilePooler.instance.GetProjectile(14);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = weaponBarrel.position + transform.up * (i + 1);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
            }
        }

        if(chaosDash)
        {
            if(chaosDashed <= 0)
                weaponBeforeChaos = currentWeapon;
            currentWeapon = (Weapons) Random.Range(0, 19);
            chaosDashed++;
        }

        if(duplicateDash)
        {
            duplicateDashed++;
        }

        Collider2D[] colliders;
        if(ionCannonDash)
        {
            SoundManager.instance.PlayFXSound(6, 1 / (1.2f - heat), .5f);
            RaycastHit2D hit = Physics2D.CircleCast(transform.position + transform.up*5, .3f, -weaponBarrel.up, 30, alienMask);
            trailIndex++;
            dashIonBeam.gameObject.SetActive(false);
            if(trailIndex > 4)
                trailIndex = 0;
            if(hit)
            {
                tempProjectile = ProjectilePooler.instance.GetProjectile(44);
                tempProjectile.transform.position = hit.point;
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                dashIonBeam.SetPosition(0, transform.position);
                dashIonBeam.SetPosition(1, hit.point);
                dashIonBeam.gameObject.SetActive(true);

                inverseShockwaveSystem.Emit(1);
            }
            else
            {
                dashIonBeam.SetPosition(0, transform.position + transform.up * 5);
                dashIonBeam.SetPosition(1, transform.position - weaponBarrel.up * 50);
                dashIonBeam.gameObject.SetActive(true);
            }

            tempExplosion = ExplosionPooler.instance.GetExplosion(10);
        }

        if(pushAtDestDash)
        {
            shockwaveSystem.Emit(1);
            Rigidbody2D tempRb;
            colliders = Physics2D.OverlapCircleAll(transform.position + transform.up * 5f, 3, explosionMask);
            foreach(Collider2D collider in colliders)
            {
                tempRb = collider.GetComponent<Rigidbody2D>();
                if(tempRb.drag > 2)
                    tempRb.AddForce((collider.transform.position - (transform.position + transform.up * 5f)).normalized * 17, ForceMode2D.Impulse);
                else
                    tempRb.AddForce((collider.transform.position - (transform.position + transform.up * 5f)).normalized * 10, ForceMode2D.Impulse);

                if(collider.gameObject.layer == 16)
                {
                    collider.GetComponent<Hull>().Damage(1000, 0);
                }
            }
        }

        if(pullAtOriginDash)
        {
            inverseShockwaveSystem.Emit(1);
            Rigidbody2D tempRb;
            colliders = Physics2D.OverlapCircleAll(transform.position, 3, explosionMask);
            foreach(Collider2D collider in colliders)
            {
                tempRb = collider.GetComponent<Rigidbody2D>();
                if(tempRb.drag > 2)
                    tempRb.AddForce((collider.transform.position - (transform.position)).normalized * (-17), ForceMode2D.Impulse);
                else
                    tempRb.AddForce((collider.transform.position - (transform.position)).normalized * (-10), ForceMode2D.Impulse);

                if(collider.gameObject.layer == 16)
                {
                    collider.GetComponent<Hull>().Damage(1000, 0);
                }
            }
        }

        if(dashMine)
        {
            for(int i = 0; i < 2; i++)
            {
                tempProjectile = ProjectilePooler.instance.GetProjectile(45);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.transform.position = transform.position + transform.up * 5 * i;
                tempProjectile.SetActive(true);
            }
        }
    }

    [SerializeField]
    private BoxCollider2D pickupCollider;

    WaitForFixedUpdate wffu = new WaitForFixedUpdate();
    private IEnumerator PickUpCollider()
    {
        pickupCollider.enabled = true;
        yield return wffu;
        yield return wffu;
        pickupCollider.enabled = false;
    }
    private IEnumerator LateDash()
    {
        yield return new WaitForFixedUpdate();
        SoundManager.instance.PlayFXSound(28, Random.Range(.9f, 1.1f), .5f, 1f, true);
        dashLinePS.Play();
        rb2d.MovePosition(transform.position + transform.up * 5f);
        HandleSimpleDashProperties();
    }

    private void EnableCollider()
    {
        col.enabled = true;
        hull.blewUp = false;
    }

    private void CheckLevel()
    {
        if(currentLevel != LevelManager.instance.GetLevelNoToShow() && !LevelManager.instance.nextLevelReady)
        {
            currentLevel = LevelManager.instance.GetLevelNoToShow();
            uiConnector.UpdateLevelNo();
        }
    }

    private void HandleAcceleration()
    {
        uiConnector.UpdateBulletTime();
        if (accelerating)
        {
            bulletTime = Mathf.Clamp(bulletTime + Time.deltaTime/1.8f, 0, 1.2f); // bullet time charge
            if (bulletTime >= 1.2f)
                bulletTimeReady = true;
            bulletTimeStarted = false;

            if (GameControl.instance.timeSlowedBy.ToString() == playerNo && bulletTime < 1)
            {
                GameControl.instance.NormalizeTime();
                bulletTimeStarted = false;
                bulletTimeReady = false;
            }

            if (rb2d.velocity.magnitude < 2)
            {
                rb2d.velocity = (Vector2)transform.up * 2.5f;
            }

            rb2d.velocity += (Vector2)transform.up * acceleration * warpChargeAcc * Time.deltaTime * 1.2f;
        }
        else
        {
            if(bulletTimeStarted && bulletTimeReady)
                bulletTime -= Time.deltaTime;
            //else
            //    bulletTime = Mathf.Clamp(bulletTime + Time.deltaTime / 10f, 0, .85f); // bullet time charge
            if (bulletTime > 0 && bulletTimeStarted && bulletTimeReady)
            {
                if (playerNo == "1")
                    GameControl.instance.SlowTime(.1f, .6f, 1);
                else
                    GameControl.instance.SlowTime(.1f, .6f, 2);
            }
            else if (GameControl.instance.timeSlowedBy.ToString() == playerNo && bulletTimeStarted)
            {
                GameControl.instance.NormalizeTime();
                bulletTimeStarted = false;
                bulletTimeReady = false;
            }

            //rb2d.velocity += (Vector2)transform.up * acceleration / 15 * Time.deltaTime;
            if (rb2d.velocity.magnitude > slowDownSpeed)
                rb2d.velocity = Vector2.Lerp(rb2d.velocity, transform.up * slowDownSpeed, Time.deltaTime);
        }
    }

    // TODO temp
    private bool turnSwitch;
    private int plasmaCounter;
    private bool reverseCounter;
    private int laserCount = 6;

    private void Fire()
    {
        switch (currentWeapon)
        {
            case Weapons.SINGLE_SHOT:
                turnSwitch = !turnSwitch;
                SoundManager.instance.PlayFXSound(0, 1 / (1.2f - heat), .35f);
                //if(turnSwitch)
                //    SoundManager.instance.PlayFXSound(1, 1 / (1 - heat/2));
                //else
                //    SoundManager.instance.PlayFXSound(0, 1/(1-heat/2));
                tempProjectile = ProjectilePooler.instance.GetProjectile(0);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(1);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            case Weapons.GEMINI_BLAST:
                SoundManager.instance.PlayFXSound(5, 1 / (1 - heat), .45f);
                tempProjectile = ProjectilePooler.instance.GetProjectile(1);
                tempGemini = tempProjectile.GetComponent<GeminiProjectile>();
                tempGemini.onLeft = true;
                tempProjectile.transform.position = transform.position - transform.right * .4f;
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);

                tempProjectile = ProjectilePooler.instance.GetProjectile(1);
                tempGemini = tempProjectile.GetComponent<GeminiProjectile>();
                tempGemini.onLeft = false;
                tempProjectile.transform.position = transform.position + transform.right * .4f;
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);

                tempExplosion = ExplosionPooler.instance.GetExplosion(9);
                break;
            case Weapons.ION_CANNON:
                SoundManager.instance.PlayFXSound(6, 1 / (1.2f - heat), .5f);
                RaycastHit2D hit = Physics2D.CircleCast(transform.position, .5f, weaponBarrel.up, 30, alienMask);
                trailIndex++;
                if (trailIndex > 4)
                    trailIndex = 0;
                if (hit)
                {
                    tempProjectile = ProjectilePooler.instance.GetProjectile(3);
                    tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                    tempProjectile.transform.position = hit.point;
                    tempProjectile.tag = "p" + playerNo;
                    tempProjectile.SetActive(true);
                    ionTrails[trailIndex].SetPosition(0, transform.position);
                    ionTrails[trailIndex].SetPosition(1, hit.point);
                    ionTrails[trailIndex].gameObject.SetActive(true);
                }
                else
                {
                    ionTrails[trailIndex].SetPosition(0, transform.position + weaponBarrel.up * .5f);
                    ionTrails[trailIndex].SetPosition(1, transform.position + weaponBarrel.up * 50);
                    ionTrails[trailIndex].gameObject.SetActive(true);
                }

                tempExplosion = ExplosionPooler.instance.GetExplosion(10);
                break;
            case Weapons.PHOTON_MISSILE:
                SoundManager.instance.PlayFXSound(23, Random.Range(1f,1.2f), .5f, .5f, true);
                tempProjectile = ProjectilePooler.instance.GetProjectile(5);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                tempExplosion = ExplosionPooler.instance.GetExplosion(1);
                photonBurstCount++;
                break;
            case Weapons.SCATTER_LASER:
                SoundManager.instance.PlayFXSound(14, 1 / (1.25f - heat), .35f);
                if      (heat > .625f)   laserCount = 2;
                else if (heat > .4f)    laserCount = 3;
                else if (heat > .25f)    laserCount = 4;
                else if (heat > .15f)   laserCount = 5;
                else                    laserCount = 6;
                for (int i = 0; i < laserCount; i++)
                {
                    tempProjectile = ProjectilePooler.instance.GetProjectile(7);
                    if(i == 0)
                        tempProjectile.transform.rotation = transform.rotation;
                    else
                        tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                    tempProjectile.transform.position = transform.position + Random.Range(-.5f,1.5f) * weaponBarrel.up;
                    tempProjectile.tag = "p" + playerNo;
                    tempProjectile.SetActive(true);
                }
                tempExplosion = ExplosionPooler.instance.GetExplosion(13);
                break;
            case Weapons.ARC_SPHERE:
                SoundManager.instance.PlayFXSound(25, 1 / (1 - heat/2), .7f, 1);
                tempProjectile = ProjectilePooler.instance.GetProjectile(9);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position + weaponBarrel.up * .5f;
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                tempExplosion = ExplosionPooler.instance.GetExplosion(20);
                break;
            case Weapons.GAMMARANG:
                SoundManager.instance.PlayFXSound(25, 1.5f / (1 - heat / 2), .9f, 1);
                tempProjectile = ProjectilePooler.instance.GetProjectile(18);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position + weaponBarrel.up * .5f;
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                tempExplosion = ExplosionPooler.instance.GetExplosion(40);

                break;
            case Weapons.PARTICLE_BEAM:
                tempProjectile = ProjectilePooler.instance.GetProjectile(14);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = weaponBarrel.position + transform.up;
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);

                tempExplosion = ExplosionPooler.instance.GetExplosion(20);
                break;
            case Weapons.DOUBLE_SHOT:
                SoundManager.instance.PlayFXSound(7, 1 / (1.5f - heat), .6f);
                tempProjectile = ProjectilePooler.instance.GetProjectile(22);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position + transform.right *.4f;
                tempExplosion = ExplosionPooler.instance.GetExplosion(1);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                tempProjectile = ProjectilePooler.instance.GetProjectile(22);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position + transform.right * -.4f;
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            case Weapons.TRIPLE_SHOT:
                SoundManager.instance.PlayFXSound(7, 1 / (1.5f - heat), .7f);
                for (int i = -1; i < 2; i++)
                {
                    tempProjectile = ProjectilePooler.instance.GetProjectile(23);
                    tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z + i * 15, Vector3.forward);
                    tempProjectile.transform.position = transform.position;
                    tempExplosion = ExplosionPooler.instance.GetExplosion(1);
                    tempProjectile.tag = "p" + playerNo;
                    tempProjectile.SetActive(true);
                }
                break;
            case Weapons.S_MOLTEN_SHOT:
                SoundManager.instance.PlayFXSound(24, 2 / (1 - heat / 2), .9f, .7f);
                tempProjectile = ProjectilePooler.instance.GetProjectile(24);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(55);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            case Weapons.MICRO_ROCKETS:
                SoundManager.instance.PlayFXSound(23, 2 / (1.5f - heat / 2), .4f, .4f);
                tempProjectile = ProjectilePooler.instance.GetProjectile(25);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(1);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            case Weapons.GATLING_LASER:
                SoundManager.instance.PlayFXSound(0, 1 / (1.75f - heat), .25f);
                RaycastHit2D laserHit = Physics2D.Raycast(transform.position, weaponBarrel.up + Random.Range(-weaponSpreadAngle, weaponSpreadAngle)/30 * weaponBarrel.right, 30, alienMask);
                trailIndex++;
                if (trailIndex > 9)
                    trailIndex = 0;
                if (laserHit)
                {
                    tempProjectile = ProjectilePooler.instance.GetProjectile(26);
                    tempProjectile.transform.position = laserHit.point;
                    tempProjectile.tag = "p" + playerNo;
                    tempProjectile.SetActive(true);
                    laserTrails[trailIndex].SetPosition(0, transform.position);
                    laserTrails[trailIndex].SetPosition(1, laserHit.point);
                    laserTrails[trailIndex].gameObject.SetActive(true);
                }
                else
                {
                    laserTrails[trailIndex].SetPosition(0, transform.position + weaponBarrel.up * .5f);
                    laserTrails[trailIndex].SetPosition(1, transform.position + weaponBarrel.up * 50 + Random.Range(-weaponSpreadAngle, weaponSpreadAngle) * weaponBarrel.right);
                    laserTrails[trailIndex].gameObject.SetActive(true);
                }

                tempExplosion = ExplosionPooler.instance.GetExplosion(25);
                break;
            case Weapons.RAIL_GUN:
                SoundManager.instance.PlayFXSound(24, 2 / (1f - heat), .5f);
                tempProjectile = ProjectilePooler.instance.GetProjectile(27);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(59);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            case Weapons.CHAIN_GUN:
                SoundManager.instance.PlayFXSound(29, 1 / (1.25f + heat/2), .5f);
                tempProjectile = ProjectilePooler.instance.GetProjectile(28);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position + weaponBarrel.up * .5f;
                tempExplosion = ExplosionPooler.instance.GetExplosion(1);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                tempProjectile.transform.rotation = Quaternion.identity;
                break;
            case Weapons.POMEGRANATE:
                SoundManager.instance.PlayFXSound(26, 1 / (1.3f - heat), 1);
                tempProjectile = ProjectilePooler.instance.GetProjectile(29);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(61);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            case Weapons.HULL_BUSTER:
                SoundManager.instance.PlayFXSound(7, 1 / (1.3f - heat), 1);
                tempProjectile = ProjectilePooler.instance.GetProjectile(31);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(9);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            case Weapons.PLASMA:
                SoundManager.instance.PlayFXSound(24, 1 / (1f - heat), 1);
                tempProjectile = ProjectilePooler.instance.GetProjectile(35);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(9);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                for (int i = 0; i < 2; i++)
                {
                    tempProjectile = ProjectilePooler.instance.GetProjectile(36);
                    tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-1, 1) * plasmaCounter * 2 + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                    tempProjectile.transform.position = transform.position;
                    tempProjectile.tag = "p" + playerNo;
                    tempProjectile.SetActive(true);
                    if (reverseCounter)
                        plasmaCounter--;
                    else
                        plasmaCounter++;
                }
                break;
            case Weapons.SPARK_MISSILES:
                SoundManager.instance.PlayFXSound(25, 2.5f / (1 - heat / 2), .6f, .35f);
                tempProjectile = ProjectilePooler.instance.GetProjectile(39);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle) + weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(1);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            case Weapons.UNSTABLE_ION:
                SoundManager.instance.PlayFXSound(7, 1 / (1.7f - heat), .8f);
                tempProjectile = ProjectilePooler.instance.GetProjectile(42);
                tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(-weaponSpreadAngle, weaponSpreadAngle)+ weaponBarrel.rotation.eulerAngles.z, Vector3.forward);
                tempProjectile.transform.position = transform.position;
                tempExplosion = ExplosionPooler.instance.GetExplosion(56);
                tempProjectile.tag = "p" + playerNo;
                tempProjectile.SetActive(true);
                break;
            default:
                tempExplosion = ExplosionPooler.instance.GetExplosion(1);
                break;
        }

        if (plasmaCounter > 4)
            reverseCounter = true;
        else if (plasmaCounter < -4)
            reverseCounter = false;
        
        tempExplosion.transform.position = transform.position + transform.up * .5f;
        tempExplosion.transform.rotation = transform.rotation;
        tempExplosion.SetActive(true);
        float totalFireRate = shipFireRate * weaponFireRateMultiplier;
        if (totalFireRate <= 0)
            fireCooldown = 0;
        else
            fireCooldown = 1f/ totalFireRate;

        if(currentWeapon == Weapons.PHOTON_MISSILE)
        {
            fireCooldown = Mathf.Clamp(fireCooldown, .3f, 5);
            photonCooldown = 1;
            if (photonBurstCount < 2)
                fireCooldown = 0.04f;
            else
                photonBurstCount = 0;
        }
        else if(currentWeapon == Weapons.PARTICLE_BEAM)
        {
            fireCooldown = .15f;
        }
        
        if (heat > .4f)
        {
            //TutorialControl.instance.ActivateOverheatTuto();
            fireCooldown += heat / totalFireRate * 5;
            heatSmoke.Play();
            if (heat > .7f)
            {
                fireCooldown += heat * 1.5f;
                if (currentWeapon == Weapons.PHOTON_MISSILE)
                    fireCooldown += heat * 3;
            }
        }

        if (chaosGun)
        {
            heat = 0;
            fireCooldown = 1/shipFireRate/3;
            weaponSpreadAngle = 30;
            currentWeapon = (Weapons)Random.Range(0, 19);
        }
        else if(chaosDashed > 0)
        {
            chaosDashed--;

            if(chaosDashed == 0)
            {
                float temp = fireCooldown;
                if(!chaosGun)
                    SetWeapon(weaponBeforeChaos);
                fireCooldown = temp;
            }
            else
            {
                currentWeapon = (Weapons) Random.Range(0, 19);
                weaponSpreadAngle = 30;
            }
        }

        bulletTimeStarted = true;
        rb2d.AddForce(-transform.up * recoilForce / 3, ForceMode2D.Impulse);
    }

    [SerializeField]
    private GameObject warpCharge;
    public void ToggleWarpCharge()
    {
        warpCharge.SetActive(!warpCharge.activeInHierarchy);
    }
    public void ToggleWarpCharge(bool status)
    {
        warpCharge.SetActive(status);
    }

    private void UpdateWeaponBarrel()
    {
        if (targetingCPU)
        {
            RaycastHit2D raycastHit2D = Physics2D.CircleCast(transform.position + transform.up * 5f, 5f, transform.up, 20f, alienMask);

            if (raycastHit2D)
            {
                Vector3 distanceVec = transform.position - raycastHit2D.transform.position;
                Quaternion targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
                weaponBarrel.rotation = Quaternion.RotateTowards(weaponBarrel.rotation, targetRot, Time.deltaTime * 30f);
            }
            else
            {
                weaponBarrel.rotation = Quaternion.RotateTowards(weaponBarrel.rotation, transform.rotation, Time.deltaTime * 30f);
            }
        }
    }

    public void SpawnDrones()
    {
        for (int i = 0; i < 5; i++)
        {
            tempProjectile = ProjectilePooler.instance.GetProjectile(11);
            tempProjectile.transform.rotation = Quaternion.AngleAxis(i * 72 + transform.rotation.eulerAngles.z, Vector3.forward);
            tempProjectile.transform.position = transform.position;
            tempProjectile.tag = "p" + playerNo;
            tempProjectile.SetActive(true);
            tempExplosion = ExplosionPooler.instance.GetExplosion(1);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.layer == 11 || col.gameObject.layer == 16)
        {
            Hull alienHull = col.GetComponent<Hull>();
            if(playerNo == "1")
                alienHull.Damage(shieldPoints, 1);
            else
                alienHull.Damage(shieldPoints, 2);

            if (!hull.invulnerable)
            {
                if (playerNo == "1")
                    hull.Damage(3, 1);
                else
                    hull.Damage(3, 2);
            }
            else
            {
                DamageShield();
                heatSmoke.Play();
            }
            return;
        }

        if (col.gameObject.layer == 12)
        {
            DamageShield();
            return;
        }

        if (col.gameObject.layer == 10 && tempDrift)
        {
            tempDrift.transform.parent = null;
            driftTrailSpawned = false;
        }
    }

    private void Avenge()
    {
        tempProjectile = ProjectilePooler.instance.GetProjectile(32);
        tempProjectile.transform.position = transform.position;
        tempProjectile.tag = "p" + playerNo;
        tempProjectile.SetActive(true);
        SoundManager.instance.PlayFXSound(12, 2f, 1, 2);
    }

    public void DamageShield()
    {
        SoundManager.instance.PlayFXSound(15, 1.25f, 1f, 2, 1);

        bulletTimeStarted = false;
        GameControl.instance.SlowTime(.2f, .1f);

        if (hull.invulnerable)
        {
            GameObject e;

            if(shieldPoints == 0)
            {
                if (guardianShield)
                {
                    tempExplosion = ExplosionPooler.instance.GetExplosion(35);
                    tempExplosion.transform.position = transform.position;
                    tempExplosion.SetActive(true);
                    EnterPhaseFor(7f);
                    hull.invulnerable = false;
                    guardianShieldObj.SetActive(false);
                    guardianShield = false;
                    uiConnector.UpdateExtraShields();
                    return;
                }
                else if (avengerShield)
                {
                    avengerShield = false;
                    tempExplosion = ExplosionPooler.instance.GetExplosion(42);
                    tempExplosion.transform.position = transform.position;
                    tempExplosion.SetActive(true);
                    EnterPhaseFor(1f);
                    Avenge();
                    hull.invulnerable = false;
                    avengerShieldObj.SetActive(false);
                    uiConnector.UpdateExtraShields();
                    return;
                }
            }

            if (shieldPoints > 0 && shieldPoints < 4)
            {
                if(shieldPoints < 3)
                    uiConnector.shields[shieldPoints].fillAmount = 0;
                heat -= .09f;
                if (heat <= 0)
                    heat = 0;
                shieldPoints--;
                uiConnector.shields[shieldPoints].fillAmount = 0;
                if(playerNo == "1")
                    e = ExplosionPooler.instance.GetExplosion(11);
                else
                    e = ExplosionPooler.instance.GetExplosion(41);
                e.transform.position = transform.position;
                e.SetActive(true);
                tempShield = 0;
                EnterPhaseFor(.75f);
                //TutorialControl.instance.ActivateAegisTuto();
                SpawnDrone();
            }

            if (shieldPoints == 0)
            {
                if (guardianShield)
                {
                    hull.invulnerable = true;
                    guardianShieldObj.SetActive(true);
                }
                else if (avengerShield)
                {
                    hull.invulnerable = true;
                    avengerShieldObj.SetActive(true);
                }
                else
                    hull.invulnerable = false;

                shieldObject.SetActive(false);
                uiConnector.shields[0].fillAmount = 0;

                //PickupManager.instance.SpawnPickUp(33, transform.position);
                rb2d.velocity *= 1.1f;
            }

            uiConnector.UpdateShields();
            uiConnector.UpdateExtraShields();
        }
    }
    
    private void SpawnDrone()
    {
        if(droneCount > 0)
        {
            droneCount--;

            tempProjectile = ProjectilePooler.instance.GetProjectile(11);
            tempProjectile.transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward);
            tempProjectile.transform.position = transform.position;
            tempProjectile.tag = "p" + playerNo;
            tempProjectile.SetActive(true);
            tempExplosion = ExplosionPooler.instance.GetExplosion(1);
            uiConnector.UpdateDroneCount();
        }
    }

    private void OnDisable()
    {
        if(GameControl.instance.gameOn)
            SoundManager.instance.PlayFXSound(9, 1, 1, 5);
        canDash = false;
        uiConnector.TogglePowerupUI(false);
        tempProjectile = ProjectilePooler.instance.GetProjectile(20);
        tempProjectile.transform.position = transform.position;
        tempProjectile.tag = "p" + playerNo;
        tempProjectile.SetActive(true);
        if (particleBeam)
            particleBeam.SetActive(false);
        chaosGun = false;
        targetingCPU = false;
        GameControl.instance.SetSpawnTimer(3, playerNo);
        accelerating = true;
        //PickupManager.instance.ppCam.SetActive(true);
        PickupManager.instance.peScript.enabled = false;
        PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .08f);
        dashIonBeam.gameObject.SetActive(false);
        pickupCollider.enabled = false;
        SetWeapon(weaponBeforeChaos);
        cursor.SetActive(false);

        uiConnector.ResetItemsForDeath();

        if (playerNo == "1")
        {
            if (GameControl.instance.player1Lives == 0)
            {
                if(GameControl.instance.p2c == 1)
                    GameControl.instance.canSpawnOver[0] = true;
                else
                    GameControl.instance.canSpawnOver[0] = false;
                GameControl.instance.playerPanels[0].SetActive(false);
                GameControl.instance.playerCount--;
                GameControl.instance.p1c++;
                GameControl.instance.pressToFireTexts[0].gameObject.SetActive(true);
                GameControl.instance.scoreOverwriteWarnings[0].SetActive(true);
                GameControl.instance.counters[0] = 10f;
            }
        }
        else if (GameControl.instance.player2Lives == 0)
        {
            if (GameControl.instance.p1c == 1)
                GameControl.instance.canSpawnOver[1] = true;
            else
                GameControl.instance.canSpawnOver[1] = false;
            GameControl.instance.playerPanels[1].SetActive(false);
            GameControl.instance.playerCount--;
            GameControl.instance.p2c--;
            GameControl.instance.pressToFireTexts[0].gameObject.SetActive(true);
            GameControl.instance.scoreOverwriteWarnings[1].SetActive(true);
            GameControl.instance.counters[1] = 10f;
        }
        CameraShaker.instance.ShakeCamera(2f, .15f);
        ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));

        //bools that are lost on death
        dimensionalReload = false;
        absoluteCooling = false;
        advancedVents = false;
        autoTurretOn = false;
        shieldRegenMultiplier = 1;
    }

    public void ResetPlayer()
    {
        finishedGame = false;
        canDash = false;
        animTimer = 0;
        transform.rotation = Quaternion.identity;
        guardianShield = false;
        avengerShield = false;
        currentWeapon = Weapons.SINGLE_SHOT;
        dimensionalReload = false;
        absoluteCooling = false;
        bonusFireRatePoints = 4;
        bonusCoolingPoints = 3;
        gemCount = 25;
        pineappleCount = 0;
        shieldPoints = 1;
        currentShieldPoints = 2;
        engineExhaust.gameObject.SetActive(true);

        increasedPhase = false;
        particleDash = false;
        duplicateDash = false;
        freeDash = false;
        dashMine = false;
        dashPickup = false;
        chaosDash = false;
        doubleDash = false;
        pushAtDestDash = false;
        pullAtOriginDash = false;
        ionCannonDash = false;
        chaosGun = false;
        dashUpgradeCount = 0;
        uiConnector.ResetAllItems();

        OnEnable();
    }

    public void PullTowardCenter()
    {
        rb2d.velocity += (Vector2.zero - (Vector2)transform.position) * 1.25f;
    }

    public void AddFireRate()
    {
        if(dimensionalReload)
        {
            PickupManager.instance.SpawnPickUp(3, transform.position);
            return;
        }
        if(bonusFireRatePoints < 20)
        {
            bonusFireRatePoints++;
            shipFireRate += .25f;
            if (bonusFireRatePoints == 20)
                shipFireRate = 8;
        }
        else
        {
            PickupManager.instance.SpawnPickUp(3, transform.position);
            return;
        }
        uiConnector.UpdateFireRate();
    }

    public void AddCooling()
    {
        if (absoluteCooling)
        {
            PickupManager.instance.SpawnPickUp(3, transform.position);
            return;
        }
        if (bonusCoolingPoints < 20)
        {
            bonusCoolingPoints++;
            cooling += 0.015f;
            if (bonusCoolingPoints == 20)
                cooling = .4f;
        }
        else
        {
            PickupManager.instance.SpawnPickUp(3, transform.position);
            return;
        }
        uiConnector.UpdateCooling();
    }

    public void ReducePower()
    {
        laserSight = false;
        if (absoluteCooling)
            absoluteCooling = false;
        else if(bonusCoolingPoints >= 1)
        {
            bonusCoolingPoints--;
            cooling -= 0.015f;
        }
        uiConnector.UpdateCooling();

        if (dimensionalReload)
            dimensionalReload = false;
        else if (bonusFireRatePoints >= 1)
        {
            bonusFireRatePoints--;
            shipFireRate -= 0.25f;
        }
        uiConnector.UpdateFireRate();

        if (currentShieldPoints > 1)
            currentShieldPoints--;
        uiConnector.UpdateShields();
    }
    
    private void UpdateStats()
    {
        shipFireRate = 3;
        cooling = 0.1f;
        for (int i = 0; i < bonusFireRatePoints; i++)
            shipFireRate += 0.25f;
        for (int i = 0; i < bonusCoolingPoints; i++)
            cooling += 0.015f;
        if (absoluteCooling)
        {
            bonusCoolingPoints = 20;
            cooling = 1;
        }
        if (dimensionalReload)
        {
            bonusFireRatePoints = 20;
            shipFireRate = 20;
        }
    }

    public void EnterPhaseFor(float seconds)
    {
        phaseSound.Play();
        if(phaseTimer > seconds)
            return;
        gameObject.layer = 0;
        phasing = true;
        phaseTimer = seconds;
        phaseObject.SetActive(true);
        hull.blewUp = true;
        uiConnector.ActivateTimer(0, seconds);
    }

    public void ExitPhase()
    {
        gameObject.layer = 8;
        phasing = false;
        phaseObject.SetActive(false);
        hull.blewUp = false;
    }

    public void StartWarp()
    {
        SetAcceleration(true);
        warpActive = true;
        engineExhaust.gameObject.SetActive(false);
        warp.Play();
        rb2d.velocity = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        particleBeam.SetActive(false);
        rb2d.drag = 5;
        laserObject.SetActive(false);
        GameControl.instance.NormalizeTime();
        ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
    }

    public void EnterDrunkMode()
    {
        drunkTimer = 7;
        drunk = true;
        uiConnector.ActivateTimer(3, drunkTimer);
    }

    public void EnterSecretDrunk()
    {
        drunkTimer = 100;
        drunk = true;
    }

    public void ExitSecretDrunk()
    {
        drunkTimer = 0;
        drunk = false;
    }

    public void EMP()
    {
        EnemyManager.instance.EMP();
        uiConnector.ActivateTimer(4, 8);
    }

    public void EndWarp()
    {
        //ExplosionPooler.instance.rippleEffect.SpawnDroplet(Camera.main.WorldToViewportPoint(transform.position));
        engineExhaust.gameObject.SetActive(true);
        SetAcceleration(true);
        warpActive = false;
        warp.Stop();
        warpDelayTimer = 0f;
        rb2d.velocity = Vector3.zero;
        if(laserSight)
            laserObject.SetActive(true);
        warpChargeAcc = 1;
    }

    public void PositionWarpCharge()
    {
        warpCharge.transform.position = transform.position + Vector3.up;
        warpChargeAcc += Time.deltaTime /3;
    }

    public void PickUpShieldPoint()
    {
        if(currentShieldPoints < 3)
            currentShieldPoints++;
        else
        {
            PickupManager.instance.SpawnPickUp(22, transform.position);
            return;
        }
        tempShield = shieldRegenTime * shieldRegenMultiplier;
        for (int i = 0; i < currentShieldPoints; i++)
        {
            uiConnector.shields[i].fillAmount = 1;
        }
        shieldPoints = currentShieldPoints;
        GameObject e = ExplosionPooler.instance.GetExplosion(12);
        e.transform.position = transform.position;
        e.SetActive(true);
        hull.invulnerable = true;
        shieldObject.SetActive(true);
        shieldTimerSet = false;
        uiConnector.UpdateShields();
    }

    public void BuildExtra(int index, bool isShield)
    {
        int build = 0;
        //TutorialControl.instance.ActivateLettersTuto();
        if (isShield)
        {
            if(shieldLetters[index])
            {
                PickupManager.instance.SpawnPickUp(3, transform.position);
                return;
            }
            shieldLetters[index] = true;
            for (int i = 0; i < 5; i++)
            {
                if (shieldLetters[i])
                    build++;
            }
            if(build == 5)
            {
                PickupManager.instance.SpawnPickUp(7, transform.position);
                shieldLetters = new bool[5];
            }
        }
        else
        {
            if(lifeLetters[index])
            {
                PickupManager.instance.SpawnPickUp(3, transform.position);
                return;
            }
            lifeLetters[index] = true;
            for (int i = 0; i < 5; i++)
            {
                if (lifeLetters[i])
                    build++;
            }
            if (build == 5)
            {
                PickupManager.instance.SpawnPickUp(6, transform.position);
                lifeLetters = new bool[5];
            }
        }

        uiConnector.UpdateLetters();
    }

    //public void AddBuildPiece(int index)
    //{
    //    if (index < 2 && buildingLife)
    //        return;
    //    if (index > 1 && buildingShield)
    //        return;

    //    if (index < 2)
    //        buildingShield = true;
    //    else
    //        buildingLife = true;

    //    if (buildingShield)
    //    {
    //        buildSlots[index] = true;

    //        if (buildSlots[0] && buildSlots[1])
    //        {
    //            builderAnim.Play("BuiltShield");
    //            PickupManager.instance.SpawnPickUp(7, transform.position);
    //            buildSlots[0] = false;
    //            buildSlots[1] = false;
    //            buildingShield = false;
    //        }
    //    }
    //    else
    //    {
    //        buildSlots[index - 2] = true;

    //        if (buildSlots[0] && buildSlots[1])
    //        {
    //            builderAnim.Play("BuiltLife");
    //            PickupManager.instance.SpawnPickUp(6, transform.position);
    //            buildSlots[0] = false;
    //            buildSlots[1] = false;
    //            buildingLife = false;
    //        }
    //    }

    //    //uiConnector.UpdateBuildSlots();
    //}

    public void ActivateLaserSight()
    {
        if(laserSight)
        {
            PickupManager.instance.SpawnPickUp(2, transform.position);
        }
        laserSight = true;
        laserObject.SetActive(true);
    }

    public void ActivateChaosGun(float duration = 5)
    {
        if(chaosDashed <= 0)
            weaponBeforeChaos = currentWeapon;
        chaosTimer = duration;
        chaosGun = true;
        uiConnector.ActivateTimer(2, chaosTimer);
    }

    public void Activatex2()
    {
        x2Timer = 15;
        x2Points = true;
        uiConnector.ActivateTimer(1, x2Timer);
    }

    public void AddGuardianShield()
    {
        if(guardianShield)
        {
            PickupManager.instance.SpawnPickUp(3, transform.position);
            return;
        }

        if (shieldPoints == 0)
        {
            hull.invulnerable = true;
            guardianShieldObj.SetActive(true);
        }

        avengerShield = false;
        guardianShield = true;
        uiConnector.UpdateExtraShields();
        return;
    }
    
    public void AddAvengerShield()
    {
        if(avengerShield)
        {
            PickupManager.instance.SpawnPickUp(3, transform.position);
            return;
        }

        if (shieldPoints == 0)
        {
            hull.invulnerable = true;
            avengerShieldObj.SetActive(true);
        }

        guardianShield = false;
        avengerShield = true;
        uiConnector.UpdateExtraShields();
        return;
    }

    public void ReadyForGameStart()
    {
        //gemCount = 500; // TODO TEMP
        //pineappleCount = 10;
        droneCount = 0;
        lifeLetters = new bool[5];
        shieldLetters = new bool[5];
        SetWeapon(Weapons.SINGLE_SHOT);
        dimensionalReload = false;
        absoluteCooling = false;
        bonusCoolingPoints = 0;
        bonusFireRatePoints = 0;
        autoTurretOn = false;
        currentShieldPoints = 1;
        //OnEnable();
    }

    public void ActivateAbsCooling()
    {
        SteamManager.Instance.UnlockAchievement("ABS_COOLING");
        absoluteCooling = true;
        uiConnector.UpdateCooling();
        OnEnable();
    }
    public void ActivateDimReload()
    {
        SteamManager.Instance.UnlockAchievement("DIM_RELOAD");
        dimensionalReload = true;
        uiConnector.UpdateFireRate();
        OnEnable();
    }

    public void CheckForDashAchievement()
    {
        if(dashUpgradeCount == 11)
            SteamManager.Instance.UnlockAchievement("DASH_UPGRADES");
    }
}
