﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class BeamAlien : SpecialAlien {

    public ScriptableBeamAlien beamAlienData;
    [SerializeField]
    public LayerMask playerMask;

    [SerializeField]
    private LineRenderer beamRenderer;
    [SerializeField]
    private GameObject beamObject;
    [SerializeField]
    private GameObject beamExplosion;
    [SerializeField]
    private GameObject laserSight;

    protected float fireCd;
    private Vector3 randomPosition;
    Vector2 distanceVec;
    Quaternion targetRot;
    private bool timeToFire;
    private float fireTimer;

    [ContextMenu("Remove that object reference error!")]
    public void GetSpecialAlien()
    {
        specialAlienData = beamAlienData;
        base.Awake();
    }

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        beamObject.SetActive(false);
        beamExplosion.SetActive(false);
        fireCd = Random.Range(5, beamAlienData.fireCooldown * 5);
        GetRandomPosition();
        timeToFire = false;
        laserSight.SetActive(false);

        //GameObject spawnExp = ExplosionPooler.instance.GetExplosion(49);
        //spawnExp.transform.position = transform.position;
        //spawnExp.SetActive(true);
    }

    private void GetRandomPosition()
    {
        randomPosition = Random.insideUnitCircle * 8;
    }

    protected override void Update()
    {
        base.Update();
        if (!GameControl.instance.paused)
        {
            fireCd -= Time.deltaTime;
            if(EnemyManager.instance.empActive)
            {
                beamObject.SetActive(false);
                beamExplosion.SetActive(false);
                laserSight.SetActive(false);
                return;
            }

            if (!timeToFire)
            {
                distanceVec = transform.position - randomPosition;
                rb2d.velocity -= distanceVec.normalized * beamAlienData.speed * Time.deltaTime;
                if (Extras.CompareVectors(transform.position, randomPosition, .2f))
                {
                    timeToFire = true;
                    fireCd = beamAlienData.fireCooldown;
                    fireTimer = 0;
                    if(canFire)
                        laserSight.SetActive(true);
                }
            }
            else
            {
                if (targetPlayer == 1)
                    distanceVec = transform.position - EnemyManager.instance.playerObject1.transform.position;
                else
                    distanceVec = transform.position - EnemyManager.instance.playerObject2.transform.position;

                if (fireCd < 0)
                    Fire();
            }

            if (beamAlienData.rotateTowardPlayer)
            {
                if (targetPlayer == 1)
                    distanceVec = transform.position - EnemyManager.instance.playerObject1.transform.position;
                else
                    distanceVec = transform.position - EnemyManager.instance.playerObject2.transform.position;
            }

            targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
            if(fireTimer > 0)
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRot, beamAlienData.turnSpeedWhenFiring * Time.deltaTime * 75);
            else
                transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, specialAlienData.turnSpeed * Time.deltaTime * 75);

        }
    }

    private void Fire()
    {
        fireTimer += Time.deltaTime;
        if (canFire)
        {
            beamObject.SetActive(true);
            beamExplosion.SetActive(true);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, beamAlienData.range, playerMask);
            if (hit)
            {
                Hull h = hit.collider.GetComponent<Hull>();

                if(h.invulnerable)
                {
                    if(h.CompareTag("Player1"))
                        PickupManager.instance.player1.DamageShield();
                    else if(h.CompareTag("Player2"))
                        PickupManager.instance.player2.DamageShield();
                }
                else
                    h.Damage(1, 0);
                beamExplosion.transform.position = hit.point;
                beamRenderer.SetPosition(1, Vector3.up * (hit.point - (Vector2)transform.position).magnitude);
            }
            else
            {
                beamRenderer.SetPosition(1, Vector3.up * beamAlienData.range);
                beamExplosion.transform.position = transform.up * beamAlienData.range + transform.position;
            }
        }

        if (fireTimer > beamAlienData.beamDuration)
            DisableBeam();
    }

    protected virtual void DisableBeam()
    {
        fireCd = beamAlienData.fireCooldown;
        beamObject.SetActive(false);
        beamExplosion.SetActive(false);
        timeToFire = false;
        GetRandomPosition();
        laserSight.SetActive(false);
        fireTimer = 0;
        ChangeTarget();
    }
}
