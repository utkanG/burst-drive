﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class Alien5_3 : CannonAlien {

    [SerializeField]
    private Transform[] arms;
    public float armRotationSpeed = 1;
    [SerializeField]
    private Transform[] eyes;
    private Vector3[] nextLocalPositions;
    private Vector3[] nextWorldPositions;

    protected override void Awake()
    {
        base.Awake();
        nextLocalPositions = new Vector3[4];
        nextWorldPositions = new Vector3[4];
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        foreach (Transform t in arms)
        {
            t.rotation = Quaternion.AngleAxis(Random.Range(0f, 360f), Vector3.forward);
        }
        for (int i = 0; i < 4; i++)
        {
            nextLocalPositions[i] = Random.insideUnitCircle * 2.4f;
        }
    }

    protected override void Update()
    {
        base.Update();
        for (int i = -4; i < 3; i++)
        {
            if (i == 0)
            {
                arms[i].Rotate(Vector3.forward, 3 * Time.deltaTime * armRotationSpeed * ((float)cannonAlienData.hitPoints / hull.hitPoints)/2);
                continue;
            }
            arms[i+4].Rotate(Vector3.forward, i * Time.deltaTime * armRotationSpeed * ((float)cannonAlienData.hitPoints / hull.hitPoints)/2);
        }
        for (int i = 0; i < 4; i++)
        {
            nextWorldPositions[i] = transform.position + nextLocalPositions[i];
            if (Extras.CompareVectors(eyes[i].position, nextWorldPositions[i], 1f))
            {
                nextLocalPositions[i] = Random.insideUnitCircle * 2.4f;
                eyes[i].rotation = Quaternion.AngleAxis(Mathf.Atan2((nextWorldPositions[i] - eyes[i].position).y, (nextWorldPositions[i] - eyes[i].position).x) * Mathf.Rad2Deg + 90, Vector3.forward);
            }
            else
            {
                eyes[i].position += (nextWorldPositions[i] - eyes[i].position).normalized * Time.deltaTime * 3.25f;
            }
        }
    }

    protected override void Fire()
    {
        fireCd = cannonAlienData.fireRate;
        GameObject tempProjectile = ProjectilePooler.instance.GetProjectile(13);
        GameObject e = ExplosionPooler.instance.GetExplosion(23);
        
        for (int i = 0; i < 4; i++)
        {
            tempProjectile = ProjectilePooler.instance.GetProjectile(13);
            e = ExplosionPooler.instance.GetExplosion(23);
            //if (targetPlayer == 1)
            //    distanceVec = eyes[i].position - EnemyManager.instance.playerObject1.transform.position;
            //else
            //    distanceVec = eyes[i].position - EnemyManager.instance.playerObject2.transform.position;

            //targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90 + Random.Range(-cannonAlienData.weaponSpread, cannonAlienData.weaponSpread), Vector3.forward);
            tempProjectile.transform.rotation = eyes[i].rotation;

            tempProjectile.transform.position = eyes[i].position;
            tempProjectile.SetActive(true);

            e.transform.position = eyes[i].position;
            e.SetActive(true);
        }
        base.Fire();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        if(Time.time > 1)
        {
            for (int i = 0; i < 2; i++)
            {
                GameObject e = ExplosionPooler.instance.GetExplosion(44);
                e.transform.position = eyes[i].position;
                // if gemfusion active
                // PickupManager.instance.SpawnPickUp(Random.Range(0,4), transform.position); // TEMP
                e.SetActive(true);
            }
            for (int i = 2; i < 4; i++)
            {
                GameObject e = ExplosionPooler.instance.GetExplosion(45);
                e.transform.position = eyes[i].position;
                // if gemfusion active
                // PickupManager.instance.SpawnPickUp(Random.Range(0,4), transform.position); // TEMP
                e.SetActive(true);
            }
        }
    }
}
