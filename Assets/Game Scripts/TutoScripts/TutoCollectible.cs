﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutoCollectible : MonoBehaviour {

    public static int collectibleCount = 0;

    private void OnEnable()
    {
        collectibleCount++;    
    }

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        Debug.Log(collectibleCount);
        gameObject.SetActive(false);
        GameObject explosion = ExplosionPooler.instance.GetExplosion(29);
        explosion.transform.position = transform.position;
        explosion.SetActive(true);
    }

    private void OnDisable()
    {
        collectibleCount--;

        if (collectibleCount == 0)
        {
            // next stage
        }
    }
}
