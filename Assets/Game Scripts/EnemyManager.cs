﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyManager : MonoBehaviour {

    public static EnemyManager instance;

    public int maxProjectileCount = 10;
    public int enemyCount = 0;
    public int projectileCount = 0;
    public GameObject playerObject1;
    public GameObject playerObject2;

    [Space(10)]

    public List<EnemyType> enemyTypes;
    public List<List<GameObject>> enemyPools = new List<List<GameObject>>();

    [SerializeField]
    private List<Hull> currentEnemyHulls = new List<Hull>();
    List<Hull> tempHulls = new List<Hull>();
    public Slider hpSlider;

    [HideInInspector]
    public List<Asteroid> spawnedAsteroids = new List<Asteroid>();
    private List<Asteroid> tempAsteroids = new List<Asteroid>();

    public bool empActive;
    private float empTimer;

    [System.Serializable]
    public class EnemyType
    {
        public string name;
        public GameObject alienPrefab;
        public int initialPoolAmount = 30;
    }

    public void BlowUpAsteroids()
    {
        tempAsteroids.AddRange(spawnedAsteroids);
        spawnedAsteroids.Clear();
        foreach (Asteroid asteroid in tempAsteroids)
        {
            asteroid.SeriousBlowUp();
        }
        tempAsteroids.Clear();
    }

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        InitializePools();
    }

    private void Update()
    {
        if (empActive)
        {
            empTimer -= Time.deltaTime;
            if (empTimer < 0)
            {
                empActive = false;
            }
        }
    }

    // temp debug
    public void AddAlien(Hull hull)
    {
        currentEnemyHulls.Add(hull);
    }

    // temp debug
    public void RemoveAlien(Hull hull)
    {
        currentEnemyHulls.Remove(hull);
    }

    private void InitializePools()
    {
        for (int i = 0; i < enemyTypes.Count; i++)
        {
            List<GameObject> a = new List<GameObject>();
            enemyPools.Add(a);

            for (int j = 0; j < enemyTypes[i].initialPoolAmount; j++)
            {
                GameObject go = Instantiate(enemyTypes[i].alienPrefab, transform);
                go.SetActive(false);
                a.Add(go);
            }
        }
    }

    public void EMP()
    {
        empTimer = 8;
        empActive = true;
    }

    public GameObject GetAlien(int poolIndex)
    {
        if (enemyPools[poolIndex].Count > 0)
        {
            GameObject a = enemyPools[poolIndex][0];
            enemyPools[poolIndex].Remove(enemyPools[poolIndex][0]);
            return a;
        }
        else
        {
            GameObject a = Instantiate(enemyTypes[poolIndex].alienPrefab, transform);
            a.SetActive(false);
            return a;
        }
    }

    public GameObject GetRandomT2Alien()
    {
        int indexToSpawn = 1;

        switch(Random.Range(0, 9))
        {
            case 0:
                indexToSpawn = 5;
                break;
            case 1:
                indexToSpawn = 18;
                break;
            case 2:
                indexToSpawn = 19;
                break;
            case 3:
                indexToSpawn = 21;
                break;
            case 4:
                indexToSpawn = 22;
                break;
            case 5:
                indexToSpawn = 23;
                break;
            case 6:
                indexToSpawn = 24;
                break;
            case 7:
                indexToSpawn = 30;
                break;
            case 8:
                indexToSpawn = 36;
                break;
        }

        if(enemyPools[indexToSpawn].Count > 0)
        {
            GameObject a = enemyPools[indexToSpawn][0];
            enemyPools[indexToSpawn].Remove(enemyPools[indexToSpawn][0]);
            return a;
        }
        else
        {
            GameObject a = Instantiate(enemyTypes[indexToSpawn].alienPrefab, transform);
            a.SetActive(false);
            return a;
        }
    }

    public GameObject GetRandomSpecialAlien()
    {
        int indexToSpawn = 1;
        switch(Random.Range(0, 13))
        {
            case 0:
                indexToSpawn = 1;
                break;
            case 1:
                indexToSpawn = 13;
                break;
            case 2:
                indexToSpawn = 14;
                break;
            case 3:
                indexToSpawn = 15;
                break;
            case 4:
                indexToSpawn = 17;
                break;
            case 5:
                indexToSpawn = 20;
                break;
            case 6:
                indexToSpawn = 26;
                break;
            case 7:
                indexToSpawn = 27;
                break;
            case 8:
                indexToSpawn = 28;
                break;
            case 9:
                indexToSpawn = 29;
                break;
            case 10:
                indexToSpawn = 31;
                break;
            case 11:
                indexToSpawn = 32;
                break;
            case 12:
                indexToSpawn = 37;
                break;
        }

        if(enemyPools[indexToSpawn].Count > 0)
        {
            GameObject a = enemyPools[indexToSpawn][0];
            enemyPools[indexToSpawn].Remove(enemyPools[indexToSpawn][0]);
            return a;
        }
        else
        {
            GameObject a = Instantiate(enemyTypes[indexToSpawn].alienPrefab, transform);
            a.SetActive(false);
            return a;
        }
    }

    public void ReturnAlienToPool(int poolIndex, GameObject a)
    {
        enemyPools[poolIndex].Add(a);
        a.SetActive(false);
    }
    
    [ContextMenu("Kill All")]
    public void KillAll()
    {
        tempHulls.AddRange(currentEnemyHulls);
        foreach (Hull h in tempHulls)
        {
            h.Damage(1000, 0);
        }
        tempHulls.Clear();
        currentEnemyHulls.Clear();
    }
}
