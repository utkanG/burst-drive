﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Utilities;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance;
    
    public AudioMixer mainMixer;

    [SerializeField]
    private AudioSource pickupSoundSource;
    [SerializeField]
    private AudioSource otherSoundSource;

    public List<AudioClip> pickupAudioClips = new List<AudioClip>();
    public List<AudioClip> vfxSoundClips = new List<AudioClip>();

    private const string fxPitch = "Pitch";
    private const string pickUpPitch = "PitchP";
    private const string fxVol = "FXVol";

    public int maxPooledSounds;
    [SerializeField]
    private GameObject soundObjPrefab;
    [SerializeField]
    private List<SoundToPool> pooledSounds;

    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);

        SoundToPool t;
        for(int m = 0; m < maxPooledSounds; m++)
        {
            t = Instantiate(soundObjPrefab, transform).GetComponent<SoundToPool>();
            t.gameObject.SetActive(false);
            pooledSounds.Add(t);
            currentCount++;
        }
    }

    private void Update()
    {
        mainMixer.SetFloat(fxPitch, Time.timeScale);
        mainMixer.SetFloat(pickUpPitch, Time.timeScale);
    }

    public void SetVolume(float vol)
    {
        if(vol == 0) 
            mainMixer.SetFloat(fxVol, -80);
        else
            mainMixer.SetFloat(fxVol, Mathf.Log10(vol) * 20);
    }

    public void PlayPickupSound(int index)
    {
        pickupSoundSource.PlayOneShot(pickupAudioClips[index]);
    }
    public void PlayPickupSound(int index, float vol)
    {
        pickupSoundSource.volume = vol;
        pickupSoundSource.PlayOneShot(pickupAudioClips[index]);
    }
    public void PlayFXSound(int index, float pitch, float vol, float customLifetime = 1)
    {
        if(Time.time < 1) return;
        //otherSoundSource.pitch = pitch;
        //otherSoundSource.volume = fxVolume * vol;
        //otherSoundSource.PlayOneShot(vfxSoundClips[index]);

        SoundToPool newSource = GetAudioSource();
        newSource.source.pitch = pitch;
        newSource.source.volume = vol;
        newSource.source.outputAudioMixerGroup = otherSoundSource.outputAudioMixerGroup;
        newSource.source.clip = (vfxSoundClips[index]);
        newSource.gameObject.SetActive(true);
        newSource.ReturnToPoolIn(customLifetime);
    }

    private int currentCount;
    private SoundToPool GetAudioSource()
    {
        if(currentCount <= 0) 
        {
            SoundToPool t = Instantiate(soundObjPrefab, transform).GetComponent<SoundToPool>();
            t.gameObject.SetActive(false);
            return t; 
        }

        currentCount--;
        SoundToPool toReturn = pooledSounds[currentCount];
        pooledSounds.RemoveAt(currentCount);
        return toReturn;
    }

    public void ReturnAudioSource(SoundToPool a)
    {
        pooledSounds.Add(a);
        currentCount++;
        a.gameObject.SetActive(false);
    }

    public void PlayFXSound(int index, float pitch, float vol, float customLifetime, int priority = 128)
    {
        if(Time.time < 1) return;
        //otherSoundSource.pitch = pitch;
        //otherSoundSource.volume = fxVolume * vol;
        //otherSoundSource.PlayOneShot(vfxSoundClips[index]);

        SoundToPool newSource = GetAudioSource();
        newSource.source.pitch = pitch;
        newSource.source.volume = vol;
        newSource.source.priority = priority;
        newSource.source.outputAudioMixerGroup = otherSoundSource.outputAudioMixerGroup;
        newSource.source.clip = (vfxSoundClips[index]);
        newSource.gameObject.SetActive(true);
        newSource.ReturnToPoolIn(customLifetime);
    }

    public void PlayFXSound(int index, float pitch, float vol, float customLifetime, bool bypassEffect)
    {
        if(Time.time < 1) return;
        //otherSoundSource.pitch = pitch;
        //otherSoundSource.volume = fxVolume * vol;
        //otherSoundSource.PlayOneShot(vfxSoundClips[index]);

        SoundToPool newSource = GetAudioSource();
        newSource.source.pitch = pitch;
        newSource.source.volume = vol;
        newSource.source.bypassEffects = bypassEffect;
        newSource.source.outputAudioMixerGroup = otherSoundSource.outputAudioMixerGroup;
        newSource.source.clip = (vfxSoundClips[index]);
        newSource.gameObject.SetActive(true);
        newSource.ReturnToPoolIn(customLifetime);
    }
}
