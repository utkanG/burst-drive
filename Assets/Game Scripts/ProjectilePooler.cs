﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePooler : MonoBehaviour {

    public static ProjectilePooler instance;

    public List<ProjectileType> projectileTypes;
    public List<List<GameObject>> projectilePools = new List<List<GameObject>>();

    public List<GameObject> spawnedProjectiles = new List<GameObject>();

    [System.Serializable]
    public class ProjectileType
    {
        public string name;
        public GameObject projectilePrefab;
        public int initialPoolAmount = 50;
    }
    
    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        InitializePools();
        ClearProjectiles();
    }

    private void InitializePools()
    {
        for (int i = 0; i < projectileTypes.Count; i++)
        {
            List<GameObject> p = new List<GameObject>();
            projectilePools.Add(p);

            for (int j = 0; j < projectileTypes[i].initialPoolAmount; j++)
            {
                GameObject go = Instantiate(projectileTypes[i].projectilePrefab, transform);
                go.SetActive(false);
                p.Add(go);
            }
        }
    }

    public GameObject GetProjectile(int poolIndex)
    {
        if(projectilePools[poolIndex].Count > 0)
        {
            GameObject p = projectilePools[poolIndex][0];
            projectilePools[poolIndex].Remove(projectilePools[poolIndex][0]);
            return p;
        }
        else
        {
            GameObject p = Instantiate(projectileTypes[poolIndex].projectilePrefab, transform);
            p.SetActive(false);
            return p;
        }
    }

    public void ReturnProjectileToPool(int poolIndex, GameObject p)
    {
        projectilePools[poolIndex].Add(p);
        spawnedProjectiles.Remove(p);
        p.SetActive(false);
    }

    public void ClearProjectiles()
    {
        foreach(GameObject go in spawnedProjectiles)
        {
            go.SetActive(false);
        }

        spawnedProjectiles.Clear();
        EnemyManager.instance.projectileCount = 0;
    }
}
