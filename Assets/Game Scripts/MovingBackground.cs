﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBackground : MonoBehaviour {

    [SerializeField]
    private Transform[] playerTransforms;

    [SerializeField]
    private Transform[] starTransforms;

    Vector3 clampedPosition;
    private void Update()
    {
        if (playerTransforms[0].gameObject.activeInHierarchy)
            clampedPosition += playerTransforms[0].position.normalized/2f;
        if (playerTransforms[1].gameObject.activeInHierarchy)
            clampedPosition += playerTransforms[1].position.normalized/2f;
        clampedPosition = clampedPosition / 2f;
        clampedPosition = Vector2.ClampMagnitude(clampedPosition, 1);
        transform.position = Vector2.Lerp(transform.position, -clampedPosition, Time.deltaTime);
        for (int i = 0; i < 4; i++)
        {
            starTransforms[i].position = Vector2.Lerp(starTransforms[i].position, -clampedPosition, Time.deltaTime * i /1.5f);
        }
    }
}
