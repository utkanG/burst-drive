﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShopController : MonoBehaviour {

    [SerializeField]
    private GameObject shopObject;

    private bool forP2Occured = false;
    private int currentPlayerNo = 0;

    [System.Serializable]
    public class ShopItem
    {
        public string name;
        [TextArea(4, 10)]
        public string description;
        public int gemPrice;
        public int pineapplePrice;
        public int tier;
    }

    public Button[] itemButtons;
    public Image[] buttonImages;

    public Text shipStatsText;
    public Text playerNoText;
    public Text[] gemNPineappleCountTexts;
    public Text descTitleText;
    public Text descText;
    public List<ShopItem> shopItems = new List<ShopItem>();
    public GameObject exitButtonObject;

    public Image[] shopPanels;
    public Sprite[] blueSprites;
    public Sprite[] redSprites;
    public Image exitButtonImage;

    [Header("Selection Stuff")]
    public Sprite selectedAvailableImage;
    public Sprite selectedUnavailableImage;
    public Image selectionImage;
    public RectTransform selectionTransform;
    public Color unavailableColor;
    public Transform itemPanelTransform;
    public Transform shopPanelTransform;

    private ShopItem[] pineItems = new ShopItem[5];
    private GameObject lastSelected;
    private int lastSelectedOrder;
    private bool currentItemAvailable;
    private int currentSelectedOrder;
    private bool spentGem;

    [SerializeField]
    private KeyCode[] currentKeyCodes = new KeyCode[3]; // 0 = up, 1 = down, 2 = fire/buy
    private bool[] boughtPineItem = new bool[5];
    [SerializeField]
    private Text successText;

    private ShopItem tempFireRate; // 20 gem
    private ShopItem tempCooling; // 30 gem will be hard coded and it will be disgusting

    public StandaloneInputModule inputModule;

    private int totalPineItemBought = 0;
    [SerializeField]
    private GameObject gemImgAnim;
    [SerializeField]
    private GameObject pineImgAnim;

    [Header("Tier Options")]
    [SerializeField]
    private Color[] tierColors;
    [SerializeField]
    private Text tierText;

    private bool shopActive = false;

    private void Start()
    {
        for (int i = 5; i < 18; i++)
        {
            itemButtons[i].transform.GetChild(0).GetComponent<Text>().text = "   " + shopItems[i-5].name;
            itemButtons[i].transform.GetChild(1).GetComponent<Text>().text = shopItems[i-5].gemPrice + " ";
        }
        tempItems = new List<ShopItem>();

        tempFireRate = shopItems[1];
        tempCooling = shopItems[3];
        shopActive = false;
    }

    private void ActivateSuccessText(string message)
    {
        successText.gameObject.SetActive(false);
        successText.text = message;
        successText.gameObject.SetActive(true);
    }

    private void CheckForSpecials(int playerNo)
    {
        // putting
        // 27 to 1
        // 26 to 3
        if(playerNo == 1)
        {
            if(GameControl.instance.player1Script.bonusFireRatePoints >= 20)
            {
                shopItems[1] = shopItems[27];
            }
            else
            {
                shopItems[1] = tempFireRate;
            }


            if(GameControl.instance.player1Script.bonusCoolingPoints >= 20)
            {
                shopItems[3] = shopItems[26];
            }
            else
            {
                shopItems[3] = tempCooling;
            }

        }
        else
        {
            if(GameControl.instance.player2Script.bonusFireRatePoints >= 20)
            {
                shopItems[1] = shopItems[27];
            }
            else
            {
                shopItems[1] = tempFireRate;
            }

            if(GameControl.instance.player2Script.bonusCoolingPoints >= 20)
            {
                shopItems[3] = shopItems[26];
            }
            else
            {
                shopItems[3] = tempCooling;
            }
        }

        itemButtons[6].transform.GetChild(0).GetComponent<Text>().text = "   " + shopItems[1].name;
        itemButtons[6].transform.GetChild(1).GetComponent<Text>().text = shopItems[1].gemPrice + " ";
        itemButtons[8].transform.GetChild(0).GetComponent<Text>().text = "   " + shopItems[3].name;
        itemButtons[8].transform.GetChild(1).GetComponent<Text>().text = shopItems[3].gemPrice + " ";
    }

    public void BuyItemWithGem(int order)
    {
        if(!shopActive) return;
        if (currentItemAvailable && shopObject.activeInHierarchy)
        {
            spentGem = true;
            DeductResource(shopItems[order-5].gemPrice, currentPlayerNo, true);
            if(GetItem(shopItems[order - 5].name))
            {
                successText.color = Color.green;
                ActivateSuccessText(shopItems[order - 5].name + " INSTALLED");
                SoundManager.instance.PlayFXSound(20, 1f, 1f, 1f);
                gemImgAnim.SetActive(false);
                gemImgAnim.SetActive(true);
            }
            else
            {
                successText.color = new Color(1f, .64f, 0f, 1f);
                ActivateSuccessText("ALREADY INSTALLED");
                SoundManager.instance.PlayFXSound(21, 1f, .5f, 1f);
            }
            CheckItemAvailabilities();
            SelectItem(order);
        }
        else
            SoundManager.instance.PlayFXSound(21, 1f, .5f, 1f);
    }

    private int GetPinePriceWithInflation(int originalPrice)
    {
        return (originalPrice + totalPineItemBought * totalPineItemBought) * (LevelManager.instance.Looping * 2 + 1) * (GameControl.instance.bossKillCount + 1);
    }

    public void BuyItemWithPine(int order)
    {
        if(!shopActive) return;
        if(currentItemAvailable && shopObject.activeInHierarchy)
        {
            spentGem = false;
            DeductResource(GetPinePriceWithInflation(pineItems[order].pineapplePrice), currentPlayerNo, false);
            if (GetItem(pineItems[order].name))
            {
                successText.color = Color.green;
                boughtPineItem[order] = true;
                SoundManager.instance.PlayFXSound(20, .9f, 1f, 1f);
                ActivateSuccessText(pineItems[order].name + " INSTALLED");
                itemButtons[order].transform.GetChild(1).GetComponent<Text>().text = "SOLD OUT ";
                totalPineItemBought++;
                pineImgAnim.SetActive(false);
                pineImgAnim.SetActive(true);
                UpdatePinePrices();
            }
            else
            {
                successText.color = new Color(1f, .64f, 0f, 1f);
                SoundManager.instance.PlayFXSound(21, 1f, 1f, 1f);
                ActivateSuccessText("ALREADY INSTALLED");
            }
            CheckItemAvailabilities();
            SelectItem(order);
        }
        else
            SoundManager.instance.PlayFXSound(21, 1f, .4f, 1f, true);
    }

    private void UpdateShipStats()
    {
        string absolute;
        string dimensional;
        if(currentPlayerNo == 1)
        {
            if (GameControl.instance.player1Script.dimensionalReload) dimensional = "DIMENSIONAL";
            else dimensional = GameControl.instance.player1Script.bonusFireRatePoints.ToString() + "/20";
            if (GameControl.instance.player1Script.absoluteCooling) absolute = "ABSOLUTE";
            else absolute = GameControl.instance.player1Script.bonusCoolingPoints.ToString() + "/20";

            shipStatsText.text = "<color=#add8e6ff>WEAPON:</color> " + GameControl.instance.player1Script.weapons[(int)GameControl.instance.player1Script.currentWeapon].name + "\n" +
                "<color=#ffa500ff>FIRE RATE:</color> " + dimensional + "\n" +
                "<color=#00ffffff>COOLING</color>: " + absolute + "\n" +
                "<color=#0000ffff>AEGIS SHIELDS:</color> " + GameControl.instance.player1Script.shieldPoints + "/3";
        }
        else
        {
            if (GameControl.instance.player2Script.dimensionalReload) dimensional = "DIMENSIONAL";
            else dimensional = GameControl.instance.player2Script.bonusFireRatePoints.ToString() + "/20";
            if (GameControl.instance.player2Script.absoluteCooling) absolute = "ABSOLUTE";
            else absolute = GameControl.instance.player2Script.bonusCoolingPoints.ToString() + "/20";

            shipStatsText.text = "<color=#add8e6ff>WEAPON:</color> " + GameControl.instance.player2Script.weapons[(int)GameControl.instance.player2Script.currentWeapon].name + "\n" +
                "<color=#ffa500ff>FIRE RATE:</color> " + dimensional + "\n" +
                "<color=#00ffffff>COOLING</color>: " + absolute + "\n" +
                "<color=#0000ffff>AEGIS SHIELDS:</color> " + GameControl.instance.player2Script.shieldPoints + "/3";
        }
    }

    private void Update()
    {
        if (shopObject.activeInHierarchy)
        {
            if (Input.GetKeyDown(currentKeyCodes[0])) // up
            {
                currentSelectedOrder++;
                SwitchToButton(currentSelectedOrder);
                SelectItem(currentSelectedOrder);
                //EventSystem.current.SetSelectedGameObject(itemButtons[currentSelectedOrder].gameObject);
            }
            else if (Input.GetKeyDown(currentKeyCodes[1])) // down
            {
                currentSelectedOrder--;
                SwitchToButton(currentSelectedOrder);
                SelectItem(currentSelectedOrder);
                //EventSystem.current.SetSelectedGameObject(itemButtons[currentSelectedOrder].gameObject);
            }
            else if (Input.GetKeyDown(currentKeyCodes[2])) // fire
            {
                if (currentSelectedOrder <= -1)
                    DisableShop();
                else if (currentSelectedOrder <= 4)
                    BuyItemWithPine(currentSelectedOrder);
                else if (currentSelectedOrder > 4)
                    BuyItemWithGem(currentSelectedOrder);
            }

            if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton7))
            {
                shopActive = false;
                DisableShop();
            }
            //else if (Input.GetMouseButtonDown(0))
            //{
            //    if (!EventSystem.current.currentSelectedGameObject)
            //    {
            //        if (lastSelectedOrder <= -1)
            //            EventSystem.current.SetSelectedGameObject(exitButtonObject);
            //        else
            //            EventSystem.current.SetSelectedGameObject(itemButtons[lastSelectedOrder].gameObject);
            //    }
            //}
        }
    }

    private void SwitchToButton(int order)
    {
        if (order > 17)
            order = 17;
        else if (order < -1)
            order = -1;
        SoundManager.instance.PlayFXSound(22, 1.25f, .5f, 1f, true);
        currentSelectedOrder = order;
    }

    private bool ComparePrice(int itemCost, int playerNo, bool gem)
    {
        if (gem)
        {
            if (playerNo == 1)
            {
                if (itemCost <= GameControl.instance.player1Script.gemCount)
                    return true;
                else
                    return false;
            }
            else
            {
                if (itemCost <= GameControl.instance.player2Script.gemCount)
                    return true;
                else
                    return false;
            }
        }
        else
        {
            if (playerNo == 1)
            {
                if (itemCost <= GameControl.instance.player1Script.pineappleCount)
                    return true;
                else
                    return false;
            }
            else
            {
                if (itemCost <= GameControl.instance.player2Script.pineappleCount)
                    return true;
                else
                    return false;
            }
        }
    }

    private void ReturnItem(int currentPlayerNo, bool gem, int index)
    {
        if (spentGem)
            DeductResource(-shopItems[index].gemPrice, currentPlayerNo, spentGem);
        else
            DeductResource(-GetPinePriceWithInflation(shopItems[index].pineapplePrice), currentPlayerNo, spentGem);

        // TODO if pine don't disable pine button
    }

    private bool GetItem(string itemName)
    {
        bool success = false;
        switch (itemName)
        {
            case "PLASMA":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.PLASMA) || 
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.PLASMA)) { ReturnItem(currentPlayerNo, spentGem, 25); break; }
                
                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.PLASMA);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.PLASMA);
                success = true;
                break;
            case "ABSOLUTE COOLING":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.absoluteCooling) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.absoluteCooling)) { ReturnItem(currentPlayerNo, spentGem, 26); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.ActivateAbsCooling();
                else GameControl.instance.player2Script.ActivateAbsCooling();
                success = true;
                break;
            case "DIMENSIONAL RELOAD":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.dimensionalReload) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.dimensionalReload)) { ReturnItem(currentPlayerNo, spentGem, 27); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.ActivateDimReload();
                else GameControl.instance.player2Script.ActivateDimReload();
                success = true;
                break;
            case "PARTICLE BEAM":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.PARTICLE_BEAM) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.PARTICLE_BEAM)) { ReturnItem(currentPlayerNo, spentGem, 17); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.PARTICLE_BEAM);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.PARTICLE_BEAM);
                success = true;
                break;
            case "ARC SPHERES":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.ARC_SPHERE) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.ARC_SPHERE)) { ReturnItem(currentPlayerNo, spentGem, 14); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.ARC_SPHERE);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.ARC_SPHERE);
                success = true;
                break;
            case "EXTRA LIFE":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Lives >= 4) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Lives >= 4)) { ReturnItem(currentPlayerNo, spentGem, 15); break; }

                if (currentPlayerNo == 1)
                {
                    GameControl.instance.AddLife(1, "1");
                    PickupManager.instance.player1.uiConnector.UpdateLives();
                }
                else
                {
                    GameControl.instance.AddLife(1, "2");
                    PickupManager.instance.player2.uiConnector.UpdateLives();
                }
                success = true;
                break;
            case "AUTO TURRET":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.autoTurretOn) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.autoTurretOn)) { ReturnItem(currentPlayerNo, spentGem, 13); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.ActivateAutoTurret();
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(0);
                }
                else
                {
                    GameControl.instance.player2Script.ActivateAutoTurret();
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(0);
                }
                success = true;
                break;
            case "GAMMARANG":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.GAMMARANG) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.GAMMARANG)) { ReturnItem(currentPlayerNo, spentGem, 16); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.GAMMARANG);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.GAMMARANG);
                success = true;
                break;
            case "AEGIS SHIELD":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentShieldPoints >= 3) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentShieldPoints >= 3)) { ReturnItem(currentPlayerNo, spentGem, 12); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.PickUpShieldPoint();
                else GameControl.instance.player2Script.PickUpShieldPoint();
                success = true;
                break;
            case "LASER SIGHT":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.laserSight) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.laserSight)) { ReturnItem(currentPlayerNo, spentGem, 0); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.ActivateLaserSight();
                else GameControl.instance.player2Script.ActivateLaserSight();
                success = true;
                break;
            case "DRONE PACK":
                if (currentPlayerNo == 1) GameControl.instance.player1Script.AddDrones(5);
                else GameControl.instance.player2Script.AddDrones(5);
                success = true;
                break;
            case "SCATTER LASER":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.SCATTER_LASER) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.SCATTER_LASER)) { ReturnItem(currentPlayerNo, spentGem, 11); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.SCATTER_LASER);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.SCATTER_LASER);
                success = true;
                break;
            case "AVENGER SHIELD":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.avengerShield) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.avengerShield)) { ReturnItem(currentPlayerNo, spentGem, 7); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.AddAvengerShield();
                else GameControl.instance.player2Script.AddAvengerShield();
                success = true;
                break;
            case "PHOTON MISSILES":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.PHOTON_MISSILE) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.PHOTON_MISSILE)) { ReturnItem(currentPlayerNo, spentGem, 10); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.PHOTON_MISSILE);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.PHOTON_MISSILE);
                success = true;
                break;
            case "DOUBLE SHOT":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.DOUBLE_SHOT) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.DOUBLE_SHOT)) { ReturnItem(currentPlayerNo, spentGem, 2); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.DOUBLE_SHOT);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.DOUBLE_SHOT);
                success = true;
                break;
            case "TRIPLE SHOT":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.TRIPLE_SHOT) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.TRIPLE_SHOT)) { ReturnItem(currentPlayerNo, spentGem, 4); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.TRIPLE_SHOT);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.TRIPLE_SHOT);
                success = true;
                break;
            case "GUARDIAN SHIELD":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.guardianShield) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.guardianShield)) { ReturnItem(currentPlayerNo, spentGem, 5); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.AddGuardianShield();
                else GameControl.instance.player2Script.AddGuardianShield();
                success = true;
                break;
            case "ION CANNON":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.ION_CANNON) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.ION_CANNON)) { ReturnItem(currentPlayerNo, spentGem, 4); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.ION_CANNON);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.ION_CANNON);
                success = true;
                break;
            case "GEMINI BLAST":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.GEMINI_BLAST) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.GEMINI_BLAST)) { ReturnItem(currentPlayerNo, spentGem, 19); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.GEMINI_BLAST);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.GEMINI_BLAST);
                success = true;
                break;
            case "COOLING CORE":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.bonusCoolingPoints >= 20) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.bonusCoolingPoints >= 20)) { ReturnItem(currentPlayerNo, spentGem, 3); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.AddCooling();
                else GameControl.instance.player2Script.AddCooling();
                success = true;
                break;
            case "FIRE RATE CORE":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.bonusFireRatePoints >= 20) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.bonusFireRatePoints >= 20)) { ReturnItem(currentPlayerNo, spentGem, 1); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.AddFireRate();
                else GameControl.instance.player2Script.AddFireRate();
                success = true;
                break;
            case "CHAIN GUN":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.CHAIN_GUN) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.CHAIN_GUN)) { ReturnItem(currentPlayerNo, spentGem, 21); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.CHAIN_GUN);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.CHAIN_GUN);
                success = true;
                break;
            case "GATLING LASER":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.GATLING_LASER) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.GATLING_LASER)) { ReturnItem(currentPlayerNo, spentGem, 20); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.GATLING_LASER);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.GATLING_LASER);
                success = true;
                break;
            case "HULL BUSTER":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.HULL_BUSTER) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.HULL_BUSTER)) { ReturnItem(currentPlayerNo, spentGem, 18); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.HULL_BUSTER);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.HULL_BUSTER);
                success = true;
                break;
            case "MICRO ROCKETS":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.MICRO_ROCKETS) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.MICRO_ROCKETS)) { ReturnItem(currentPlayerNo, spentGem, 6); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.MICRO_ROCKETS);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.MICRO_ROCKETS);
                success = true;
                break;
            case "POMEGRANATE":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.POMEGRANATE) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.POMEGRANATE)) { ReturnItem(currentPlayerNo, spentGem, 24); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.POMEGRANATE);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.POMEGRANATE);
                success = true;
                break;
            case "SUPER MOLTEN SHOT":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.S_MOLTEN_SHOT) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.S_MOLTEN_SHOT)) { ReturnItem(currentPlayerNo, spentGem, 22); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.S_MOLTEN_SHOT);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.S_MOLTEN_SHOT);
                success = true;
                break;
            case "RAIL GUN":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.RAIL_GUN) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.RAIL_GUN)) { ReturnItem(currentPlayerNo, spentGem, 23); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.RAIL_GUN);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.RAIL_GUN);
                success = true;
                break;
            case "THUNDER ROCKETS":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.SPARK_MISSILES) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.SPARK_MISSILES)) { ReturnItem(currentPlayerNo, spentGem, 28); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.SPARK_MISSILES);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.SPARK_MISSILES);
                success = true;
                break;
            case "UNSTABLE ION GUN":
                if ((currentPlayerNo == 1 && GameControl.instance.player1Script.currentWeapon == PlayerShip.Weapons.UNSTABLE_ION) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.currentWeapon == PlayerShip.Weapons.UNSTABLE_ION)) { ReturnItem(currentPlayerNo, spentGem, 29); break; }

                if (currentPlayerNo == 1) GameControl.instance.player1Script.SetWeapon(PlayerShip.Weapons.UNSTABLE_ION);
                else GameControl.instance.player2Script.SetWeapon(PlayerShip.Weapons.UNSTABLE_ION);
                success = true;
                break;

            case "HYPERSPACE NET":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.dashPickup == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.dashPickup == true)) { ReturnItem(currentPlayerNo, spentGem, 30); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.dashPickup = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(5);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else
                { 
                    GameControl.instance.player2Script.dashPickup = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(5);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "CRYOGENIC VENTS":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.advancedVents == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.advancedVents == true)) { ReturnItem(currentPlayerNo, spentGem, 31); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.advancedVents = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(2);
                }
                else 
                { 
                    GameControl.instance.player2Script.advancedVents = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(2);
                }
                success = true;
                break;
            case "CHAOS DICE":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.chaosDash == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.chaosDash == true)) { ReturnItem(currentPlayerNo, spentGem, 32); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.chaosDash = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(10);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else { 
                    GameControl.instance.player2Script.chaosDash = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(10);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "4D PRINTER":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.duplicateDash == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.duplicateDash == true)) { ReturnItem(currentPlayerNo, spentGem, 33); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.duplicateDash = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(9);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else
                { 
                    GameControl.instance.player2Script.duplicateDash = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(9);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "DARK ENERGY DYNAMO":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.doubleDash == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.doubleDash == true)) { ReturnItem(currentPlayerNo, spentGem, 34); break; }

                if(currentPlayerNo == 1) 
                { 
                    GameControl.instance.player1Script.doubleDash = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(4);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else
                { 
                    GameControl.instance.player2Script.doubleDash = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(4);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "RESONANCE BATTERY":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.increasedPhase == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.increasedPhase == true)) { ReturnItem(currentPlayerNo, spentGem, 35); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.increasedPhase = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(3);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else 
                { 
                    GameControl.instance.player2Script.increasedPhase = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(3);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "MINE DISPENSER":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.dashMine == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.dashMine == true)) { ReturnItem(currentPlayerNo, spentGem, 36); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.dashMine = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(6);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else
                { 
                    GameControl.instance.player2Script.dashMine = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(6);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "ANTI MATTER SPOILER":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.particleDash == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.particleDash == true)) { ReturnItem(currentPlayerNo, spentGem, 37); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.particleDash = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(8);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else
                { 
                    GameControl.instance.player2Script.particleDash = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(8);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "HEAT VACUUM":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.freeDash == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.freeDash == true)) { ReturnItem(currentPlayerNo, spentGem, 38); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.freeDash = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(11);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else 
                {
                    GameControl.instance.player2Script.freeDash = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(11);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "GRAVITON BRAKE":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.pushAtDestDash == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.pushAtDestDash == true)) { ReturnItem(currentPlayerNo, spentGem, 39); break; }

                if(currentPlayerNo == 1) 
                {
                    GameControl.instance.player1Script.pushAtDestDash = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(13);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else 
                { 
                    GameControl.instance.player2Script.pushAtDestDash = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(13);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "GRAVITON EXHAUST":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.pullAtOriginDash == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.pullAtOriginDash == true)) { ReturnItem(currentPlayerNo, spentGem, 40); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.pullAtOriginDash = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(12);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else
                {
                    GameControl.instance.player2Script.pullAtOriginDash = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(12);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "SUPER ION PROPULSION":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.ionCannonDash == true) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.ionCannonDash == true)) { ReturnItem(currentPlayerNo, spentGem, 41); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.ionCannonDash = true;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(7);
                    GameControl.instance.player1Script.dashUpgradeCount++;
                }
                else
                {
                    GameControl.instance.player2Script.ionCannonDash = true;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(7);
                    GameControl.instance.player2Script.dashUpgradeCount++;
                }
                success = true;
                break;
            case "AEGIS ACCELERATOR":
                if((currentPlayerNo == 1 && GameControl.instance.player1Script.shieldRegenMultiplier == 2) ||
                    (currentPlayerNo == 2 && GameControl.instance.player2Script.shieldRegenMultiplier == 2)) { ReturnItem(currentPlayerNo, spentGem, 42); break; }

                if(currentPlayerNo == 1)
                {
                    GameControl.instance.player1Script.shieldRegenMultiplier = 2;
                    GameControl.instance.player1Script.uiConnector.ActivateItemImage(1);
                }
                else
                {
                    GameControl.instance.player2Script.shieldRegenMultiplier = 2;
                    GameControl.instance.player2Script.uiConnector.ActivateItemImage(1);
                }
                success = true;
                break;
            default:
                Debug.LogError("There is no such item, this shouldn't happen, what did you do?!?!¿");
                break;
        }

        if(currentPlayerNo == 1)
            GameControl.instance.player1Script.CheckForDashAchievement();
        else
            GameControl.instance.player2Script.CheckForDashAchievement();

        return success;
    }

    private void UpdateResourceTexts()
    {
        if(currentPlayerNo == 1)
        {
            GameControl.instance.player1Script.uiConnector.UpdateInventory();
            gemNPineappleCountTexts[0].text = GameControl.instance.player1Script.gemCount.ToString() + "   ";
            gemNPineappleCountTexts[1].text = GameControl.instance.player1Script.pineappleCount.ToString() + "   ";
        }
        else
        {
            GameControl.instance.player2Script.uiConnector.UpdateInventory();
            gemNPineappleCountTexts[0].text = GameControl.instance.player2Script.gemCount.ToString() + "   ";
            gemNPineappleCountTexts[1].text = GameControl.instance.player2Script.pineappleCount.ToString() + "   ";
        }
    }

    private void CheckItemAvailabilities()
    {
        CheckForSpecials(currentPlayerNo);
        for (int i = 0; i < 18; i++)
        {
            if (i <= 4)
            {
                if (ComparePrice(GetPinePriceWithInflation(pineItems[i].pineapplePrice), currentPlayerNo, false) && !boughtPineItem[i])
                    buttonImages[i].color = Color.white;
                else
                    buttonImages[i].color = unavailableColor;
            }
            else
            {
                if (ComparePrice(shopItems[i - 5].gemPrice, currentPlayerNo, true))
                    buttonImages[i].color = Color.white;
                else
                    buttonImages[i].color = unavailableColor;
            }
        }
        UpdateShipStats();
    }

    private void DeductResource(int amount, int playerNo, bool gem)
    {
        if (gem)
        {
            if (playerNo == 1)
                GameControl.instance.player1Script.gemCount -= amount;
            else
                GameControl.instance.player2Script.gemCount -= amount;
        }
        else
        {
            if (playerNo == 1)
                GameControl.instance.player1Script.pineappleCount -= amount;
            else
                GameControl.instance.player2Script.pineappleCount -= amount;
        }
        UpdateResourceTexts();
    }

    private void GetKeyCodes(int player)
    {
        if (player == 1)
        {
            if (GameControl.instance.player1Script.controllerInput)
            {
                currentKeyCodes[0] = KeyCode.None;
                currentKeyCodes[1] = KeyCode.None;
                currentKeyCodes[2] = KeyCode.None;
                inputModule.verticalAxis = "Vertical" + GameControl.instance.player1Script.controllerIndex;
                currentKeyCodes[2] = GameControl.instance.player1Script.keyCodes[5];
            }
            else
            {
                currentKeyCodes[0] = GameControl.instance.player1Script.keyCodes[3];
                currentKeyCodes[1] = GameControl.instance.player1Script.keyCodes[6];
                currentKeyCodes[2] = GameControl.instance.player1Script.keyCodes[2];
                inputModule.verticalAxis = "";
            }
        }
        else
        {
            if (GameControl.instance.player2Script.controllerInput)
            {
                currentKeyCodes[0] = KeyCode.None;
                currentKeyCodes[1] = KeyCode.None;
                currentKeyCodes[2] = KeyCode.None;
                inputModule.verticalAxis = "Vertical" + GameControl.instance.player2Script.controllerIndex;
                currentKeyCodes[2] = GameControl.instance.player2Script.keyCodes[5];
            }
            else
            {
                currentKeyCodes[0] = GameControl.instance.player2Script.keyCodes[3];
                currentKeyCodes[1] = GameControl.instance.player2Script.keyCodes[6];
                currentKeyCodes[2] = GameControl.instance.player2Script.keyCodes[2];
                inputModule.verticalAxis = "";
            }
        }
    }

    public void SelectItem(int order)
    {
        RectTransform tempRect;

        if (lastSelectedOrder <= 4 && lastSelectedOrder >= 0)
        {
            if (ComparePrice(GetPinePriceWithInflation(pineItems[lastSelectedOrder].pineapplePrice), currentPlayerNo, false) && !boughtPineItem[lastSelectedOrder])
                buttonImages[lastSelectedOrder].color = Color.white;
            else
                buttonImages[lastSelectedOrder].color = unavailableColor;
        }
        else if (lastSelectedOrder > 4)
        {
            if (ComparePrice(shopItems[lastSelectedOrder - 5].gemPrice, currentPlayerNo, true))
                buttonImages[lastSelectedOrder].color = Color.white;
            else
                buttonImages[lastSelectedOrder].color = unavailableColor;
        }

        if (order < 0)
        {
            tierText.gameObject.SetActive(false);
            descTitleText.text = "EXIT SHOP";
            descText.text = "<color=#ffff00ff>-</color> <color=#00ff00ff>[Movement] </color> keys to<i> <color=#ffffffff>NAVIGATE</color></i>.\n" +
                "<color=#ffff00ff>-</color><color=#00ff00ff> [Fire]</color> to <i><color=#ffffffff>BUY</color></i>.\n" +
                "<color=#ffff00ff>-</color><color=#ffa500ff> [Escape]</color> to <i><color=#ffffffff>EXIT</color></i>.";
            selectionImage.sprite = selectedAvailableImage;

            tempRect = exitButtonObject.GetComponent<RectTransform>();
            selectionTransform.SetParent(shopPanelTransform);
            selectionTransform.anchorMin = tempRect.anchorMin;
            selectionTransform.anchorMax = tempRect.anchorMax;
            selectionTransform.anchoredPosition = tempRect.anchoredPosition;
            selectionTransform.sizeDelta = tempRect.sizeDelta;

            if (lastSelectedOrder <= 4 && lastSelectedOrder >= 0)
            {
                if (ComparePrice(GetPinePriceWithInflation(pineItems[lastSelectedOrder].pineapplePrice), currentPlayerNo, false) && !boughtPineItem[lastSelectedOrder])
                    buttonImages[lastSelectedOrder].color = Color.white;
                else
                    buttonImages[lastSelectedOrder].color = unavailableColor;
            }
            else if (lastSelectedOrder > 4)
            {
                if (ComparePrice(shopItems[lastSelectedOrder - 5].gemPrice, currentPlayerNo, true))
                    buttonImages[lastSelectedOrder].color = Color.white;
                else
                    buttonImages[lastSelectedOrder].color = unavailableColor;
            }
            lastSelected = exitButtonObject;
            lastSelectedOrder = order;
            currentSelectedOrder = order;
            return;
        }
        tempRect = itemButtons[order].GetComponent<RectTransform>();
        selectionTransform.SetParent(itemPanelTransform);
        selectionTransform.anchorMin = tempRect.anchorMin;
        selectionTransform.anchorMax = tempRect.anchorMax;
        selectionTransform.anchoredPosition = tempRect.anchoredPosition;
        selectionTransform.sizeDelta = tempRect.sizeDelta;
        exitButtonImage.color = Color.white;
        if (order <= 4)
        {
            if (ComparePrice(GetPinePriceWithInflation(pineItems[order].pineapplePrice), currentPlayerNo, false) && !boughtPineItem[order])
                currentItemAvailable = true;
            else
                currentItemAvailable = false;
            descTitleText.text = pineItems[order].name;
            descText.text = pineItems[order].description;

            if (currentItemAvailable)
                selectionImage.sprite = selectedAvailableImage;
            else
                selectionImage.sprite = selectedUnavailableImage;

            tierText.gameObject.SetActive(true);
            tierText.color = tierColors[pineItems[order].tier-1];
            tierText.text = "Tier <size=17>"+ pineItems[order].tier + "</size>";
        }
        else
        {
            currentItemAvailable = ComparePrice(shopItems[order-5].gemPrice, currentPlayerNo, true);
            descTitleText.text = shopItems[order-5].name;
            descText.text = shopItems[order-5].description;

            if (currentItemAvailable)
                selectionImage.sprite = selectedAvailableImage;
            else
                selectionImage.sprite = selectedUnavailableImage;

            tierText.gameObject.SetActive(true);
            tierText.color = tierColors[shopItems[order - 5].tier - 1];
            tierText.text = "Tier <size=17>" + shopItems[order - 5].tier + "</size>";
        }

        currentSelectedOrder = order;
        lastSelected = itemButtons[order].gameObject;
        lastSelectedOrder = order;
        //EventSystem.current.SetSelectedGameObject(null);
        //EventSystem.current.SetSelectedGameObject(itemButtons[order].gameObject);
    }

    private List<ShopItem> tempItems;
    public void ChoosePineItems()
    {
        for(int i = 0; i < 5; i++)
            boughtPineItem[i] = false;

        tempItems.Clear();
        tempItems.AddRange(shopItems.GetRange(12, shopItems.Count - 12));

        int itemCount = tempItems.Count;
        int maxPines = Mathf.Max(PickupManager.instance.player1.pineappleCount, PickupManager.instance.player2.pineappleCount);

        int nextSlot = 0;

        for(int i = 0; i < itemCount; i++)
        {
            int chosenRand = Random.Range(0, itemCount);
            itemCount--;
            if(tempItems[chosenRand].pineapplePrice > maxPines)
            {
                tempItems.RemoveAt(chosenRand);
                continue;
            }
            else
            {
                pineItems[nextSlot] = tempItems[chosenRand];
                tempItems.RemoveAt(chosenRand);

                nextSlot++;
                if(nextSlot >= 5)
                    break;
            }
        }

        int remainingSlots = 5 - nextSlot;
        if(remainingSlots > 0)
        {
            for(int i = 0; i < remainingSlots; i++)
            {
                pineItems[4 - i] = shopItems[Random.Range(0, shopItems.Count)];
            }
        }

        if(!GameControl.instance.shopOpenedOnce)
        {
            GameControl.instance.shopOpenedOnce = true;
            pineItems[0] = shopItems[3];
            pineItems[2] = shopItems[5];
            pineItems[4] = shopItems[31];
            pineItems[1] = shopItems[33];
            pineItems[3] = shopItems[12];
        }

        for(int i = 0; i < 5; i++)
        {
            itemButtons[i].transform.GetChild(0).GetComponent<Text>().text = "   " + pineItems[i].name;

            if(pineItems[i].pineapplePrice == 0)
                itemButtons[i].transform.GetChild(1).GetComponent<Text>().text = "FREE ";
            else
                itemButtons[i].transform.GetChild(1).GetComponent<Text>().text = pineItems[i].pineapplePrice + " ";
        }
    }

    private void UpdatePinePrices()
    {
        for(int i = 0; i < 5; i++)
        {
            if(boughtPineItem[i]) continue;

            int finalPinePrice = GetPinePriceWithInflation(pineItems[i].pineapplePrice);

            itemButtons[i].transform.GetChild(0).GetComponent<Text>().text = "   " + pineItems[i].name;

            if(finalPinePrice == 0)
                itemButtons[i].transform.GetChild(1).GetComponent<Text>().text = "FREE ";
            else
                itemButtons[i].transform.GetChild(1).GetComponent<Text>().text = finalPinePrice + " ";
        }
    }

    public void SwitchInputModuleToDefault()
    {
        inputModule.submitButton = "Submit";
        inputModule.horizontalAxis = "Horizontal";
        inputModule.verticalAxis = "Vertical";
        for (int i = 0; i < 3; i++)
            currentKeyCodes[i] = KeyCode.None;
    }

    public void ActivateShop(int player) // 1 or 2
    {
        totalPineItemBought = 0;
        shopActive = true;
        Cursor.visible = true;
        forP2Occured = false;
        SwitchInputModuleToDefault();
        UpdateShipStats();
        inputModule.submitButton = "";
        GameControl.instance.player1Script.EndWarp();
        GameControl.instance.player1Script.ResetWeaponStuff();
        GameControl.instance.player2Script.EndWarp();
        GameControl.instance.player2Script.ResetWeaponStuff();

        UpdatePinePrices();

        if (player == 1)
            inputModule.verticalAxis = "Vertical" + GameControl.instance.player1Script.controllerIndex;
        else
            inputModule.verticalAxis = "Vertical" + GameControl.instance.player2Script.controllerIndex;

        //PickupManager.instance.ppCam.SetActive(true);
        PickupManager.instance.peScript.enabled = false;
        PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .08f);
        GameControl.instance.player1Script.ExitSecretDrunk();
        GameControl.instance.player2Script.ExitSecretDrunk();
        GetKeyCodes(player);
        if (GameControl.instance.player1Script.playerAIControl.controlOn)
            return;
        
        if (player == 1)
        {
            if (GameControl.instance.p1c == 1)
            {
                //PickupManager.instance.ppCam.SetActive(false);
                PickupManager.instance.peScript.enabled = true;
                PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .4f);
                GameControl.instance.player1Script.EnterSecretDrunk();
                GameControl.instance.player2Script.EnterSecretDrunk();
                currentPlayerNo = 1;
                playerNoText.text = "PLAYER 1";
                gemNPineappleCountTexts[0].text = GameControl.instance.player1Script.gemCount + "   ";
                gemNPineappleCountTexts[1].text = GameControl.instance.player1Script.pineappleCount + "   ";
                shopObject.SetActive(true);
                LevelManager.instance.shopActive = true;
                EventSystem.current.SetSelectedGameObject(exitButtonObject);
                SelectItem(-1);
                CheckItemAvailabilities();
                GameControl.instance.paused = true; // just in case
                for (int i = 0; i < 4; i++)
                {
                    shopPanels[i].sprite = blueSprites[i];
                }
            }
            else
                ActivateShop(2);
        }
        else if(player == 2)
        {
            forP2Occured = true;
            if (GameControl.instance.p2c == 1)
            {
                for (int i = 0; i < 4; i++)
                {
                    shopPanels[i].sprite = redSprites[i];
                }
                //PickupManager.instance.ppCam.SetActive(true);
                PickupManager.instance.peScript.enabled = true;
                PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .4f);
                GameControl.instance.player1Script.EnterSecretDrunk();
                GameControl.instance.player2Script.EnterSecretDrunk();
                currentPlayerNo = 2;
                playerNoText.text = "PLAYER 2";
                gemNPineappleCountTexts[0].text = GameControl.instance.player2Script.gemCount + "   ";
                gemNPineappleCountTexts[1].text = GameControl.instance.player2Script.pineappleCount + "   ";
                shopObject.SetActive(true);
                LevelManager.instance.shopActive = true;
                EventSystem.current.SetSelectedGameObject(exitButtonObject);
                SelectItem(-1);
                CheckItemAvailabilities();
                if (!GameControl.instance.player2Script.controllerInput)
                    inputModule.verticalAxis = "";
                GameControl.instance.paused = true; // just in case
            }
            else
            {
                LevelManager.instance.shopActive = false;
                LevelManager.instance.tempTimer = 1f;
                //Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                SwitchInputModuleToDefault();
            }
        }

    }

    public void DisableShop()
    {
        currentSelectedOrder = -1;
        shopActive = false;
        SoundManager.instance.PlayFXSound(22, 1, 1, .5f);
        GameControl.instance.paused = false; // just in case
        shopObject.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);

        if (forP2Occured)
        {
            LevelManager.instance.shopActive = false;
            LevelManager.instance.tempTimer = 1f;
            //Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            SwitchInputModuleToDefault();
        }
        else
        {
            ActivateShop(2);
        }

        //PickupManager.instance.ppCam.SetActive(true);
        PickupManager.instance.peScript.enabled = false;
        PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .08f);
        GameControl.instance.player1Script.ExitSecretDrunk();
        GameControl.instance.player2Script.ExitSecretDrunk();
    }

    public void ResetSelection()
    {
        if(currentSelectedOrder < 0) return;
        EventSystem.current.SetSelectedGameObject(itemButtons[currentSelectedOrder].gameObject);
    }
}
