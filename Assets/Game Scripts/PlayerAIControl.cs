﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAIControl : MonoBehaviour {

    public bool controlOn = false;
    public float minLockDuration = 1;
    private float lockTimer = 0;
    private bool targetLocked = false;
    private Transform targetTransform;
    private float turnTimer;
    private bool turnTimerSet;

    [SerializeField]
    private PlayerShip ship;

    private bool[] inputBools;
    // 0 key right
    // 1 key left
    // 2 key fire
    // 3 key down accelerate
    // 4 key up accelerate
    // 5 key up fire

    private void Awake()
    {
        inputBools = new bool[6];
    }

    public void GetInputs(ref bool[] inputArray)
    {
        if (!controlOn)
            return;
        else
        {
            if(inputArray.Length > 6)
            {
                Debug.Log("Input array length is larger than 6!");
                return;
            }

            for (int i = 0; i < 6; i++)
            {
                inputArray[i] = inputBools[i];
            }
        }
    }

    private void Update()
    {
        if (!controlOn)
            return;
        if (IsLookingAtAlien())
            Shoot();
        else
            StopShooting();

        // dodge if projectile incoming

        // get away from aliens

        // scan n shoot for aliens
        if (!targetLocked)
            ScanForAliens();
        else
        {
            if ((ship.transform.position - targetTransform.position).magnitude > 15)
                Accelerate();
            else
                StopAcceleration();

            if ((ship.transform.position - targetTransform.position).magnitude < 8 && !turnTimerSet)
            {
                TurnToward(ship.transform.position - targetTransform.position);
            }
            else
            {
                if (!turnTimerSet)
                {
                    turnTimer = 1;
                    turnTimerSet = true;
                }
                TurnToward(targetTransform.position);
            }

            if ((ship.transform.position - targetTransform.position).magnitude < 3)
                Accelerate();

            lockTimer -= Time.deltaTime;
            turnTimer -= Time.deltaTime;
            if (lockTimer <= 0)
                targetLocked = false;
            if (turnTimer <= 0)
                turnTimerSet = true;

        }

        //if(targetTransform)
        //    Debug.DrawLine(ship.transform.position, targetTransform.position, Color.green);
    }
    
    private void Shoot() 
    {
        inputBools[2] = true;
    }

    private void StopShooting() 
    {
        if (inputBools[2])
        {
            inputBools[5] = true;
            inputBools[2] = false;
        }
        else
        {
            inputBools[5] = false;
        }
    }

    private bool IsLookingAtAlien()
    {
        bool raycast = Physics2D.CircleCast(ship.transform.position, .3f, ship.transform.up, 25, ship.alienMask);
        Debug.DrawLine(ship.transform.position, ship.transform.position + ship.transform.up * 25, Color.red);
        return raycast;
    }

    private void ScanForAliens()
    {
        RaycastHit2D hit2D = Physics2D.CircleCast(ship.transform.position, 30, ship.transform.up, .01f, ship.alienMask);
        inputBools[0] = false;
        inputBools[1] = false;

        if (hit2D)
        {
            if (!hit2D.collider.gameObject.activeInHierarchy)
                return;
            targetTransform = hit2D.transform;
            targetLocked = true;
            lockTimer = minLockDuration;
            turnTimerSet = false;
        }
    }

    private void TurnToward(Vector3 vector)
    {
        Vector2 distanceVector = ship.transform.position - vector;
        Quaternion targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVector.y, distanceVector.x) * Mathf.Rad2Deg + 180, Vector3.forward);
        
        if (Mathf.Abs(ship.transform.rotation.eulerAngles.z - targetRot.eulerAngles.z) < 5)
        {
            inputBools[0] = false;
            inputBools[1] = false;
            return;
        }
        
        if(Mathf.Abs((ship.transform.rotation.eulerAngles.z - targetRot.eulerAngles.z) % 360) <= 90)
        {
            inputBools[0] = true;
            inputBools[1] = false;
        }
        else
        {
            inputBools[0] = false;
            inputBools[1] = true;
        }
    }

    private void Accelerate()
    {
        inputBools[3] = true;
    }

    private void StopAcceleration()
    {
        if (inputBools[3])
        {
            inputBools[4] = true;
            inputBools[3] = false;
        }
        else
        {
            inputBools[4] = false;
        }
    }
}
