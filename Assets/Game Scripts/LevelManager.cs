﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEngine.UI;
using Steamworks;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public bool level0Ready = true;
    public bool level0Active = false;

    [Range(0, 100)]
    public int currentLevelNo;
    public int Looping = 0;
    public LevelData currentLevelData;
    [Header("Saving Data")]
    [Range(1, 100)]
    public int saveTo;
    public string saveAs;
    public int maxBullets;
    public int[] asteroidCounts = new int[2];
    public bool riftAppears = false;
    [Space(10)]
    [HideInInspector]
    public bool shopActive;
    
    public List<string> levelNames = new List<string>();

    public bool nextLevelReady = false;
    public float timeBetweenLevels = 3;
    public float tempTimer;
    [Space(20)]
    [SerializeField]
    private Text levelNoShowText;
    [SerializeField]
    private Text levelNameShowText;
    [SerializeField]
    private ParticleSystem warpEffect;
    [SerializeField]
    private Text warpWarning;
    [SerializeField]
    private ShopController shopControl;

    private ParticleSystem.ForceOverLifetimeModule warpForceModule;

    private float levelTitleTimer;
    public bool warping;
    private float warpTimer;
    private bool warpTurn;

    [SerializeField]
    private Vector2[] asteroidLocations = new Vector2[4];
    public GameObject backGround;
    public SpriteRenderer opaqueBackRenderer;
    public SpriteRenderer otherDimensionRenderer;
    [SerializeField]
    private Sprite[] backgroundSprites;
    [Space(10)]
    [Header("Rift Options")]
    [SerializeField]
    private float riftSpawnTime;
    [SerializeField]
    private Rift rift;
    private float levelTimer;
    private bool riftSpawned;

    [SerializeField]
    private GameObject demoPanelObject;
    [SerializeField]
    private GameObject warpChargeSound;
    [SerializeField]
    private AudioSource warpWindSound;
    [SerializeField]
    private AudioSource risingSound;

    [System.Serializable]
    public class TipElement
    {
        public int levelToShowAt;
        [TextArea]
        public string textToShow;
    }
    [Header("Tip Text Stuff")]
    [SerializeField]
    private TipElement[] tips;
    [SerializeField]
    private Text tipText;
        
    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);

        warpForceModule = warpEffect.forceOverLifetime;
        mainCamera = Camera.main;
        //Debug.Log(Input.GetJoystickNames().Length);
    }

    public void Start()
    {
        nextLevelReady = true;
        warpTimer = 0;
        warping = false;
        warpTurn = false;
        tempTimer = 7;
        warpWarning.gameObject.SetActive(false);
        warpForceModule.yMultiplier = 500;
        warpEffect.Stop();
        mainCamera.transform.position = Vector3.zero - Vector3.forward * 10;
        levelClearedObject.SetActive(false);
        Looping = 0;
        tipText.gameObject.SetActive(false);
    }

    private Camera mainCamera;
    private float bendTimer;
    GameObject tempExp;
    private void UpdateWarpWarning()
    {
        if(warpTurn && tempTimer <= 5)
        {
            if (!warping)
            {
                risingSound.pitch += Time.deltaTime * .55f;
                bendTimer -= Time.deltaTime;
                GameControl.instance.player1Script.ToggleWarpCharge(true);
                GameControl.instance.player2Script.ToggleWarpCharge(true);
                GameControl.instance.player1Script.PositionWarpCharge();
                GameControl.instance.player2Script.PositionWarpCharge();
                if (bendTimer <= 0)
                {
                    if (GameControl.instance.player1Object.activeInHierarchy)
                    {
                        tempExp = ExplosionPooler.instance.GetExplosion(69);
                        tempExp.transform.position = GameControl.instance.player1Object.transform.position + Vector3.up;
                        tempExp.SetActive(true);
                        //ExplosionPooler.instance.rippleEffect.SpawnDroplet(mainCamera.WorldToViewportPoint(GameControl.instance.player1Object.transform.position + Vector3.up));
                    }
                    if (GameControl.instance.player2Object.activeInHierarchy)
                    {
                        tempExp = ExplosionPooler.instance.GetExplosion(69);
                        tempExp.transform.position = GameControl.instance.player2Object.transform.position + Vector3.up;
                        tempExp.SetActive(true);
                        //ExplosionPooler.instance.rippleEffect.SpawnDroplet(mainCamera.WorldToViewportPoint(GameControl.instance.player2Object.transform.position + Vector3.up));
                    }
                    bendTimer = tempTimer / 2.5f;
                    risingSound.pitch = .25f;
                }
            }

            if(!warpWarning.gameObject.activeInHierarchy)
                warpChargeSound.gameObject.SetActive(true);
            warpWarning.gameObject.SetActive(true);
            int i = Mathf.RoundToInt(((tempTimer % 1) * 100));
            if (i < 10)
                warpWarning.text = "0" + (int)tempTimer + ":0" + i;
            else  if(i <100)
                warpWarning.text = "0" + (int)tempTimer + ":" + i;

            if(tempTimer < 0)
            {
                warpWarning.text = "Ẁ̧̡̕ÁŖ̶̷̧͢Ṕ̷̶ ̸̸̡͜҉Í́̕͡N̛T̡͘E̶͝R͘͘͘͢D̶͝͝Į̸́͝C̡͡T͏̡͘Ì̀O͏̶̢͘͢N̢̧̧";
            }

            if(warping)
            {
                warpWarning.gameObject.SetActive(false);
                warpChargeSound.gameObject.SetActive(false);
            }
        }
    }

    public int GetLevelNoToShow()
    {
        return currentLevelNo + Looping * 50;
    }

    private void ActivateRift()
    {
        if (EnemyManager.instance.empActive)
            EnemyManager.instance.empActive = false;
        if (!riftSpawned)
        {
            riftSpawned = true;
            rift.transform.position = Random.insideUnitCircle * Random.Range(0f, 8f);
            rift.SetSpawnIndex(25);
            rift.SetEnemyCount(GetLevelNoToShow());
            rift.gameObject.SetActive(true);
        }
    }

    private bool fightingBoss = false;
    private void ActivateLevel0()
    {
        level0Active = true;
        level0Ready = false;
        TutorialControl.instance.ActivateSkipTuto();
    }

    public void ToOtherDimension()
    {
        SoundManager.instance.PlayFXSound(4, 2, 1, 2);
        otherDimensionRenderer.maskInteraction = SpriteMaskInteraction.None;
        otherDimensionRenderer.transform.position = Random.insideUnitCircle * 15;
        otherDimensionRenderer.transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward);
        //otherDimensionRenderer.sortingLayerName = "Player Ship";
        //otherDimensionRenderer.sortingOrder = -10;
        if(GameControl.instance.bossAlive && currentLevelNo < 46)
        {
            GameObject boss = EnemyManager.instance.GetAlien(33);
            boss.SetActive(true);
        }

        GameObject tempExplosion = ExplosionPooler.instance.GetExplosion(65);
        tempExplosion.transform.position = Vector3.zero;
        tempExplosion.SetActive(true);
    }

    public void BackToNormal()
    {
        otherDimensionRenderer.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        otherDimensionRenderer.sortingLayerName = "Background";
        otherDimensionRenderer.sortingOrder = -45;
    }

    private void Update()
    {
        if (GameControl.instance.gameOn && !GameControl.instance.paused)
        {
            //if (level0Active)
            //    return;
            //if (level0Ready && !level0Active)
            //    ActivateLevel0();

            if (levelTimer >= riftSpawnTime && currentLevelData.riftAppears)
                ActivateRift();

            levelTimer += Time.deltaTime;
            UpdateWarpWarning();
            if (warpTimer < 2.5f && warping)
            {
                warpForceModule.yMultiplier -= 10f;
            }
            else if (warpTimer > 2.5f && warping)
            {
                warpForceModule.yMultiplier += 5;
            }

            if (levelTitleTimer < 0)
                DisableLevelTitle();
            else
                levelTitleTimer -= Time.deltaTime;

            if (currentLevelNo < levelNames.Count)
            {
                warpTimer -= Time.deltaTime;
                if (!warping && EnemyManager.instance.enemyCount == 0)
                    tempTimer -= Time.deltaTime;
                if (warpTimer < 0f && warping)
                {
                    warpWindSound.Stop();
                    SoundManager.instance.PlayFXSound(4, 2, 1, 2);
                    warping = false;
                    warpEffect.Stop();
                    BackToNormal();
                    if (GameControl.instance.player1Object.activeInHierarchy)
                        ExplosionPooler.instance.rippleEffect.SpawnDroplet(mainCamera.WorldToViewportPoint(GameControl.instance.player1Object.transform.position + Vector3.up));
                    if (GameControl.instance.player2Object.activeInHierarchy)
                        ExplosionPooler.instance.rippleEffect.SpawnDroplet(mainCamera.WorldToViewportPoint(GameControl.instance.player2Object.transform.position + Vector3.up));
                    backGround.SetActive(true);
                    tempTimer = timeBetweenLevels;
                    GameControl.instance.player1Script.ExitSecretDrunk();
                    GameControl.instance.player2Script.ExitSecretDrunk();
                    shopControl.ActivateShop(1);

                    warpTurn = false;
                    mainCamera.transform.position = Vector3.zero - Vector3.forward * 10;
                    warpForceModule.yMultiplier = 500;
                }

                if (nextLevelReady && tempTimer < -.1f && warpTimer < 0f && !shopActive)
                {
                    warpWarning.gameObject.SetActive(false);
                    LoadNextLevel();
                    if (currentLevelData.riftAppears)
                        EnemyManager.instance.enemyCount++;
                    levelTimer = 0;
                    riftSpawned = false;
                    nextLevelReady = false;
                }
                else
                {
                    if (EnemyManager.instance.enemyCount == 0 && (EnemyManager.instance.playerObject1.activeInHierarchy || EnemyManager.instance.playerObject2.activeInHierarchy) && !nextLevelReady)
                    {
                        switch (currentLevelNo)
                        {
                            case 21:
                                fightingBoss = true; break;
                            case 24:
                                fightingBoss = false; break;
                            case 46:
                                fightingBoss = true; break;
                            case 49:
                                fightingBoss = false; break;
                            //case 71:
                            //    fightingBoss = true; break;
                            //case 74:
                            //    fightingBoss = false; break;
                            //case 96:
                            //    fightingBoss = true; break;
                            //case 99:
                            //    fightingBoss = false; break;
                            default:
                                MusicManager.instance.ChangeMusic(MusicManager.Music.Combat);
                                break;
                        }
                        if (!warpTurn)
                        {
                            if(!fightingBoss)
                                StartCoroutine(LevelClearedAnimation());
                            if (currentLevelNo % 5 == 0)
                                tempTimer = timeBetweenLevels + 5;
                            else
                                tempTimer = timeBetweenLevels;
                        }
                        if (currentLevelNo % 5 == 0)
                        {
                            warpTurn = true;
                            //PickupManager.instance.player1.EnterPhaseFor(5);
                            //PickupManager.instance.player2.EnterPhaseFor(5);
                            if (tempTimer >= 0f)
                                return;
                            warping = true;
                            if(((riftSpawned && currentLevelData.riftAppears) || !currentLevelData.riftAppears)/* || currentLevelNo % 25 == 0*/)
                            {
                                ActivateWarpFor(5);
                                if(currentLevelNo == 50) // TODO REMOVE THIS FOR THE FULL GAME (ONLY FOR DEMO)
                                {
                                    //warpChargeSound.gameObject.SetActive(false);
                                    //demoPanelObject.SetActive(false);
                                    //demoPanelObject.SetActive(true);
                                    //GameControl.instance.EndGame();
                                    //GameControl.instance.ActivateGameOverMenu(1.75f);
                                    //warpWindSound.Stop();
                                    currentLevelNo = 0;
                                    Looping++;
                                }
                            }
                            warpWarning.gameObject.SetActive(false);
                        }
                        nextLevelReady = true;
                        currentLevelNo++;
                    }
                }
            }
            else
            {
                currentLevelNo = 1;
            }
        }
    }

    [Header("Level Cleared Stuff")]
    [SerializeField]
    private GameObject levelClearedObject;
    private IEnumerator LevelClearedAnimation()
    {
        tipText.gameObject.SetActive(false);
        levelClearedObject.SetActive(false);
        levelClearedObject.SetActive(true);
        yield return new WaitForSeconds(.25f);

        EnemyManager.instance.BlowUpAsteroids();
        GameControl.instance.SlowTime(.5f, .8f);
        ClearLevelExplosion();
        CheckForTips();

        yield return new WaitForSeconds(.5f);
        //GameControl.instance.player1Script.PullTowardCenter();
        //GameControl.instance.player2Script.PullTowardCenter();
        yield return new WaitForEndOfFrame();
    }

    [HideInInspector]
    public bool addedElites;
    private void CheckForTips()
    {
        if(addedElites)
        {
            ActivateTip(tips[2].textToShow);
            addedElites = false;
            return;
        }
        foreach(TipElement tip in tips)
        {
            if((GetLevelNoToShow()) == tip.levelToShowAt)
            {
                ActivateTip(tip.textToShow);
                break;
            }
        }
    }

    private void ActivateTip(string s)
    {
        tipText.text = s;
        tipText.gameObject.SetActive(false);
        tipText.gameObject.SetActive(true);
    }

    private void ClearLevelExplosion()
    {
        SoundManager.instance.PlayFXSound(4, .8f, 1, 5);
        GameObject e = ExplosionPooler.instance.GetExplosion(71);
        e.transform.position = Vector3.zero;
        e.SetActive(true);
        ExplosionPooler.instance.rippleEffect.SpawnDroplet(mainCamera.WorldToViewportPoint(Vector3.zero));
    }

    [Space(10)]
    [SerializeField]
    private List<Path> currentPaths;
    [SerializeField]
    private List<Alien> currentAliens;
    [SerializeField]
    private List<SpecialAlien> currentSpecialAliens;

    [Space(20)]
    public GameObject pathPrefab;
    public GameObject pathBeginPrefab;
    public GameObject pathNodePrefab;
    public GameObject pathEndPrefab;
    public GameObject endPositionsObjectPrefab;

    //#region TODO Comment out this part when building 
    //[ContextMenu("Save Current Level")]
    //public void SaveLevelData()
    //{
    //    currentAliens.Clear();
    //    currentPaths.Clear();
    //    currentSpecialAliens.Clear();

    //    currentPaths.AddRange(FindObjectsOfType<Path>());
    //    currentAliens.AddRange(FindObjectsOfType<Alien>());
    //    currentSpecialAliens.AddRange(FindObjectsOfType<SpecialAlien>() as SpecialAlien[]);

    //    LevelData newData = ScriptableObject.CreateInstance(typeof(LevelData)) as LevelData; // Save this to a folder
    //    newData.Initialize(currentPaths, currentAliens, currentSpecialAliens);
    //    newData.levelNo = saveTo;
    //    newData.name = saveAs;
    //    newData.maxProjectiles = maxBullets;
    //    newData.bigAsteroidCount = asteroidCounts[1];
    //    newData.smallAsteroidCount = asteroidCounts[0];
    //    newData.riftAppears = riftAppears;

    //    string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/Resources/Levels/" + saveTo.ToString() + "_" + saveAs + ".asset");
    //    AssetDatabase.CreateAsset(newData, assetPathAndName);
    //    AssetDatabase.SaveAssets();

    //    levelNames.Add("new level name");
    //    levelNames[saveTo - 1] = saveAs;

    //    currentLevelData = Resources.Load("Levels/" + currentLevelNo + "_" + levelNames[currentLevelNo - 1]) as LevelData;
    //}
    //#endregion

    public void GenerateLevel(LevelData level)
    {
        List<GameObject> pathObjects = new List<GameObject>();

        for (int i = 0; i < level.pathElements.Count; i++)
        {
            GameObject path = Instantiate(pathPrefab, transform);
            pathObjects.Add(path);

            for (int k = 0; k < level.pathElements[i].positions.Length; k++)
            {
                GameObject pathNodeObj;

                if (k == 0)
                    pathNodeObj = Instantiate(pathBeginPrefab, level.pathElements[i].positions[k], Quaternion.identity, pathObjects[i].transform);
                else if (k == level.pathElements[i].positions.Length - 1)
                    pathNodeObj = Instantiate(pathEndPrefab, level.pathElements[i].positions[k], Quaternion.identity, pathObjects[i].transform);
                else
                    pathNodeObj = Instantiate(pathNodePrefab, level.pathElements[i].positions[k], Quaternion.identity, pathObjects[i].transform);

                PathNode nodeScript = pathNodeObj.GetComponent<PathNode>();
                nodeScript.speedMultiplier = level.pathElements[i].speeds[k];
                nodeScript.resetVelocity = level.pathElements[i].resetSpeed[k];
                nodeScript.teleportToNext = level.pathElements[i].teleportToNext[k];
                nodeScript.destroyAlien = level.pathElements[i].destroyAlien[k];
                nodeScript.velocityAfterArrival = level.pathElements[i].velsAfterArrival[k];
                nodeScript.col.radius = level.pathElements[i].radiiOfCols[k];
            }

            Path pathScript = pathObjects[i].GetComponent<Path>();
            pathScript.GetPath();
            if (level.pathElements[i].loop)
                pathScript.loop = true;
            else
                pathScript.loop = false;

            GameObject endPositionsObject = Instantiate(endPositionsObjectPrefab, transform);
            EndPositions endPosScript = endPositionsObject.GetComponent<EndPositions>();

            foreach (Vector2 position in level.pathElements[i].endPositions)
            {
                endPosScript.endPositions.Add(position);
            }

            pathScript.endPosScript = endPosScript;
        }

        // mixed aliens
        if(Looping > 1)
        {
            for(int i = 0; i < level.enemyPositionElements.Count; i++)
            {
                GameObject newAlien = EnemyManager.instance.GetRandomT2Alien();
                newAlien.SetActive(false);
                newAlien.transform.position = level.enemyPositionElements[i].position;
                Alien alienScript = newAlien.GetComponent<Alien>();
                EnemyManager.instance.AddAlien(alienScript.hull);
                if(level.enemyPositionElements[i].pathIndex >= 0)
                    alienScript.path = pathObjects[level.enemyPositionElements[i].pathIndex].GetComponent<Path>();
                else
                    alienScript.path = null;
                newAlien.SetActive(true);
            }
        }
        else // normal
        {
            for(int i = 0; i < level.enemyPositionElements.Count; i++)
            {
                GameObject newAlien = EnemyManager.instance.GetAlien(level.enemyPositionElements[i].poolIndex);
                newAlien.SetActive(false);
                newAlien.transform.position = level.enemyPositionElements[i].position;
                Alien alienScript = newAlien.GetComponent<Alien>();
                EnemyManager.instance.AddAlien(alienScript.hull);
                if(level.enemyPositionElements[i].pathIndex >= 0)
                    alienScript.path = pathObjects[level.enemyPositionElements[i].pathIndex].GetComponent<Path>();
                else
                    alienScript.path = null;
                newAlien.SetActive(true);
            }
        }

        for (int i = 0; i < level.specialAlienElements.Count; i++)
        {
            GameObject newSpecialAlien = EnemyManager.instance.GetAlien(level.specialAlienElements[i].poolIndex);
            SpecialAlien alienScript = newSpecialAlien.GetComponent<SpecialAlien>();
            EnemyManager.instance.AddAlien(alienScript.hull);
            newSpecialAlien.transform.position = level.specialAlienElements[i].position;
            newSpecialAlien.SetActive(true);

            for(int a = 0; a < Looping; a++)
            {
                newSpecialAlien = EnemyManager.instance.GetRandomSpecialAlien();
                alienScript = newSpecialAlien.GetComponent<SpecialAlien>();
                EnemyManager.instance.AddAlien(alienScript.hull);
                newSpecialAlien.transform.position = Random.insideUnitCircle.normalized * 30;
                newSpecialAlien.SetActive(true);
            }
        }

        // boss at lvl 1
        if(Looping > 1 && currentLevelNo == 1)
        {
            GameObject newSpecialAlien = EnemyManager.instance.GetAlien(16);
            SpecialAlien alienScript = newSpecialAlien.GetComponent<SpecialAlien>();
            EnemyManager.instance.AddAlien(alienScript.hull);
            newSpecialAlien.transform.position = Random.insideUnitCircle.normalized * 50;
            newSpecialAlien.SetActive(true);
        }

        // elites
        for(int i = 0; i < Looping + GameControl.instance.bossKillCount * 2; i++)
        {
            GameObject newSpecialAlien = EnemyManager.instance.GetRandomSpecialAlien();
            SpecialAlien alienScript = newSpecialAlien.GetComponent<SpecialAlien>();
            EnemyManager.instance.AddAlien(alienScript.hull);
            newSpecialAlien.transform.position = Random.insideUnitCircle.normalized * 30;
            newSpecialAlien.SetActive(true);
        }
    }

    public void LoadNextLevel()
    {
        int a = transform.childCount;

        for (int i = 0; i < a; i++)
            Destroy(transform.GetChild(i).gameObject);

        currentLevelData = Resources.Load("Levels/" + currentLevelNo + "_" + levelNames[currentLevelNo - 1]) as LevelData;
        GenerateLevel(currentLevelData);
        EnemyManager.instance.maxProjectileCount = currentLevelData.maxProjectiles * (1 + Looping);
        PickupManager.instance.canSpawnPickups = true;
        PickupManager.instance.spawnedPickupCount = 0;
        GameObject asteroid;

        for (int i = 0; i < currentLevelData.bigAsteroidCount; i++)
        {
            asteroid = EnemyManager.instance.GetAlien(Random.Range(7,10));
            asteroid.transform.position = asteroidLocations[Random.Range(0, asteroidLocations.Length)];
            EnemyManager.instance.spawnedAsteroids.Add(asteroid.GetComponent<Asteroid>());
            asteroid.SetActive(true);
        }

        for (int i = 0; i < currentLevelData.smallAsteroidCount; i++)
        {
            asteroid = EnemyManager.instance.GetAlien(Random.Range(10, 13));
            asteroid.transform.position = asteroidLocations[Random.Range(0, asteroidLocations.Length)];
            EnemyManager.instance.spawnedAsteroids.Add(asteroid.GetComponent<Asteroid>());
            asteroid.SetActive(true);
        }

        if(SteamManager.Initialized)
        {
            SteamFriends.SetRichPresence("score", GameControl.instance.GetTotalScore().ToString());
            SteamFriends.SetRichPresence("level", GetLevelNoToShow().ToString());
            SteamFriends.SetRichPresence("steam_display", "#StatusWithScore");
        }

        ActivateLevelTitleFor(2.5f);
    }

    private void ActivateLevelTitleFor(float seconds)
    {
        levelTitleTimer = seconds;
        levelNameShowText.gameObject.SetActive(false);
        levelNoShowText.gameObject.SetActive(false);
        levelNameShowText.gameObject.SetActive(true);
        levelNoShowText.gameObject.SetActive(true);
        levelNoShowText.text = "GET READY\nLEVEL " + GetLevelNoToShow();

        levelNameShowText.text = levelNames[currentLevelNo - 1];

        if(Looping > 0)
        {
            if(currentLevelNo == 1)
            {
                ActivateTip(tips[2].textToShow);
            }
            //exceptions
            if(GameControl.instance.bossAlive && currentLevelNo == 46)
                levelNameShowText.text = "FULLY REPAIRED";
            else if(GameControl.instance.bossAlive && currentLevelNo == 1)
            {
                levelNameShowText.text = "YOU MISSED IT";
            }
            else if(!GameControl.instance.bossAlive && currentLevelNo == 1)
            {
                levelNameShowText.text = "NEW GAME+";
            }

            if(currentLevelNo == 1 && Looping >= 2)
            {
                levelNameShowText.text = "LETS START TO MIX IT UP A BIT";
                SteamManager.Instance.UnlockAchievement("LEVEL_101");
            }
        }
    }

    public void DisableLevelTitle()
    {
        levelNoShowText.text = "";
        levelNameShowText.text = "";
        levelNameShowText.gameObject.SetActive(false);
        levelNoShowText.gameObject.SetActive(false);
    }

    public void ActivateWarpFor(float time)
    {
        warpWindSound.Play();
        SoundManager.instance.PlayFXSound(4, 2, 1, 2, 1);
        SoundManager.instance.PlayFXSound(8, 1, 1, 2, 2);
        warpTimer = time;
        warpEffect.Play();
        tempTimer = 1;
        EnemyManager.instance.KillAll();
        ProjectilePooler.instance.ClearProjectiles();
        PickupManager.instance.ClearPickups();
        //PickupManager.instance.ppCam.SetActive(false);
        //PickupManager.instance.peScript.enabled = true;
        //PickupManager.instance.bloomInhibImage.color = new Color(0, 0, 0, .4f);
        backGround.SetActive(false);
        ChangeBackground();
        shopControl.ChoosePineItems();
    }
    
    public void ClearEnemies()
    {
        EnemyManager.instance.KillAll();
        ProjectilePooler.instance.ClearProjectiles();
        PickupManager.instance.ClearPickups();
    }

    public void CleanUpLevel()
    {
        tempTimer = 1;
        EnemyManager.instance.KillAll();
        ProjectilePooler.instance.ClearProjectiles();
        PickupManager.instance.ClearPickups();
        //TutorialControl.instance.ResetTuto();
        level0Ready = true;
        LevelManager.instance.DisableLevelTitle();
        warpWarning.gameObject.SetActive(false);
        levelClearedObject.SetActive(false);
        Start();
        backGround.SetActive(true);
        ChangeBackground();
        rift.SetLifeTime(.15f);
        rift.SetEnemyCount(0);

        int a = transform.childCount;
        for (int i = 0; i < a; i++)
            Destroy(transform.GetChild(i).gameObject);
    }

    private void ChangeBackground()
    {
        opaqueBackRenderer.sprite = backgroundSprites[Random.Range(0, backgroundSprites.Length)];
        opaqueBackRenderer.color = new Color(Random.Range(.25f, .3f), Random.Range(.25f, .3f), Random.Range(.25f, .3f), 1);
        opaqueBackRenderer.transform.rotation = Quaternion.AngleAxis(Random.Range(0, 360), Vector3.forward);
    }
}