﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    static class Extras
    {
        public static bool CompareVectors(Vector2 vectorA, Vector2 vectorB, float tolerance)
        {
            if (Mathf.Abs(vectorB.x - vectorA.x) < tolerance && Mathf.Abs(vectorB.y - vectorA.y) < tolerance)
                return true;
            else
                return false;
        }


        public static float Remap(float f, float old1, float old2, float new1, float new2, bool clamp = false)
        {
            if(clamp)
            {
                if(old1 > old2)
                    SwapFloat(ref old1, ref old2);

                Mathf.Clamp(f, old1, old2);
            }

            return new1 + (new2 - new1) * (f - old1) / (old2 - old1);
        }

        public static void SwapFloat(ref float a, ref float b)
        {
            float temp = a;
            a = b;
            b = temp;
        }
    }
}
