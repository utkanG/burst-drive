﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour
{
    public static MusicManager instance;

    public AudioMixer mainMixer;

    private AudioSource source;

    public List<AudioClip> musicClips = new List<AudioClip>();

    public enum Music { Menu, Combat }
    public Music currentM;

    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);
    
        source = GetComponent<AudioSource>();
        currentM = Music.Menu;
    }

    public void ChangeMusic(Music m)
    {
        if(currentM == m) return;

        source.clip = musicClips[(int) m];
        source.Play();
        currentM = m;
    }

    public void SetVolume(float vol)
    {
        source.volume = vol;
    }
}
