﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleBeam : MonoBehaviour {

    private List<Hull> hullsToDamage = new List<Hull>();
    private List<GameObject> particleBeamHits = new List<GameObject>();

    public int playerIndex;

    private void OnTriggerEnter2D(Collider2D col)
    {
        Hull h = col.GetComponent<Hull>();
        particleBeamHits.Add(ExplosionPooler.instance.GetExplosion(28));
        hullsToDamage.Add(h);
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        Hull h = col.GetComponent<Hull>();

        if (hullsToDamage.Contains(h))
        {
            int i = hullsToDamage.IndexOf(h);
            particleBeamHits[i].SetActive(false);
            particleBeamHits.RemoveAt(i);
            hullsToDamage.RemoveAt(i);
        }
    }

    private void Update()
    {
        for (int i = 0; i < hullsToDamage.Count; i++)
        {
            if (!hullsToDamage[i].invulnerable)
            {
                hullsToDamage[i].Damage(1, playerIndex); // TODO source
                if (!hullsToDamage[i].blewUp)
                {
                    particleBeamHits[i].transform.position = hullsToDamage[i].transform.position;
                    particleBeamHits[i].SetActive(true);
                }
                else
                    particleBeamHits[i].SetActive(false);
            }
        }

        if (hullsToDamage.Count != 0 && EnemyManager.instance.enemyCount == 0)
            hullsToDamage.Clear();
    }

    private void OnDisable()
    {
        hullsToDamage.Clear();
        foreach (GameObject particleHit in particleBeamHits)
            particleHit.SetActive(false);
        particleBeamHits.Clear();
    }
}
