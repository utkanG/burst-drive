﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Projectile", menuName = "Projectile", order = 2)]
public class ScriptableProjectile : ScriptableObject {
    
    public float speed = 15;
    public int damage = 1;
    public float lifeTime = 2;
    public int poolIndex = 0;
    public int explosionPoolIndex = 0;
    public bool isEnemy = false;
}
