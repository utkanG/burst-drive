﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBossCannon : Projectile {

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
            {
                return;
            }
            h.Damage(projectileData.damage, playerIndex); 
        }
    }

    protected override void Explode()
    {
        gameObject.SetActive(false);
        GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
        explosion.transform.position = transform.position;
        explosion.transform.rotation = transform.rotation;
        explosion.SetActive(true);
        ProjectilePooler.instance.ReturnProjectileToPool(projectileData.poolIndex, gameObject);
    }
}
