﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePieceProjectile : Projectile {

    protected override void OnEnable()
    {
        base.OnEnable();
        rb2d.velocity = Random.Range(projectileData.speed, projectileData.speed * 4) * transform.up;
        currentLife += Random.Range(0, .2f);
    }

    protected override void Update()
    {
        base.Update();
        rb2d.velocity += Random.insideUnitCircle * 7;
    }
}
