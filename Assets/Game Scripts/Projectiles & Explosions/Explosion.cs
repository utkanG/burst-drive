﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    // scriptable(?)
    public float lifeTime = 1f;
    public int poolIndex = 0;

    private float currentLife;

    private void OnEnable()
    {
        currentLife = lifeTime;
    }

    private void Update()
    {
        currentLife -= Time.deltaTime;
        if (currentLife <= 0)
        {
            ExplosionPooler.instance.ReturnExplosionToPool(poolIndex, gameObject);
            gameObject.SetActive(false);
        }
    }
}
