﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeminiProjectile : Projectile {

    public bool onLeft;

    private float tempTime = 0;

    protected override void OnEnable()
    {
        base.OnEnable();
        tempTime = 0;
    }

    protected override void Update()
    {
        base.Update();
        tempTime += Time.deltaTime;
        if(!GameControl.instance.paused)
            PingPong();
    }

    private void PingPong()
    {
        if (onLeft)
        {
            rb2d.velocity += (Vector2)transform.right * Time.deltaTime * 45;
        }
        else
        {
            rb2d.velocity -= (Vector2)transform.right * Time.deltaTime * 45;
        }

        if (tempTime >= .25f)
        {
            tempTime = 0;
            onLeft = !onLeft;
            rb2d.velocity = projectileData.speed * transform.up;
        }
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            dealtDmg = true;
            currentLife = 0;
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
            {
                currentLife = 0;
                return;
            }

            if (h.hitByGemini)
            {
                h.Damage(2 , playerIndex); 
                GameObject e = ExplosionPooler.instance.GetExplosion(5);
                SoundManager.instance.PlayFXSound(11, Random.Range(2f, 3f), .4f, .25f);
                e.transform.position = h.transform.position;
                e.SetActive(true);
            }
            else
                h.Damage(projectileData.damage, playerIndex); 

            h.GeminiHit();
        }
    }
}
