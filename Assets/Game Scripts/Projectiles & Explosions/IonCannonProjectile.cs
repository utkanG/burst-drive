﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IonCannonProjectile : Projectile {

    public int projectileCount = 3;

    public bool aoe = false;
    public LayerMask aoeMask;

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
            {
                currentLife = 0;
                return;
            }

            h.Damage(projectileData.damage, playerIndex); // TODO source
            dealtDmg = true;
            currentLife = 0;

            for (int i = 0; i < projectileCount; i++)
            {
                GameObject tempProjectile;
                tempProjectile = ProjectilePooler.instance.GetProjectile(4);
                tempProjectile.transform.rotation = Quaternion.Euler(0,0,transform.rotation.eulerAngles.z + (i-1)*90);
                switch (i-1)
                {
                    case -1:
                        tempProjectile.transform.position = transform.position + transform.right * 2f;
                        break;
                    case 0:
                        tempProjectile.transform.position = transform.position + transform.up * 2f;
                        break;
                    case 1:
                        tempProjectile.transform.position = transform.position - transform.right * 2f; 
                        break;
                    default:
                        break;
                }
                tempProjectile.tag = tag;
                tempProjectile.SetActive(true);
            }

            if(aoe)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 1f, aoeMask);
                foreach(Collider2D collider in colliders)
                {
                    collider.GetComponent<Hull>().Damage(5, 0);
                }
            }
        }
    }
}
