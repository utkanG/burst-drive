﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pomegranate : Projectile {

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        base.OnTriggerEnter2D(col);
        for (int i = 0; i < 10; i++)
        {
            GameObject miniProjectile = ProjectilePooler.instance.GetProjectile(30);
            miniProjectile.transform.rotation = Quaternion.AngleAxis(36 * i + transform.rotation.z, Vector3.forward);
            miniProjectile.transform.position = miniProjectile.transform.up * .7f + transform.position;
            miniProjectile.SetActive(true);
        }
    }

    protected override void Explode()
    {
        base.Explode();
    }
}
