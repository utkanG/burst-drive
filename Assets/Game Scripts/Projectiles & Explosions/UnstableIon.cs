﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnstableIon : Projectile {

    private bool alreadyHit;

    protected override void OnEnable()
    {
        base.OnEnable();
        alreadyHit = false;
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable || alreadyHit)
            {
                dealtDmg = true;
                currentLife = 0;
                return;
            }
            else
            {
                GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
                explosion.transform.position = transform.position;
                explosion.transform.rotation = transform.rotation;
                explosion.SetActive(true);
                transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 360));
                rb2d.velocity = transform.up * projectileData.speed;
                alreadyHit = true;
            }
            h.Damage(projectileData.damage, playerIndex); // TODO source
        }
    }
}
