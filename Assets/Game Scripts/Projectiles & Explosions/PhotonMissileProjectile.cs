﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonMissileProjectile : Projectile {

    GameObject target;
    private Collider2D collider;

    private bool acquiredTarget = false;
    [SerializeField]
    private LayerMask alienMask;

    protected override void Awake()
    {
        base.Awake();
        BackToSensing();
        collider = GetComponent<Collider2D>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        BackToSensing();
        collider.enabled = true;
    }

    protected override void Update()
    {
        base.Update();
        if (!GameControl.instance.paused)
        {
            if (!acquiredTarget)
            {
                LookForTarget();
                rb2d.velocity += (Vector2)transform.right * Random.Range(-2, 2f) + (Vector2)transform.up / 2;
            }
            else if(!dealtDmg)
            {
                Vector2 direction = (target.transform.position - transform.position).normalized;
                rb2d.velocity += direction * 2.5f;
                if (!target.activeInHierarchy)
                    BackToSensing();
            }
            Quaternion targetRot = Quaternion.AngleAxis(Mathf.Atan2(rb2d.velocity.y, rb2d.velocity.x) * Mathf.Rad2Deg - 90, Vector3.forward);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, .3f);
        }
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!acquiredTarget)
            AcquireTarget(col.gameObject);
        else
        {
            if (!dealtDmg)
            {
                Hull h = col.GetComponent<Hull>();
                if (h.invulnerable)
                {
                    currentLife = 0f;
                    return;
                }
                currentLife = 0f;
                h.Damage(projectileData.damage, playerIndex);
            }
        }
    }

    private void AcquireTarget(GameObject obj)
    {
        target = obj;
        acquiredTarget = true;
    }

    private void BackToSensing()
    {
        acquiredTarget = false;
        target = null;
    }

    protected override void Explode()
    {
        if (dealtDmg)
        {
            Remove();
            return;
        }
        collider.enabled = false;
        rb2d.velocity = Vector2.zero;
        currentLife = .2f;
        GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
        explosion.transform.position = transform.position;
        explosion.transform.rotation = transform.rotation;
        explosion.SetActive(true);
        dealtDmg = true;
    }

    private void LookForTarget()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 4, transform.up, 3, alienMask);
        if(hit)
            AcquireTarget(hit.collider.gameObject);
    }

    private void Remove()
    {
        gameObject.SetActive(false);
        ProjectilePooler.instance.ReturnProjectileToPool(projectileData.poolIndex, gameObject);
    }
}
