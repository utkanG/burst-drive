﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gammarang : Projectile {

    private float timer;
    private Vector2 direction;
    private bool directionChanged;
    private int hitsRemaining;

    protected override void OnEnable()
    {
        base.OnEnable();
        transform.localScale = Vector3.one;
        timer = .25f;
        hitsRemaining = 5;
        direction = transform.up;
        directionChanged = false;
    }

    protected override void Update()
    {
        base.Update();
        timer -= Time.deltaTime;
        transform.localScale += Vector3.one * Time.deltaTime / 2f;

        if (timer <= 0 && !directionChanged)
        {
            direction = -direction;
            directionChanged = true;
        }

        rb2d.velocity += direction * projectileData.speed * Time.deltaTime;
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
            {
                hitsRemaining--;
                return;
            }
            hitsRemaining--;
            if(!directionChanged)
                h.Damage(projectileData.damage, playerIndex);
            else
                h.Damage(projectileData.damage + 3, playerIndex);
            GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
            explosion.transform.position = transform.position;
            explosion.transform.rotation = transform.rotation;
            explosion.SetActive(true);
            if (hitsRemaining <= 0)
                currentLife = 0;
        }
    }
}
