﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HullBuster : Projectile {

    GameObject target;

    private bool acquiredTarget = false;
    [SerializeField]
    private LayerMask alienMask;
    private int hits = 4;

    protected override void Awake()
    {
        base.Awake();
        BackToSensing();
        rb2d.drag = 3;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        BackToSensing();
        hits = 4;
    }

    protected override void Update()
    {
        base.Update();
        if (!GameControl.instance.paused)
        {
            if (!acquiredTarget)
            {
                LookForTarget();
            }
            else
            {
                Vector2 direction = (Vector2)(target.transform.position - transform.position).normalized;
                rb2d.velocity += direction * 2.5f;
                if (!target.activeInHierarchy)
                    BackToSensing();
            }
            Quaternion targetRot = Quaternion.AngleAxis(Mathf.Atan2(rb2d.velocity.y, rb2d.velocity.x) * Mathf.Rad2Deg - 90, Vector3.forward);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, .3f);
        }
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!acquiredTarget)
            AcquireTarget(col.gameObject);
        else
        {
            SoundManager.instance.PlayFXSound(25, 1.5f / (1 + hits * .1f), .4f, .9f, 130);
            rb2d.velocity = transform.up * projectileData.speed + transform.right * Random.Range(-20, 20);
            hits--;
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
                return;
            h.Damage(projectileData.damage, playerIndex);
            dealtDmg = true;
            GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
            explosion.transform.position = transform.position;
            explosion.transform.rotation = transform.rotation;
            explosion.SetActive(true);
            if (hits <= 0)
                currentLife = 0;
        }
    }

    private void AcquireTarget(GameObject obj)
    {
        target = obj;
        acquiredTarget = true;
    }

    private void BackToSensing()
    {
        acquiredTarget = false;
        target = null;
    }

    private void LookForTarget()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 8, transform.up, 3, alienMask);
        if (hit)
            AcquireTarget(hit.collider.gameObject);
    }
}
