﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcSphereProjectile : Projectile
{
    public int maxBounces = 4;
    private LayerMask alienMask;
    private Hull nextInChain;
    private Hull currentAlien;

    private int bounces;
    private LineRenderer[] arcs;
    private Animator anim;
    private Collider2D tempAlienCollider;

    private float chainTimer = 0;

    protected override void Awake()
    {
        arcs = new LineRenderer[maxBounces + 1];
        base.Awake();
        anim = GetComponent<Animator>();
        alienMask = LayerMask.GetMask("Alien");
        for (int i = 0; i < maxBounces + 1; i++)
        {
            arcs[i] = ExplosionPooler.instance.GetExplosion(18).GetComponent<LineRenderer>();
        }
    }

    protected override void OnEnable()
    {
        anim.SetBool("Dead", false);
        nextInChain = null;
        currentAlien = null;
        base.OnEnable();
        bounces = 0;
        chainTimer = 0;
    }

    protected override void Update()
    {
        base.Update();
        chainTimer -= Time.deltaTime;

        if (chainTimer <= 0 && nextInChain)
        {
            ContinueLink();
        }
    }

    private void ContinueLink()
    {
        if (!currentAlien)
            return;
        arcs[bounces].SetPosition(0, currentAlien.transform.position);
        arcs[bounces].SetPosition(1, nextInChain.transform.position);
        arcs[bounces].gameObject.SetActive(true);
        GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
        explosion.transform.position = nextInChain.transform.position;
        explosion.SetActive(true);
        if (!nextInChain.invulnerable)
            nextInChain.Damage(projectileData.damage / 2 + 1, playerIndex);
        currentAlien = nextInChain;
        transform.position = nextInChain.transform.position;

        bounces++;
        currentLife += .25f;

        LookForNextTarget(currentAlien.transform.position);
        
        if (!nextInChain || bounces > maxBounces || nextInChain == currentAlien)
        {
            currentLife = 0;
            tempAlienCollider.enabled = true;
        }
        
        chainTimer = Random.Range(.1f, .2f);
    }

    private void LookForNextTarget(Vector2 startPos)
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 10, transform.up, 0, alienMask);
        if (hit)
        {
            if (tempAlienCollider)
                tempAlienCollider.enabled = true;

            nextInChain = hit.transform.GetComponent<Hull>();
            tempAlienCollider = hit.collider;

            tempAlienCollider.enabled = false;

            if(currentAlien == nextInChain)
            {
                nextInChain = null;
                return;
            }
        }
        else
            tempAlienCollider.enabled = true;
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            GameObject explosion;
            Hull h = col.GetComponent<Hull>();

            tempAlienCollider = col;

            currentAlien = h;
            if (h.invulnerable)
            {
                explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
                explosion.transform.position = transform.position;
                explosion.transform.rotation = transform.rotation;
                explosion.SetActive(true);
                currentLife = 0;
                return;
            }
            h.Damage(projectileData.damage, playerIndex); 
            dealtDmg = true;
            currentLife+= .26f;

            tempAlienCollider.enabled = false;

            explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
            explosion.transform.position = transform.position;
            explosion.transform.rotation = transform.rotation;
            explosion.SetActive(true);

            rb2d.velocity = Vector2.zero;
            anim.SetBool("Dead", true);

            LookForNextTarget(transform.position);
        }
    }

    protected override void Explode()
    {
        gameObject.SetActive(false);
        ProjectilePooler.instance.ReturnProjectileToPool(projectileData.poolIndex, gameObject);
        if (projectileData.isEnemy)
            EnemyManager.instance.projectileCount--;
        if(tempAlienCollider)
            tempAlienCollider.enabled = true;
    }
}
