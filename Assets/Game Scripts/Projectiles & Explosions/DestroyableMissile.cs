﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Hull))]
public class DestroyableMissile : Alien {

    private Transform currentTarget;
    private bool targetLost = false;
    private float currentLifeTime;

    protected override void OnEnable()
    {
        rb2d.velocity = Vector2.zero;
        rb2d.drag = 1;
        hull.hitPoints = alienData.hitPoints;
        hull.invulnerable = false;
        hull.blewUp = false;
        currentLifeTime = Random.Range(alienData.weaponSpread, 2f*alienData.weaponSpread);

        targetLost = false;
    }

    protected override void Update()
    {
        if (currentTarget.gameObject.activeInHierarchy && !targetLost)
        {
            RotateToward(rb2d.velocity + (Vector2)transform.position);

            distanceVec = (currentTarget.position + transform.up- transform.position).normalized;
            rb2d.velocity += distanceVec * alienData.speed * Time.deltaTime;
        }
        else
        {
            targetLost = true;
        }

        if (targetLost)
        {
            rb2d.velocity = (Vector2)transform.up * alienData.speed;
        }

        currentLifeTime -= Time.deltaTime;
        if(currentLifeTime <= 0)
        {
            hull.Damage(1000, 0);
        }
    }

    protected override void OnDisable()
    {
        SoundManager.instance.PlayFXSound(3, Random.Range(1f, 1.2f), .8f, .6f);
        CameraShaker.instance.ShakeCamera(.3f, .06f);
        EnemyManager.instance.RemoveAlien(hull);
    }

    public void SetSpeed(float velocity)
    {
        rb2d.velocity = velocity * (Vector2)transform.up;
    }

    public void SetTarget(Transform target)
    {
        currentTarget = target;
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {

    }
}
