﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionPooler : MonoBehaviour {

    public static ExplosionPooler instance;

    public List<ExplosionType> explosionTypes;
    public List<List<GameObject>> explosionPools = new List<List<GameObject>>();

    public RippleEffect rippleEffect;

    [System.Serializable]
    public class ExplosionType
    {
        public string name;
        public GameObject explosionPrefab;
        public int initialPoolAmount = 50;
    }

    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);

        rippleEffect = Camera.main.GetComponent<RippleEffect>();
    }

    private void Start()
    {
        InitializePools();
        Invoke("SpawnIntroParticles", 1);
    }

    private void SpawnIntroParticles()
    {
        GameObject e = GetExplosion(3);
        e.transform.position = Vector3.up * 9;
        e.SetActive(true);
    }

    private void InitializePools()
    {
        for (int i = 0; i < explosionTypes.Count; i++)
        {
            List<GameObject> p = new List<GameObject>();
            explosionPools.Add(p);

            for (int j = 0; j < explosionTypes[i].initialPoolAmount; j++)
            {
                GameObject go = Instantiate(explosionTypes[i].explosionPrefab, transform);
                go.SetActive(false);
                p.Add(go);
            }
        }
    }

    public GameObject GetExplosion(int poolIndex)
    {
        if (explosionPools[poolIndex].Count > 0)
        {
            GameObject p = explosionPools[poolIndex][0];
            explosionPools[poolIndex].Remove(explosionPools[poolIndex][0]);
            return p;
        }
        else
        {
            GameObject p = Instantiate(explosionTypes[poolIndex].explosionPrefab, transform);
            p.SetActive(false);
            return p;
        }
    }

    public void ReturnExplosionToPool(int poolIndex, GameObject p)
    {
        explosionPools[poolIndex].Add(p);
        p.SetActive(false);
    }
}
