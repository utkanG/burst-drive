﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailgunProjectile : Projectile {
    
    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
            {
                return;
            }
            h.Damage(projectileData.damage, playerIndex);

            GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
            explosion.transform.position = transform.position;
            explosion.transform.rotation = transform.rotation;
            explosion.SetActive(true);
        }
    }
}
