﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperMoltenShot : Projectile {

    private int hitsRemaining;

    protected override void OnEnable()
    {
        base.OnEnable();
        hitsRemaining = 4;
        transform.localScale = Vector3.one * 1.25f;
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
            {
                hitsRemaining--;
                return;
            }
            hitsRemaining--;
            h.Damage(projectileData.damage, playerIndex); 

            GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
            explosion.transform.position = transform.position;
            explosion.transform.rotation = transform.rotation;
            explosion.SetActive(true);
            transform.localScale = Vector3.one/1.2f;

            if (hitsRemaining <= 0)
                currentLife = 0;
        }
    }
}
