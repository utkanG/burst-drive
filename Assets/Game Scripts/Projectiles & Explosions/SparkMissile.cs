﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkMissile : Projectile {

    private Collider2D collider;
    private float tempTimer;

    protected override void Awake()
    {
        base.Awake();
        collider = GetComponent<Collider2D>();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        collider.enabled = true;
    }

    protected override void Update()
    {
        base.Update();
        if (!GameControl.instance.paused && !dealtDmg)
        {
            tempTimer -= Time.deltaTime;
            if (tempTimer <= 0)
            {
                rb2d.velocity = Random.insideUnitCircle * 35 + projectileData.speed * (Vector2)transform.up;
                tempTimer = Random.Range(.02f, .1f);
            }
        }
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
            {
                currentLife = 0f;
                return;
            }
            currentLife = 0f;
            h.Damage(projectileData.damage, playerIndex);
        }
    }

    protected override void Explode()
    {
        if (dealtDmg)
        {
            Remove();
            return;
        }
        collider.enabled = false;
        rb2d.velocity = Vector2.zero;
        currentLife = .5f;
        GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
        explosion.transform.position = transform.position;
        explosion.transform.rotation = transform.rotation;
        explosion.SetActive(true);
        dealtDmg = true;
    }

    private void Remove()
    {
        gameObject.SetActive(false);
        ProjectilePooler.instance.ReturnProjectileToPool(projectileData.poolIndex, gameObject);
    }
}
