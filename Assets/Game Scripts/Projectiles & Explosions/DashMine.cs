﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashMine : Projectile
{
    protected override void OnEnable()
    {
        base.OnEnable();
        rb2d.AddTorque(Random.Range(-1f,1f) * 90);
        rb2d.velocity = Random.insideUnitCircle * 5;
        currentLife *= Random.Range(.8f,1.2f);
    }

    protected override void Explode()
    {
        base.Explode();
        SoundManager.instance.PlayFXSound(10, Random.Range(.7f,.8f), 1);
    }
}
