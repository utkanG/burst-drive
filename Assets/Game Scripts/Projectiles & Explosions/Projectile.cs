﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Projectile : MonoBehaviour {

    public ScriptableProjectile projectileData;

    protected float currentLife;
    protected Rigidbody2D rb2d;

    protected bool dealtDmg = false;
    protected int playerIndex = 0;

    protected virtual void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    protected virtual void OnEnable()
    {
        currentLife = projectileData.lifeTime;
        rb2d.velocity = projectileData.speed * transform.up;
        dealtDmg = false;
        ProjectilePooler.instance.spawnedProjectiles.Add(gameObject);
        if (CompareTag("p1"))
            playerIndex = 1;
        else if (CompareTag("p2"))
            playerIndex = 2;
        else playerIndex = 0;
    }

    protected virtual void Update()
    {
        currentLife -= Time.deltaTime;
        if(currentLife <= 0)
        {
            Explode();
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        if (!dealtDmg)
        {
            Hull h = col.GetComponent<Hull>();
            if (h.invulnerable)
            {
                currentLife = 0;
                return;
            }
            h.Damage(projectileData.damage, playerIndex);
            dealtDmg = true;
            currentLife = 0;
        }
    }

    protected virtual void Explode()
    {
        gameObject.SetActive(false);
        GameObject explosion = ExplosionPooler.instance.GetExplosion(projectileData.explosionPoolIndex);
        explosion.transform.position = transform.position;
        explosion.transform.rotation = transform.rotation;
        explosion.SetActive(true);
        ProjectilePooler.instance.ReturnProjectileToPool(projectileData.poolIndex, gameObject);
        if (projectileData.isEnemy)
            EnemyManager.instance.projectileCount--;
    }
}
