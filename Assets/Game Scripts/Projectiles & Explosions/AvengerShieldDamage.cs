﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvengerShieldDamage : Projectile {

    private int count = 0;

    protected override void OnEnable()
    {
        base.OnEnable();
        count = 0;
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        Hull h = col.GetComponent<Hull>();
        h.Damage(projectileData.damage, playerIndex);

        if(projectileData.poolIndex != 20) return;
        if(count < 15)
        {
            if(!h.blewUp) return;

            count++;
            if(count == 15)
            {
                SteamManager.Instance.UnlockAchievement("PLAYER_DEATH");
            }
        }
    }

    protected override void Explode()
    {
        ProjectilePooler.instance.ReturnProjectileToPool(projectileData.poolIndex, gameObject);
        gameObject.SetActive(false);
    }
}
