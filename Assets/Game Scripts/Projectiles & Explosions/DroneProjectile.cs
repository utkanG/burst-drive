﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneProjectile : Projectile {

    private float bootUpTimer;
    private Hull targetHull;
    private float fireCooldown;
    private LineRenderer droneBeam;
    private bool inRange;
    [SerializeField]
    private LayerMask alienMask;
    [SerializeField]
    private GameObject greenLaser;
    private int direction;

    protected override void Awake()
    {
        base.Awake();
        droneBeam = ExplosionPooler.instance.GetExplosion(21).GetComponent<LineRenderer>();
        droneBeam.transform.position = Vector3.zero;
        droneBeam.gameObject.SetActive(false);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        rb2d.drag = 2;
        direction = UnityEngine.Random.Range(-1, 2);
        if (direction == 0)
            direction = 1;
        rb2d.freezeRotation = false;
        greenLaser.SetActive(false);
        bootUpTimer = 2f;
        inRange = false;
        targetHull = null;
        int rand = UnityEngine.Random.Range(-1, 2) * 5;
        if (rand == 0)
            rand = 5;
        rb2d.AddTorque(rand, ForceMode2D.Impulse);
    }

    protected override void Update()
    {
        bootUpTimer -= Time.deltaTime;
        fireCooldown -= Time.deltaTime;
        if (bootUpTimer <= 0 && !targetHull)
        {
            ChooseTarget();
            greenLaser.SetActive(true);
        }
        else if(targetHull)
        {
            Vector2 distance = (targetHull.transform.position - transform.position);
            RotateToward(targetHull.transform.position);
            CheckInRange(distance);
            if (inRange && fireCooldown <= 0)
                Fire();
            else if(!GameControl.instance.paused)
                MoveToTarget(distance);
        }
        base.Update();
    }

    private void MoveToTarget(Vector2 dist)
    {
        if (dist.magnitude > 5)
        {
            rb2d.velocity += dist.normalized * 1.1f;
        }
        else
            rb2d.velocity += (Vector2)transform.right * direction - (Vector2)transform.up * .2f;
    }

    private void CheckInRange(Vector2 dist)
    {
        if (dist.magnitude < 5)
            inRange = true;
        else
            inRange = false;
        if (!targetHull.gameObject.activeInHierarchy)
        {
            ChooseTarget();
        }
    }

    private void RotateToward(Vector3 target)
    {
        Vector2 distanceVec = transform.position - target;
        Quaternion targetRot = Quaternion.AngleAxis(Mathf.Atan2(distanceVec.y, distanceVec.x) * Mathf.Rad2Deg + 90, Vector3.forward);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, .5f);
    }

    private void Fire()
    {
        if (targetHull)
        {
            SoundManager.instance.PlayFXSound(0, 1 / 1f, .15f);
            fireCooldown = .4f;
            if(!targetHull.invulnerable)
                targetHull.Damage(projectileData.damage, playerIndex); 
            droneBeam.SetPosition(0, transform.position);
            droneBeam.SetPosition(1, targetHull.transform.position);
            droneBeam.gameObject.SetActive(true);
            rb2d.freezeRotation = true;
            GameObject e = ExplosionPooler.instance.GetExplosion(25);
            e.transform.position = transform.position;
            e.SetActive(true);
            GameObject e2 = ExplosionPooler.instance.GetExplosion(23);
            e2.transform.position = targetHull.transform.position;
            e2.SetActive(true);
            if (targetHull.hitPoints <= 0)
            {
                PickupManager.instance.SpawnPickUp(UnityEngine.Random.Range(0, 4), targetHull.transform.position);
                ChooseTarget();
            }
        }
    }

    private void ChooseTarget()
    {
        direction = UnityEngine.Random.Range(-1, 2);
        if (direction == 0)
            direction = 1;
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 10, Vector2.up, 0, alienMask);
        if (hit)
            targetHull = hit.transform.GetComponent<Hull>();
        else
            Explode();
        
        rb2d.drag = 5;
    }
}
