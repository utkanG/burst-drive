﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Microrocket : Projectile {

    GameObject target;
    private float timer = 0;

    private bool acquiredTarget = false;
    [SerializeField]
    private LayerMask alienMask;

    private bool locked;

    protected override void Awake()
    {
        base.Awake();
        LockRocket();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        LockRocket();
        timer = 0;
        locked = false;
    }

    protected override void Update()
    {
        base.Update();
        if (locked)
            return;
        if (!GameControl.instance.paused)
        {
            if (!acquiredTarget)
            {
                LookForTarget();
            }
            else
            {
                Vector2 direction = (target.transform.position - transform.position).normalized;
                rb2d.velocity += direction * 2.5f;
                timer -= Time.deltaTime;
                if (!target.activeInHierarchy || timer <= 0)
                    LockRocket();
            }
            Quaternion targetRot = Quaternion.AngleAxis(Mathf.Atan2(rb2d.velocity.y, rb2d.velocity.x) * Mathf.Rad2Deg - 90, Vector3.forward);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, .3f);
        }
    }

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if (!acquiredTarget)
            AcquireTarget(col.gameObject);
        else
            base.OnTriggerEnter2D(col);
    }

    private void AcquireTarget(GameObject obj)
    {
        target = obj;
        acquiredTarget = true;
        timer = .4f;
    }

    private void LockRocket()
    {
        locked = true;
        acquiredTarget = false;
        target = null;
        rb2d.velocity = transform.up * projectileData.speed;
    }

    private void LookForTarget()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 4, transform.up, 3, alienMask);
        if (hit)
            AcquireTarget(hit.collider.gameObject);
    }
}
