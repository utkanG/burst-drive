﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShaker : MonoBehaviour {

    public static CameraShaker instance;

    private Vector3 originalPosition;

    public float shakeDuration;
    private float shakeDensity;

    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);

        originalPosition = transform.position;
        shakeDuration = 0;
    }

    private void Update()
    {
        if(shakeDuration > 0 && !GameControl.instance.paused)
        {
            shakeDuration -= Time.deltaTime;
            transform.position = originalPosition + Random.insideUnitSphere * shakeDensity / 10;
        }
        else
        {
            transform.position = originalPosition;
        }
    }

    public void ShakeCamera(float density, float duration)
    {
        shakeDensity = density;
        shakeDuration = duration;
    }
}
