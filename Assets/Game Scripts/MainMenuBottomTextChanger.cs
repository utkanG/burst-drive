﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuBottomTextChanger : MonoBehaviour {

    [TextArea]
    public List<string> texts;

    private Text bottomText;

    private void Awake()
    {
        bottomText = GetComponent<Text>();
    }

    private void OnEnable()
    {
        bottomText.text = texts[Random.Range(0, texts.Count)];
    }
}
