﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialControl : MonoBehaviour {

    public static TutorialControl instance;

    [SerializeField]
    private Text mainText;
    [SerializeField]
    private Text flashingText;
    [SerializeField]
    private GameObject introTextObject;

    [TextArea]
    public string[] tutoTexts;
    [TextArea]
    public string continueText;
    [TextArea]
    public string continueTextController;
    [TextArea]
    public string skipTextController;
    [TextArea]
    public string skipText;

    [SerializeField]
    private Image holdTimerImage;

    private int currentTextIndex;
    private float timer;
    private float holdTimer;
    private bool tutoBegun;
    private bool objectiveGiven;

    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(gameObject);
    }

    private void Start()
    {
        ResetTuto();
        tutoTexts[2] = "YOU CAN CALL ME \"" + System.Environment.UserName.ToUpper() + "\"...";
    }

    private void Update()
    {
        if (LevelManager.instance.level0Active)
        {
            timer += Time.deltaTime;
            if (!tutoBegun)
            {
                holdTimerImage.fillAmount = timer / 4f;
            }

            if (tutoBegun && (Input.GetKeyDown(KeyCode.Return)||Input.GetKeyDown(KeyCode.Joystick1Button8)) && !objectiveGiven)
            {
                NextText();
            }

            if ((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Joystick1Button8)) && !tutoBegun)
            {
                holdTimer += Time.deltaTime;
                timer -= Time.deltaTime;

                if (holdTimer >= 1f)
                {
                    StartTuto();
                    timer = 0;
                }
            }

            if ((Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.Joystick1Button8)))
            {
                holdTimer = 0;
            }

            if (timer >= 4 && !tutoBegun)
            {
                EndTuto();
            }

            if(timer >= 2)
            {
                flashingText.gameObject.SetActive(true);
            }
        }
    }

    public void ResetTuto()
    {
        objectiveGiven = false;
        holdTimerImage.fillAmount = 0;
        currentTextIndex = 0;
        tutoBegun = false;
        holdTimer = 0;
        timer = 0;
        introTextObject.SetActive(false);

        if(!PickupManager.instance.player1.controllerInput)
            flashingText.text = skipText;
        else
            flashingText.text = skipTextController;

        flashingText.gameObject.SetActive(false);
        mainText.gameObject.SetActive(false);
    }

    public void ActivateSkipTuto()
    {
        if (!PickupManager.instance.player1.controllerInput)
            flashingText.text = skipText;
        else
            flashingText.text = skipTextController;
        flashingText.gameObject.SetActive(true);
    }

    public void ActivateIntroText()
    {
        //introTextObject.SetActive(true); //TODO
    }

    public void EndTuto()
    {
        timer = 1;
        holdTimerImage.fillAmount = 0;
        flashingText.gameObject.SetActive(false);
        mainText.gameObject.SetActive(false);
        LevelManager.instance.level0Active = false;
        LevelManager.instance.level0Ready = false;
    }

    private void StartTuto()
    {
        holdTimerImage.fillAmount = 0;
        tutoBegun = true;
        currentTextIndex = 0;
        mainText.text = tutoTexts[currentTextIndex];
        mainText.gameObject.SetActive(true);
        flashingText.gameObject.SetActive(false);
        if (!PickupManager.instance.player1.controllerInput)
            flashingText.text = continueText;
        else
            flashingText.text = continueTextController;
    }

    private void NextText()
    {
        objectiveGiven = false; // default, change this for given objectives according to index
        timer = 0;
        flashingText.gameObject.SetActive(false);
        currentTextIndex++;
        if (currentTextIndex >= tutoTexts.Length)
        {
            EndTuto();
            return;
        }
        mainText.text = tutoTexts[currentTextIndex];
    }
}
