﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundToPool : MonoBehaviour {

    public AudioSource source;

    private void Awake()
    {
        gameObject.AddComponent<AudioSource>();
        source = GetComponent<AudioSource>();
    }

    public void ReturnToPoolIn(float seconds)
    {
        Invoke("ReturnToPool", seconds);
    }

    private void ReturnToPool()
    {
        SoundManager.instance.ReturnAudioSource(this);
    }
}
