﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/HighEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DisplaceTex ("Displacement Texture", 2D) = "white" {}
		_Magnitude("Magnitude", Range(0,0.1)) = 1
		_speed("Speed", float) = 1
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _speed;
			sampler2D _DisplaceTex;
			float _Magnitude;

			fixed4 frag (v2f i) : SV_Target
			{
				float2 distuv = float2(i.uv.x , i.uv.y);

				float2 disp = tex2D(_DisplaceTex, i.uv + _Time.x * 2).xy;
				disp = ((disp * 2) - 1) * _Magnitude;

				float4 col = tex2D(_MainTex, i.uv + disp + float2(sin(_Time[1] * _speed) / 2200, 0));

				//fixed4 col = tex2D(_MainTex, i.uv + float2(sin(i.vertex.y / 50 * _yAmount + _Time[1] * _speed) / 50 * _yAmount2, sin(i.vertex.x / 25 * _xAmount + _Time[1] * _speed) / 50 * _xAmount2));
				// just invert the colors
				//col.rgb = 1 - col.rgb;
				return col;
			}
			ENDCG
		}
	}
}
